package com.epam.newsmanagement.controller;

public class Constans {

	//Url to action
	public static final String ACT_VAL_HOME = "/home";
	public static final String ACT_VAL_HOME_RESET = "/home/reset";
	public static final String ACT_VAL_NEWS_NEWS_ID = "/news/{newsId}";
	
	//Path variable
	public static final String PATH_VAR_NEWS_ID = "newsId";
	
	//Attribute of session	
	public static final String SESS_ATTR_PAGE = "page";
	public static final String SESS_ATTR_PAGE_NUMB = "pageNumber";
	public static final String SESS_ATTR_AUTHOR_ID_SELECTED = "authorIdSelected";
	public static final String SESS_ATTR_TAG_ID_SELECTED = "tagIdListSelected";
	public static final String SESS_ATTR_FILTER = "filter";
	
	//Attributes of model	
	public static final String MODEL_ATTR_AUTHOR_LIST = "authorList";
	public static final String MODEL_ATTR_TAG_LIST = "tagList";
	public static final String MODEL_ATTR_COMMENT = "comment";
	public static final String MODEL_ATTR_NEWS_VO = "newsVO";
	public static final String MODEL_ATTR_PREV = "prev";
	public static final String MODEL_ATTR_NEXT = "next";
	
	//Page	
	public static final String PAGE_HOME = "home";
	public static final String PAGE_ERROR = "error";
	public static final String PAGE_NEWS = "news";
	public static final String REDIRECT_TO_NEWS_NEWS_ID = "redirect:/news/{newsId}";
	
	//Program variable
	public static final int PAGE_SIZE = 3;
}
