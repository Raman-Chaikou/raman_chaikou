package com.epam.newsmanagement.controller;

import static com.epam.newsmanagement.controller.Constans.ACT_VAL_NEWS_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_COMMENT;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_VO;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEXT;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_PREV;
import static com.epam.newsmanagement.controller.Constans.PAGE_ERROR;
import static com.epam.newsmanagement.controller.Constans.PAGE_NEWS;
import static com.epam.newsmanagement.controller.Constans.PATH_VAR_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.REDIRECT_TO_NEWS_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.SESS_ATTR_AUTHOR_ID_SELECTED;
import static com.epam.newsmanagement.controller.Constans.SESS_ATTR_FILTER;
import static com.epam.newsmanagement.controller.Constans.SESS_ATTR_TAG_ID_SELECTED;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.vo.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;

@Controller
public class ViewNewsController {
	
	private static final Logger LOG = Logger.getLogger(ViewNewsController.class);
	
	@Autowired
	private NewsManagementService newsManagementService;
	
	@Autowired
	@Qualifier("commentTextValidator")
	private Validator validator;
		
	@RequestMapping(value = ACT_VAL_NEWS_NEWS_ID, 
					method = RequestMethod.GET)
	public String news(
			@PathVariable(PATH_VAR_NEWS_ID) long newsId, 
			Model model,
			HttpSession session){
		try {
			
			addAttributeModel(model, newsId, session);
			CommentTO comment = new CommentTO();
			model.addAttribute(MODEL_ATTR_COMMENT, comment);
			
		} catch (ServiceException e) {
			LOG.error("Exception in news(), method = get", e);
			return PAGE_ERROR;
		}
		return PAGE_NEWS;
	}
	
	private NewsTO fetchNextNews(List<NewsTO> newsTOList, 
								long newsId){
		int positionLastNews = newsTOList.size() - 1;
		if(newsTOList.get(positionLastNews).getNewsId() == newsId){
			return null;
		}
		return newsTOList.get(positionLastNews);
	}
	
	private NewsTO fetchPrevNews(List<NewsTO> newsTOList, 
								long newsId){
		if(newsTOList.get(0).getNewsId() == newsId){
			return null;
		}
		return newsTOList.get(0);
	}
	
	private NewsFilter initFilter(HttpSession session){
		NewsFilter filter = (NewsFilter) session.getAttribute(SESS_ATTR_FILTER);
		filter = filter == null ? new NewsFilter() : filter;
		List<Long> tagIdListFilter = readTagFilter(filter);
		filter.setTagIdsFilter(tagIdListFilter);
		return filter;
	}
	
	@SuppressWarnings("unchecked")
	private List<Long> readTagFilter(NewsFilter filter) {
		return (List<Long>) (filter.getTagIdsFilter() == null 
				? Collections.emptyList() : filter.getTagIdsFilter());
	}
	
	@RequestMapping(value = ACT_VAL_NEWS_NEWS_ID, 
					method = RequestMethod.POST)
	public String createComment(
					@PathVariable(PATH_VAR_NEWS_ID) long newsId,
					Model model,
					@ModelAttribute(MODEL_ATTR_COMMENT) @Validated CommentTO comment,
					HttpSession session,
					BindingResult bindingResult){
		String urlReturnPage = PAGE_NEWS;
		try {
			validator.validate(comment, bindingResult);
			if (!bindingResult.hasErrors()) {
				comment.setNewsId(newsId);
				comment.setCreationDate(new Date());
				newsManagementService.addComment(comment);
				urlReturnPage = REDIRECT_TO_NEWS_NEWS_ID;
			}
			model.addAttribute(MODEL_ATTR_COMMENT, comment);		
			addAttributeModel(model, newsId, session);		
			
		} catch (ServiceException e) {
			LOG.error("Exception in createComment(), method = post", e);
			return PAGE_ERROR; 
		}		
		return urlReturnPage;
	}
	
	private void addAttributeModel(Model model, long newsId, 
			HttpSession session) throws ServiceException{
		
		NewsVO newsVO = newsManagementService.takeNews(newsId);
		model.addAttribute(MODEL_ATTR_NEWS_VO, newsVO);
		NewsFilter filter = initFilter(session);
		List<NewsTO> newsTOList = newsManagementService
									.fetchNewsAndNeighbor(newsId, filter);
		NewsTO newsTOPrev = fetchPrevNews(newsTOList, newsId);
		NewsTO newsTONext = fetchNextNews(newsTOList, newsId);
		model.addAttribute(MODEL_ATTR_PREV, newsTOPrev);
		model.addAttribute(MODEL_ATTR_NEXT, newsTONext);
	}
}
