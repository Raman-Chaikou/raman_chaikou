package com.epam.newsmanagement.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;
import com.epam.newsmanagement.util.page.entity.Page;

import static com.epam.newsmanagement.controller.Constans.*;

@Controller
public class HomeController {
		

	private static final Logger LOG = Logger.getLogger(HomeController.class);
	
	@Autowired
	private NewsManagementService newsManagementServie;
	
	@RequestMapping(value = ACT_VAL_HOME, method = RequestMethod.GET)
	public String home(Model model,
			@RequestParam(defaultValue = "1") int pageNumber,
			HttpSession session){
		try {
			NewsFilter filter = initFilter(session);
			Page page = newsManagementServie.fetchListNews(
					PAGE_SIZE, pageNumber, filter);
			
			session.setAttribute(SESS_ATTR_PAGE, page);
			session.setAttribute(SESS_ATTR_PAGE_NUMB, pageNumber);
			
			List<AuthorTO> authorTOList = newsManagementServie.fetchAuthors();
			model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authorTOList);
			
			List<TagTO> tagTOList = newsManagementServie.fetchTags();
			model.addAttribute(MODEL_ATTR_TAG_LIST, tagTOList);

		} catch (ServiceException e) {
			LOG.error("Exception in home(), method = get ",e);
			return PAGE_ERROR; 
		}
			return PAGE_HOME;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = ACT_VAL_HOME, method = RequestMethod.POST)
	public String filter( Model model,
			@RequestParam(defaultValue = "1") int pageNumber,
			@RequestParam(required = false,defaultValue = "0") Long authorId,
			@RequestParam(required = false) List<Long> tagIdList,
			HttpSession session){
		try {
			
			tagIdList = (List<Long>) (tagIdList == null ? Collections.emptyList() 
							                            : tagIdList);
			
			NewsFilter filter = new NewsFilter();
			filter.setAuthorIdFilter(authorId);
			filter.setTagIdsFilter(tagIdList);
			
			session.setAttribute(SESS_ATTR_FILTER, filter);
			
			Page page = newsManagementServie.fetchListNews(
					PAGE_SIZE, pageNumber,filter);
						
			session.setAttribute(SESS_ATTR_PAGE, page);
			session.setAttribute(SESS_ATTR_PAGE_NUMB, pageNumber);
			
			List<AuthorTO> authorTOList = newsManagementServie.fetchAuthors();
			model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authorTOList);
			
			List<TagTO> tagTOList = newsManagementServie.fetchTags();
			model.addAttribute(MODEL_ATTR_TAG_LIST, tagTOList);

		} catch (ServiceException e) {
			LOG.error("Exception in home(), method = post", e);
			return PAGE_ERROR;
		}

		return PAGE_HOME;
	}

	@RequestMapping(value = ACT_VAL_HOME_RESET , method = RequestMethod.GET)
	public String resetFilter(HttpSession session, Model model){
		
		Long authorId = 0L;
		List<Long> tagIdList = Collections.emptyList();
		
		NewsFilter filter = new NewsFilter();
		filter.setAuthorIdFilter(authorId);
		filter.setTagIdsFilter(tagIdList);
			
		session.setAttribute(SESS_ATTR_FILTER, filter);
		
		int pageNumber = 1;
		try {
			
			Page page = newsManagementServie.fetchListNews(
					PAGE_SIZE, pageNumber,filter);
			session.setAttribute(SESS_ATTR_PAGE, page);
			session.setAttribute(SESS_ATTR_PAGE_NUMB, pageNumber);
			List<AuthorTO> authorTOList = newsManagementServie.fetchAuthors();
			model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authorTOList);
			
			List<TagTO> tagTOList = newsManagementServie.fetchTags();
			model.addAttribute(MODEL_ATTR_TAG_LIST, tagTOList);
		} catch (ServiceException e) {
			LOG.error("Exception in resetFilter(), method = get. ", e);
			return PAGE_ERROR;
		}
					
		return PAGE_HOME;
	}
	
	private NewsFilter initFilter(HttpSession session){
		NewsFilter filter = (NewsFilter) session.getAttribute(SESS_ATTR_FILTER);
		filter = filter == null ? new NewsFilter() : filter;
		List<Long> tagIdListFilter = readTagFilter(filter);
		filter.setTagIdsFilter(tagIdListFilter);
		return filter;
	}

	
	@SuppressWarnings("unchecked")
	private List<Long> readTagFilter(NewsFilter filter) {
		return (List<Long>) (filter.getTagIdsFilter() == null 
										? Collections.emptyList() : filter.getTagIdsFilter());
	}
}
