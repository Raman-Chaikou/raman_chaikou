<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<div class="container-home-header" align="center">
		<div style="margin-bottom: -15px;">
			<form action="<c:url value="/home"/>" method="post" >
			<div style="float: left; margin-left: 200px; padding-right: 5px;">
				<select name="authorId">
				
					<option value="0" <c:if test="${filter.authorIdFilter == 0 }">selected</c:if> >
						<spring:message code="filter.author.message"/>
					</option>	
					<c:forEach items="${authorList}" var="author">
								<option value="${author.authorId}" 
										<c:if test="${filter.authorIdFilter == author.authorId }"> 
											selected
										</c:if>>
									${author.authorName}
								</option>
					</c:forEach>
				</select>
			</div>
			<div class="multiselect" style="float: left;padding-right: 5px;">
				<div class="selectBox" onclick="showCheckboxes()">
					<select>
						<option><spring:message code="filter.tag.message"/></option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes">
					<c:forEach items="${tagList}" var="tag">
						<label class="checkBTag" for="${tag.tagId}">
							<input type="checkbox"  name="tagIdList" value="${tag.tagId}" 
								class="checkBTag" id="${tag.tagId}" 
								<c:if test="${ not empty filter.tagIdListFilter and filter.tagIdListFilter.contains(tag.tagId) }">
									checked
								</c:if>
							 />
							${tag.tagName}
						</label>
					</c:forEach>
				</div>
			</div>
			<div style="float: left;">
					<input type="submit"  value="<spring:message code="name.filter"/>">
			</div>
			</form>
			<form style="margin-right: 100px;" action="<c:url value="/home/reset"/>" method="get">
					<input type="submit"  value="<spring:message code="name.reset"/>">
			</form>	
		</div>
		<br>		
	</div>
	<div class="container-home-body" align="center">
	<c:if test="${not empty page.newsVOList }">
		<c:forEach items="${page.newsVOList}" var="newsVO">
		<div>
			<table  class="container-home-body-table">
				<tr>
					<td class="cont-hbt-title" align="left">${newsVO.news.title}</td>
					<td class="cont-hbt-name" align="center">
						(<spring:message code="name.by"/> ${newsVO.author.authorName})
					</td>
					<td class="cont-hbt-date" colspan="2" align="right">
						<spring:message code="pattern.date" var="dataStyle" scope="page"/>
						<fmt:formatDate value="${newsVO.news.modificationDate}" pattern="${dataStyle}"/>
					</td>
				<tr>
				<tr>
					<td colspan="4" align="left">${newsVO.news.shortText}</td>
				</tr>
				<tr>
					<td class="cont-hbt-taglist" colspan="2" align="right">
						<c:forEach items="${newsVO.tagList}" var="tag">
							${tag.tagName} 
						</c:forEach>
					</td>
					<td class="cont-hbt-commentlist" align="center">
						<spring:message code="name.comments"/>(${newsVO.commentList.size()})
					</td>
					<td class="cont-hbt-view" align="right">
						<a href="<c:url value="/news/${newsVO.news.newsId}"/>">
							<spring:message code="name.view"/>
						</a>
					</td>
				</tr>
			</table>
		</div>
		<br>
		<br>
		</c:forEach>
	</c:if>
	</div>
	<div class="container-home-footer" align="center">
		<div>
			<form class="submit-pag" action="<c:url value="/home"/>" method="get">
				<c:if test="${not empty page.newsVOList }">
					<c:forEach begin="1" end="${page.countPage}" var="i">
						<input type="submit" name="pageNumber" value="${i}">
					</c:forEach>
				</c:if>
			</form>
		</div>
	</div>