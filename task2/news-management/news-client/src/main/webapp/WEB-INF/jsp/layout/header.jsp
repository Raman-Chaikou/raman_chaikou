<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table class="table-header">
	<tr>
		<td class="table-header-td-left" align="left">
			<h2><spring:message code="name.portal"/></h2>
		</td>
		<td class="table-header-td-right" align="right">
			<a href="?language=en">EN</a>
			<a href="?language=ru">RU</a>
		</td>
	</tr>
</table>