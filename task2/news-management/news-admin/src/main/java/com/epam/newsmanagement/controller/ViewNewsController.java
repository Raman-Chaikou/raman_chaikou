package com.epam.newsmanagement.controller;

import static com.epam.newsmanagement.controller.Constans.ACT_VAL_NEWS_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_NEWS_NEWS_ID_DEL_COMM;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_COMMENT;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_VO;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEXT;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_PREV;
import static com.epam.newsmanagement.controller.Constans.PAGE_NEWS;
import static com.epam.newsmanagement.controller.Constans.PATH_VAR_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.REDIRECT_TO_NEWS_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.REG_PARAM_COMMENT_ID;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.vo.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;
import com.epam.newsmanagement.utils.controller.ControllerUtils;

@Controller
public class ViewNewsController {

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	@Qualifier("commentTextValidator")
	private Validator validator;

	@RequestMapping(value = ACT_VAL_NEWS_NEWS_ID, method = RequestMethod.GET)
	public String news(@PathVariable(PATH_VAR_NEWS_ID) long newsId,
			Model model, HttpSession session) throws ServiceException {
		fillAttribute(model, session, newsId);
		CommentTO comment = new CommentTO();
		model.addAttribute(MODEL_ATTR_COMMENT, comment);
		return PAGE_NEWS;
	}

	@RequestMapping(value = ACT_VAL_NEWS_NEWS_ID, method = RequestMethod.POST)
	public String createComment(@PathVariable(PATH_VAR_NEWS_ID) long newsId,
			Model model,
			@ModelAttribute(MODEL_ATTR_COMMENT) @Validated CommentTO comment,
			HttpSession session, BindingResult bindingResult)
			throws ServiceException {
		String urlReturnPage = PAGE_NEWS;

		validator.validate(comment, bindingResult);
		if (!bindingResult.hasErrors()) {
			comment.setNewsId(newsId);
			comment.setCreationDate(new Date());
			newsManagementService.addComment(comment);
			urlReturnPage = REDIRECT_TO_NEWS_NEWS_ID;
		}
		model.addAttribute(MODEL_ATTR_COMMENT, comment);
		fillAttribute(model, session, newsId);

		return urlReturnPage;
	}

	private void fillAttribute(Model model, HttpSession session, Long newsId)
			throws ServiceException {
		NewsVO newsVO = newsManagementService.takeNews(newsId);
		model.addAttribute(MODEL_ATTR_NEWS_VO, newsVO);

		NewsFilter filter = ControllerUtils.initFilter(session);
		List<NewsTO> newsTOList = newsManagementService.fetchNewsAndNeighbor(
				newsId, filter);
		NewsTO newsTOPrev = fetchPrevNews(newsTOList, newsId);
		NewsTO newsTONext = fetchNextNews(newsTOList, newsId);
		model.addAttribute(MODEL_ATTR_PREV, newsTOPrev);
		model.addAttribute(MODEL_ATTR_NEXT, newsTONext);
	}

	@RequestMapping(value = ACT_VAL_NEWS_NEWS_ID_DEL_COMM, method = RequestMethod.POST)
	public String deleteComment(
			@RequestParam(value = REG_PARAM_COMMENT_ID) Long commentId)
			throws ServiceException {
		newsManagementService.deleteComment(commentId);
		return REDIRECT_TO_NEWS_NEWS_ID;
	}

	private NewsTO fetchNextNews(List<NewsTO> newsTOList, long newsId) {
		int positionLastNews = newsTOList.size() - 1;
		return newsTOList.get(positionLastNews).getNewsId() == newsId ? null
				: newsTOList.get(positionLastNews);
	}

	private NewsTO fetchPrevNews(List<NewsTO> newsTOList, long newsId) {
		if (newsTOList.get(0).getNewsId() == newsId) {
			return null;
		}
		return newsTOList.get(0);
	}
}
