package com.epam.newsmanagement.utils.controller;

import static com.epam.newsmanagement.controller.Constans.SESS_ATTR_FILTER;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.util.filter.entity.NewsFilter;

public class ControllerUtils {
	public static List<Long> notNullList(List<Long> list){
		return list == null ? Collections.emptyList() : list;
	}
	
	private static List<Long> readTagFilter(NewsFilter filter){
		return filter.getTagIdsFilter() == null ? Collections.emptyList() : 
													filter.getTagIdsFilter();
	}
	
	public static NewsFilter initFilter(HttpSession session){
		NewsFilter filter = (NewsFilter) session.getAttribute(SESS_ATTR_FILTER);
		filter = filter == null ? new NewsFilter() : filter;
		List<Long> tagIdsFilter = ControllerUtils.readTagFilter(filter);
		filter.setTagIdsFilter(tagIdsFilter);
		return filter;	
	}
}
