package com.epam.newsmanagement.utils.formater;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.format.Formatter;

public class DateFormatter implements Formatter<Date>{
	
	
	
	@Override
	public Date parse(String text, Locale locale) throws ParseException {
		if(text.length() == 0){
			return null;
		}	
		return getDateFormat(locale).parse(text);
	}
	
	@Override
	public String print(Date date, Locale locale) {
		if(date == null){
			return "";
		}
		return getDateFormat(locale).format(date);
	}
	
	
	private DateFormat getDateFormat(Locale locale){
		String patternDate = FormatterUtils.takePatternDate(locale);
		DateFormat dateFormat = new SimpleDateFormat(patternDate, locale);
		return dateFormat;
		
	}
}
