package com.epam.newsmanagement.utils.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.to.TagTO;

public class TagValidator implements Validator{
	private static final int COUNT_CHARACTER = 30;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return TagTO.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		TagTO tag = (TagTO) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tagName", "valid.tag.tagName");
		if(tag.getTagName().length() > COUNT_CHARACTER){
			errors.rejectValue("tagName", "valid.tag.tagName.size");
		}
		
	}
	
}
