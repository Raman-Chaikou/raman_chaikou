package com.epam.newsmanagement.utils.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.to.NewsTO;

public class NewsValidator implements Validator{
	
	private static final int COUNT_CHARACTER_TITLE = 30;
	private static final int COUNT_CHARACTER_SHORT_TEXT = 100;
	private static final int COUNT_CHARACTER_FULL_TEXT = 2000;
	
	
	@Override
	public boolean supports(Class<?> clazz) {
		return NewsTO.class.equals(clazz);
	}
	
	@Override
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "valid.news.title");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "modificationDate", "valid.news.modificationDate");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortText", "valid.news.shorttext");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fullText", "valid.news.fulltext");
		
		NewsTO newsTO = (NewsTO) object;
		validTitle(newsTO, errors);
		validShortText(newsTO, errors);
		validFullText(newsTO, errors);
	}
	
	private void validTitle(NewsTO newsTO, Errors errors){
		if(newsTO.getTitle().length() > COUNT_CHARACTER_TITLE){
			errors.rejectValue("title", "valid.news.title.size");
		}
	}
	
	private void validShortText(NewsTO newsTO, Errors errors){
		if(newsTO.getShortText().length() > COUNT_CHARACTER_SHORT_TEXT){
			errors.rejectValue("shortText", "valid.news.shorttext.size");
		}
	}
	
	private void validFullText(NewsTO newsTO, Errors errors){
		if(newsTO.getFullText().length() > COUNT_CHARACTER_FULL_TEXT){
			errors.rejectValue("fullText", "valid.news.fullText.size");
		}
	}
}
