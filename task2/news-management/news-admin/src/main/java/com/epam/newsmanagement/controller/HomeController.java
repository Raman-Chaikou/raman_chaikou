package com.epam.newsmanagement.controller;

import static com.epam.newsmanagement.controller.Constans.ACT_VAL_HOME;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_HOME_DELETE;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_HOME_RESET;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_AUTHOR_LIST;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_TAG_LIST;
import static com.epam.newsmanagement.controller.Constans.PAGE_HOME;
import static com.epam.newsmanagement.controller.Constans.PAGE_SIZE;
import static com.epam.newsmanagement.controller.Constans.REDIRECT_HOME;
import static com.epam.newsmanagement.controller.Constans.SESS_ATTR_FILTER;
import static com.epam.newsmanagement.controller.Constans.SESS_ATTR_PAGE;
import static com.epam.newsmanagement.controller.Constans.SESS_ATTR_PAGE_NUMB;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;
import com.epam.newsmanagement.util.page.entity.Page;
import com.epam.newsmanagement.utils.controller.ControllerUtils;

@Controller
public class HomeController {

	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping(value = ACT_VAL_HOME, method = RequestMethod.GET)
	public String home(Model model,
			@RequestParam(defaultValue = "1") int pageNumber,
			HttpSession session) throws ServiceException {
		NewsFilter filter = ControllerUtils.initFilter(session);
		Page page = newsManagementService.fetchListNews(PAGE_SIZE, pageNumber,
				filter);
		fillAttribute(session, model, page, pageNumber);
		return PAGE_HOME;
	}

	private void fillAttribute(HttpSession session, Model model, Page page,
			int pageNumber) throws ServiceException {
		session.setAttribute(SESS_ATTR_PAGE, page);
		session.setAttribute(SESS_ATTR_PAGE_NUMB, pageNumber);

		List<AuthorTO> authorTOList = newsManagementService.fetchAuthors();
		model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authorTOList);

		List<TagTO> tagTOList = newsManagementService.fetchTags();
		model.addAttribute(MODEL_ATTR_TAG_LIST, tagTOList);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = ACT_VAL_HOME, method = RequestMethod.POST)
	public String filter(Model model,
			@RequestParam(defaultValue = "1") int pageNumber,
			@RequestParam(required = false, defaultValue = "0") Long authorId,
			@RequestParam(required = false) List<Long> tagIds,
			HttpSession session) throws ServiceException {

		tagIds = (List<Long>) (tagIds == null ? Collections.emptyList()
				: tagIds);

		NewsFilter filter = new NewsFilter();
		filter.setAuthorIdFilter(authorId);
		filter.setTagIdsFilter(tagIds);
		session.setAttribute(SESS_ATTR_FILTER, filter);

		Page page = newsManagementService.fetchListNews(PAGE_SIZE, pageNumber,
				filter);

		fillAttribute(session, model, page, pageNumber);

		return PAGE_HOME;
	}

	@RequestMapping(value = ACT_VAL_HOME_DELETE, method = RequestMethod.POST)
	public String deleteNews(
			@RequestParam(required = false) List<Long> newsSelectedList)
			throws ServiceException {
		List<Long> newsIds = newsSelectedList == null ? Collections.emptyList()
				: newsSelectedList;
		newsManagementService.deleteNewsListById(newsIds);
		return REDIRECT_HOME;
	}

	@RequestMapping(value = ACT_VAL_HOME_RESET, method = RequestMethod.GET)
	public String resetFilter(HttpSession session, Model model)
			throws ServiceException {

		Long authorId = 0L;
		List<Long> tagIds = Collections.emptyList();

		NewsFilter filter = new NewsFilter();
		filter.setAuthorIdFilter(authorId);
		filter.setTagIdsFilter(tagIds);

		session.setAttribute(SESS_ATTR_FILTER, filter);

		int pageNumber = 1;
		Page page = newsManagementService.fetchListNews(PAGE_SIZE, pageNumber,
				filter);
		fillAttribute(session, model, page, pageNumber);

		return PAGE_HOME;
	}
}
