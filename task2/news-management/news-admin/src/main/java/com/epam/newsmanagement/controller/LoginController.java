package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static com.epam.newsmanagement.controller.Constans.*;

@Controller
@RequestMapping(value = ACT_VAL_LOGIN)
public class LoginController {

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = REG_PARAM_ERROR, required = false) String error) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject(MODEL_ATTR_ERROR, error);
		}
		model.setViewName(PAGE_LOGIN);
		return model;
	}
}
