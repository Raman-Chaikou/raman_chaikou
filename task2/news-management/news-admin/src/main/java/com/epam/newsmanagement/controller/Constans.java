package com.epam.newsmanagement.controller;

public class Constans {

	//Url to action
	public static final String ACT_VAL_HOME = "/home";
	public static final String ACT_VAL_HOME_RESET = "/home/reset";
	public static final String ACT_VAL_NEWS_NEWS_ID = "/news/{newsId}";
	public static final String ACT_VAL_NEWS_ADD = "/news/add";
	public static final String ACT_VAL_AUTHOR = "/author";
	public static final String ACT_VAL_UPDATE = "/update";
	public static final String ACT_VAL_EXPIRE = "/expire";
	public static final String ACT_VAL_SAVE = "/save";
	public static final String ACT_VAL_NEWS_ID_EDIT = "/news/{newsId}/edit";
	public static final String ACT_VAL_HOME_DELETE = "/home/delete";
	public static final String ACT_VAL_LOGIN = "/login";
	public static final String ACT_VAL_TAG = "/tag";
	public static final String ACT_VAL_DELETE = "/delete";
	public static final String ACT_VAL_NEWS_NEWS_ID_DEL_COMM = "/news/{newsId}/delete/comment";
	
	//Path variable
	public static final String PATH_VAR_NEWS_ID = "newsId";
	
	//Attribute of session	
	public static final String SESS_ATTR_PAGE = "page";
	public static final String SESS_ATTR_PAGE_NUMB = "pageNumber";
	public static final String SESS_ATTR_AUTHOR_ID_SELECTED = "authorIdSelected";
	public static final String SESS_ATTR_TAG_ID_SELECTED = "tagIdsSelected";
	public static final String SESS_ATTR_FILTER = "filter";
	
	//Attributes of model	
	public static final String MODEL_ATTR_AUTHOR_LIST = "authors";
	public static final String MODEL_ATTR_TAG_LIST = "tags";
	public static final String MODEL_ATTR_COMMENT = "comment";
	public static final String MODEL_ATTR_NEWS_VO = "newsVO";
	public static final String MODEL_ATTR_PREV = "prev";
	public static final String MODEL_ATTR_NEXT = "next";
	public static final String MODEL_ATTR_NEWS_TO = "newsTO";
	public static final String MODEL_ATTR_CURR_DATE = "currDate";
	public static final String MODEL_ATTR_NEWS_ID = "newsId";
	public static final String MODEL_ATTR_AUTHOR_TO = "authorTO";
	public static final String MODEL_ATTR_AUTHOR = "author";
	public static final String MODEL_ATTR_NEWS_TO_DATE = "newsTODate";
	public static final String MODEL_ATTR_ERROR = "error";
	public static final String MODEL_ATTR_TAG_TO = "tagTO";
	public static final String MODEL_ATTR_TAG = "tag";
	public static final String MODEL_ATTR_NEWS_EDIT_AUTHOR_ID = "authorIdEdit";
	public static final String MODEL_ATTR_NEWS_EDIT_TAG_LIST = "tagIdsEdit";
	
	//Page	
	public static final String PAGE_HOME = "home";
	public static final String PAGE_ERROR = "error";
	public static final String PAGE_NEWS = "news";
	public static final String REDIRECT_TO_NEWS_NEWS_ID = "redirect:/news/{newsId}";
	public static final String PAGE_ADD_NEWS = "addNews";
	public static final String PAGE_AUTHOR = "author";
	public static final String REDIRECT_TO_AUTHOR = "redirect:/author";
	public static final String PAGE_EDIT_NEWS = "editNews";
	public static final String REDIRECT_HOME = "redirect:/home";
	public static final String PAGE_LOGIN = "login";
	public static final String PAGE_TAG = "tag";
	public static final String REDIRECT_TO_TAG = "redirect:/tag";
	
	//Program variable
	public static final int PAGE_SIZE = 3;
	
	//Request param
	public static final String REG_PARAM_ERROR = "error";
	public static final String REG_PARAM_COMMENT_ID = "commentId";
}
