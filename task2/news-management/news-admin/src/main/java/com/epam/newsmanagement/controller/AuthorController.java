package com.epam.newsmanagement.controller;

import static com.epam.newsmanagement.controller.Constans.ACT_VAL_AUTHOR;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_EXPIRE;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_SAVE;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_UPDATE;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_AUTHOR;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_AUTHOR_LIST;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_AUTHOR_TO;
import static com.epam.newsmanagement.controller.Constans.PAGE_AUTHOR;
import static com.epam.newsmanagement.controller.Constans.REDIRECT_TO_AUTHOR;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
@RequestMapping(value = ACT_VAL_AUTHOR)
public class AuthorController {

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	@Qualifier("authorValidator")
	private Validator validator;

	@RequestMapping(method = RequestMethod.GET)
	public String author(Model model) throws ServiceException {
		fieldAttributeModel(model);
		return PAGE_AUTHOR;
	}

	@RequestMapping(value = ACT_VAL_UPDATE, method = RequestMethod.GET)
	public String authorUpdate(Model model) throws ServiceException {
		fieldAttributeModel(model);
		return PAGE_AUTHOR;
	}

	private void fieldAttributeModel(Model model) throws ServiceException {
		List<AuthorTO> authors = newsManagementService.fetchNotExpiredAuthors();
		model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authors);

		AuthorTO authorTO = new AuthorTO();
		model.addAttribute(MODEL_ATTR_AUTHOR_TO, authorTO);

		AuthorTO author = new AuthorTO();
		model.addAttribute(MODEL_ATTR_AUTHOR, author);
	}

	@RequestMapping(value = ACT_VAL_UPDATE, method = RequestMethod.POST)
	public String authorUpdate(Model model,
			@ModelAttribute(MODEL_ATTR_AUTHOR_TO) @Validated AuthorTO authorTO,
			BindingResult bindingResult) throws ServiceException {
		validator.validate(authorTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			newsManagementService.editAuthor(authorTO);
			return REDIRECT_TO_AUTHOR;
		}
		AuthorTO author = new AuthorTO();
		model.addAttribute(MODEL_ATTR_AUTHOR, author);

		List<AuthorTO> authors = newsManagementService.fetchNotExpiredAuthors();
		model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authors);

		return PAGE_AUTHOR;
	}

	@RequestMapping(value = ACT_VAL_EXPIRE, method = RequestMethod.POST)
	public String authorExpire(Long authorIdExpire) throws ServiceException {
		newsManagementService.expireAuthor(authorIdExpire);
		return REDIRECT_TO_AUTHOR;

	}

	@RequestMapping(value = ACT_VAL_SAVE, method = RequestMethod.POST)
	public String authorSave(Model model,
			@ModelAttribute(MODEL_ATTR_AUTHOR) @Validated AuthorTO author,
			BindingResult bindingResult) throws ServiceException {
		validator.validate(author, bindingResult);
		if (!bindingResult.hasErrors()) {
			newsManagementService.addAuthor(author);
			return REDIRECT_TO_AUTHOR;
		}
		AuthorTO authorTO = new AuthorTO();
		model.addAttribute(MODEL_ATTR_AUTHOR_TO, authorTO);

		List<AuthorTO> authors = newsManagementService.fetchNotExpiredAuthors();
		model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authors);

		return PAGE_AUTHOR;
	}

	@RequestMapping(value = ACT_VAL_SAVE, method = RequestMethod.GET)
	public String authorSave(Model model) throws ServiceException {
		fieldAttributeModel(model);
		return PAGE_AUTHOR;
	}
}
