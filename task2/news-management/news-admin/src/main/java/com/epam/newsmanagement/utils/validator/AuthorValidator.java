package com.epam.newsmanagement.utils.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.to.AuthorTO;

public class AuthorValidator implements Validator{
	private static final int COUNT_CHARACTER = 30;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return AuthorTO.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		AuthorTO author = (AuthorTO) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorName", "valid.author.authorname");
		if(author.getAuthorName().length() > COUNT_CHARACTER){
			errors.rejectValue("authorName", "valid.author.authorname.size");
		}
	}
}
