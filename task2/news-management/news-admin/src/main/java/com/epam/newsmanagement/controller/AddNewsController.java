package com.epam.newsmanagement.controller;

import static com.epam.newsmanagement.controller.Constans.ACT_VAL_NEWS_ADD;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_AUTHOR_LIST;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_CURR_DATE;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_TO;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_TAG_LIST;
import static com.epam.newsmanagement.controller.Constans.PAGE_ADD_NEWS;
import static com.epam.newsmanagement.controller.Constans.REDIRECT_TO_NEWS_NEWS_ID;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.utils.controller.ControllerUtils;

@Controller
public class AddNewsController {

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	@Qualifier("newsValidator")
	private Validator validator;

	@RequestMapping(value = ACT_VAL_NEWS_ADD, method = RequestMethod.GET)
	public String createNews(Model model) throws ServiceException {
		NewsTO newsTO = new NewsTO();
		model.addAttribute(MODEL_ATTR_NEWS_TO, newsTO);

		Date date = new Date();
		model.addAttribute(MODEL_ATTR_CURR_DATE, date);

		List<AuthorTO> authors = newsManagementService.fetchNotExpiredAuthors();
		model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authors);

		List<TagTO> tags = newsManagementService.fetchTags();
		model.addAttribute(MODEL_ATTR_TAG_LIST, tags);

		return PAGE_ADD_NEWS;
	}

	@RequestMapping(value = ACT_VAL_NEWS_ADD, method = RequestMethod.POST)
	public String createNews(
			@ModelAttribute(MODEL_ATTR_NEWS_TO) @Validated NewsTO newsTO,
			@RequestParam(required = false) List<Long> tagIds,
			@RequestParam(required = false) Long authorId, Model model,
			BindingResult bindingResult) throws ServiceException {
		validator.validate(newsTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			tagIds = ControllerUtils.notNullList(tagIds);
			long newsId = newsManagementService.saveNews(newsTO, authorId,
					tagIds);
			model.addAttribute(MODEL_ATTR_NEWS_ID, newsId);
			return REDIRECT_TO_NEWS_NEWS_ID;
		}
		List<AuthorTO> authors = newsManagementService.fetchAuthors();
		model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authors);

		List<TagTO> tags = newsManagementService.fetchTags();
		model.addAttribute(MODEL_ATTR_TAG_LIST, tags);

		return PAGE_ADD_NEWS;
	}
}
