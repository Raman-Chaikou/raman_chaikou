package com.epam.newsmanagement.controller;

import static com.epam.newsmanagement.controller.Constans.ACT_VAL_DELETE;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_SAVE;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_TAG;
import static com.epam.newsmanagement.controller.Constans.ACT_VAL_UPDATE;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_TAG;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_TAG_LIST;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_TAG_TO;
import static com.epam.newsmanagement.controller.Constans.PAGE_TAG;
import static com.epam.newsmanagement.controller.Constans.REDIRECT_TO_TAG;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
@RequestMapping(value = ACT_VAL_TAG)
public class TagController {

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	@Qualifier("tagValidator")
	private Validator validator;

	@RequestMapping(method = RequestMethod.GET)
	public String tag(Model model) throws ServiceException {
		fillAttribute(model);
		return PAGE_TAG;
	}

	@RequestMapping(value = ACT_VAL_UPDATE, method = RequestMethod.GET)
	public String tagUpdate(Model model) throws ServiceException {
		fillAttribute(model);
		return PAGE_TAG;
	}

	private void fillAttribute(Model model) throws ServiceException {
		TagTO tagTO = new TagTO();
		model.addAttribute(MODEL_ATTR_TAG_TO, tagTO);

		TagTO tag = new TagTO();
		model.addAttribute(MODEL_ATTR_TAG, tag);

		List<TagTO> tags = newsManagementService.fetchTags();
		model.addAttribute(MODEL_ATTR_TAG_LIST, tags);
	}

	@RequestMapping(value = ACT_VAL_UPDATE, method = RequestMethod.POST)
	public String tagUpdate(Model model,
			@ModelAttribute(MODEL_ATTR_TAG_TO) @Validated TagTO tagTO,
			BindingResult bindingResult) throws ServiceException {
		validator.validate(tagTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			newsManagementService.editTag(tagTO);
			return REDIRECT_TO_TAG;
		}
		TagTO tag = new TagTO();
		model.addAttribute(MODEL_ATTR_TAG, tag);

		List<TagTO> tags = newsManagementService.fetchTags();
		model.addAttribute(MODEL_ATTR_TAG_LIST, tags);
		return PAGE_TAG;
	}

	@RequestMapping(value = ACT_VAL_DELETE, method = RequestMethod.POST)
	public String tagDelete(Model model, Long tagIdDelete)
			throws ServiceException {
		TagTO tag = new TagTO();
		model.addAttribute(MODEL_ATTR_TAG, tag);
		newsManagementService.deleteTag(tagIdDelete);
		return REDIRECT_TO_TAG;
	}

	@RequestMapping(value = ACT_VAL_SAVE, method = RequestMethod.POST)
	public String tagSave(Model model,
			@ModelAttribute(MODEL_ATTR_TAG) @Validated TagTO tag,
			BindingResult bindingResult) throws ServiceException {
		validator.validate(tag, bindingResult);
		if (!bindingResult.hasErrors()) {
			newsManagementService.addTag(tag);
			return REDIRECT_TO_TAG;
		}
		TagTO tagTO = new TagTO();
		model.addAttribute(MODEL_ATTR_TAG_TO, tagTO);

		List<TagTO> tags = newsManagementService.fetchTags();
		model.addAttribute(MODEL_ATTR_TAG_LIST, tags);
		return PAGE_TAG;
	}
}
