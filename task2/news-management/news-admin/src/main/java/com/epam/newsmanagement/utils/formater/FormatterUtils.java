package com.epam.newsmanagement.utils.formater;

import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class FormatterUtils {
	private static final String PROPERTIES_NAME = "format";
	private static final String PROPERTIES_KEY_PATTERN_DATE = "pattern.date";
	private static HashMap<Locale, String> mapPatterDate = new HashMap<Locale, String>();
		
	public static String takePatternDate(Locale locale){
		String patternDate = null;
		if(mapPatterDate.containsKey(locale)){
			patternDate = mapPatterDate.get(locale);
		} else {
			String properties = PROPERTIES_NAME;
			ResourceBundle resourceBundle = ResourceBundle.getBundle(properties, locale);
			patternDate = (String) resourceBundle.getObject(PROPERTIES_KEY_PATTERN_DATE);
		}
		return patternDate;
	}
}
