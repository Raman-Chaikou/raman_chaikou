package com.epam.newsmanagement.utils.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.to.CommentTO;

public class CommentValidator implements Validator{

	private static final int COUNT_CHARACTER = 100;
	
	@Override
	public boolean supports(Class<?> paramClass) {
		return CommentTO.class.equals(paramClass);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "commentText", "valid.comment.commenttext");
		CommentTO comment = (CommentTO) object;
		if(comment.getCommentText().length() > COUNT_CHARACTER){
			errors.rejectValue("commentText", "valid.comment.commenttext.size");
		}
	}
	
}
