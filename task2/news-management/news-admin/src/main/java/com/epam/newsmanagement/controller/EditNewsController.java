package com.epam.newsmanagement.controller;

import static com.epam.newsmanagement.controller.Constans.ACT_VAL_NEWS_ID_EDIT;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_AUTHOR_LIST;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_EDIT_AUTHOR_ID;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_EDIT_TAG_LIST;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_TO;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_NEWS_TO_DATE;
import static com.epam.newsmanagement.controller.Constans.MODEL_ATTR_TAG_LIST;
import static com.epam.newsmanagement.controller.Constans.PAGE_EDIT_NEWS;
import static com.epam.newsmanagement.controller.Constans.PATH_VAR_NEWS_ID;
import static com.epam.newsmanagement.controller.Constans.REDIRECT_TO_NEWS_NEWS_ID;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.entity.vo.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.utils.controller.ControllerUtils;

@Controller
@RequestMapping(value = ACT_VAL_NEWS_ID_EDIT)
public class EditNewsController {
		
	@Autowired
	private NewsManagementService newsManagementService;
	
	@Autowired
	@Qualifier("newsValidator")
	private Validator validator;
	
	@RequestMapping(method = RequestMethod.GET)
	public String editNews(
			Model model, 
			@PathVariable(PATH_VAR_NEWS_ID) Long newsId) throws ServiceException{
		
		NewsVO newsVO = newsManagementService.takeNews(newsId);
		
		NewsTO newsTO = newsVO.getNews();
		model.addAttribute(MODEL_ATTR_NEWS_TO, newsTO);
		
		Date newsTODate = newsTO.getModificationDate();
		model.addAttribute(MODEL_ATTR_NEWS_TO_DATE, newsTODate);
		
		Long authorId = newsVO.getAuthor().getAuthorId();
		model.addAttribute(MODEL_ATTR_NEWS_EDIT_AUTHOR_ID, authorId);
					
		fillAttribute(model, newsVO);
						
		return PAGE_EDIT_NEWS;
	}
	
	private void addAuthorNews(List<AuthorTO> authors, Long authorId)
			throws ServiceException{
		AuthorTO author = newsManagementService.fetchAuthor(authorId);
		if(!authors.contains(author)){
			authors.add(author);
		}
	}
	
	private List<Long> fetchTagIds(NewsVO newsVO){
		List<Long> tagIds = new ArrayList<Long>();
		for (TagTO tag : newsVO.getTags()) {
			tagIds.add(tag.getTagId());
		}
		return tagIds;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String editNews(
			@RequestParam(required = false) List<Long> tagIds,
			@RequestParam(required = false) Long authorId,
			Model model,
			@ModelAttribute(MODEL_ATTR_NEWS_TO) @Validated NewsTO newsTO,
			BindingResult bindingResult) throws ServiceException{
		validator.validate(newsTO, bindingResult);
		if(!bindingResult.hasErrors()){
			tagIds = ControllerUtils.notNullList(tagIds);
			newsManagementService.editNews(newsTO, authorId, tagIds);
			return REDIRECT_TO_NEWS_NEWS_ID;
		}
		
		NewsVO newsVO = newsManagementService.takeNews(newsTO.getNewsId());
		fillAttribute(model, newsVO);			
		return PAGE_EDIT_NEWS;
	}
	
	private void fillAttribute(Model model, NewsVO newsVO) throws ServiceException{
				
		List<Long> tagIdsNews = fetchTagIds(newsVO);
		model.addAttribute(MODEL_ATTR_NEWS_EDIT_TAG_LIST, tagIdsNews);
		
		List<AuthorTO> authors = newsManagementService.fetchNotExpiredAuthors();
		
		Long authorId = newsVO.getNews().getNewsId();
		
		addAuthorNews(authors, authorId);
		model.addAttribute(MODEL_ATTR_AUTHOR_LIST, authors);
		
		List<TagTO> tags = newsManagementService.fetchTags();
		model.addAttribute(MODEL_ATTR_TAG_LIST, tags);
	}
}
