package com.epam.newsmanagement.utils.formater;

import java.util.Date;
import java.util.Locale;

import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

public class FormatterDateFactory extends FormattingConversionServiceFactoryBean{
	
	@SuppressWarnings("deprecation")
	public void installFormatters(FormatterRegistry registry){
		super.installFormatters(registry);
		registry.addFormatterForFieldType(Date.class, new DateFormatter());
	}
}
