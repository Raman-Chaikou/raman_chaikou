<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%-- <%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %> --%>
<html>
<head>
	<link href="<c:url value='/css/style.css'/>" rel="stylesheet">
	<script type="text/javascript" src='<c:url value="/js/filterTagsAdmin.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/js/updateAuthor.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/js/deleteWindow.js"/>'></script>
	<!-- <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> -->
<title></title>
</head>
<body>
	<div class="container-layout">
		<table style="height: 100%; width: 100%; border: 2px solid; border-collapse: collapse;">
			<tr style="border-bottom: 2px solid;">
				<td colspan="2" class="container-header">
					<tiles:insertAttribute name="header"/>
				</td>
			</tr>
			<tr style="border-bottom: 2px solid;">
				<td style="width: 25%; border-top: 1px dotted; position: relative; vertical-align: top;">
				<div style="background-color: rgb(140, 140, 145);
							  width: 97%;
							  height: 150px;
							  margin-top: 3px;
							  position: absolute;">
					<div style="border: 0px;
								  border-top: 1px dotted;
								  background-color: white;
								  height: 85%;
								  margin-left: 15px;
  								  margin-right: 15px;" >
						<tiles:insertAttribute name="menu"/>
					</div>
				</div>
				</td>
				<td class="container-body" style="width: 75%; vertical-align: top;">
					<div class="container-body-inner" style="/* margin: 0px; */ margin-left: -7px;">
						<tiles:insertAttribute name="body"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="container-footer">
					<tiles:insertAttribute name="footer"/>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>