<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<table style="width: 100%;">
  <tr>
    <td  style="color:blue; width: 70%; padding-left: 30px;" align="left">
    	<h1><spring:message code="news.title"/></h1>
    </td>
    <td style="width: 30%;" align="right">
    	<table >
    	<security:authorize access="hasRole('ROLE_ADMIN')">
    		<tr align="center">
    			<td style="padding-bottom: 15px; padding-right: 20px;">
    				<spring:message code="title.hello"/>, <security:authentication property="principal.username"/>
    			</td>
    			<td>
					<form action='<c:url value="/logout"/>'>
			    		<input type="submit" value='<spring:message code="name.button.logout"/>'>
			    	</form>    				
    			</td>
    		</tr>
    	</security:authorize>
    		<tr>
    			<td colspan="2" align="right">
					<a href="?language=en">EN</a>
					<a href="?language=ru">RU</a>    				
    			</td>
    		</tr>
    	</table>
    </td>
  </tr>
</table>


