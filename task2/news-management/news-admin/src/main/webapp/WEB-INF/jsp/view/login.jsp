<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div align="center" style="padding-top: 200px;">
<form method="post" action='<c:url value="/j_spring_security_check"/>'>

	<table>
		<tr>
			<td colspan="2" align="right">
				<c:if test="${error != null}">
					<spring:message code="message.error.login"/>
				</c:if>			
			</td>
		</tr>
		<tr>
			<td align="left">
				<spring:message code="title.login"/>:
			</td>
			<td>
				<input name="j_username" type="text">			
			</td>
		</tr>
		<tr>
			<td align="left">
				<spring:message code="title.password"/>:
			</td>
			<td>
				<input name="j_password" type="password">			
			</td>
		</tr>
		<tr>
			<td align="right" colspan="2">
				<input value='<spring:message code="name.button.login"/>' type="submit">			
			</td>
		</tr>
	</table>	
</form>

</div>