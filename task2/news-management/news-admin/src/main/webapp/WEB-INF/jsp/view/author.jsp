<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div style="margin-left: 10px;">
<div style="height: 80%;">
<table>
	<tr>
		<td colspan="2" style="height: 40px;">
			<form:form commandName="authorTO">
				<form:errors  path="authorName" cssClass="container-error-text"/>
			</form:form>		
		</td>
	</tr>
	<c:forEach items="${authors}" var="author">
	  <tr>
	    <td align="left" style=" height: 40px; width: 52px;">
	    	<spring:message code="name.title.author"/>:
	    </td>
	    <td align="left">
	    	<c:url var="pathAuthorUpdate" value="/author/update"/>
	    	<form:form commandName="authorTO" method="post" cssClass="form-text" action="${pathAuthorUpdate}"  name="authorForm${author.authorId}" >
	    		<form:input type="text" cssClass="text-author-width" path="authorName" id="text${author.authorId}"  disabled="true" value="${author.authorName}"/>
	    		<form:hidden path="authorId" value="${author.authorId}"/>
	    	</form:form>
	    	<form action='<c:url value="/author/expire"/>' method="post" name="expiredAuth${author.authorId}">
	    		<input type="hidden" name="authorIdExpire" value="${author.authorId}">
	    	</form>
	    </td>
	    <td align="left" style="width: 110px;">
	    	<a href="#" onclick="edit(this)"  id="edit+${author.authorId}">
	    		<spring:message code="name.admin.edit"/>
	    	</a>
			<table>
				<tr>
					<td>
				    	<a href="#" onclick="document.forms.authorForm${author.authorId}.submit();update(this)" style="display: none;" id="action1+${author.authorId}">
				    		<spring:message code="name.update"/>
				    	</a>					
					</td>
					<td>
						<spring:message code="block.author.message" var="blockAuthorMessage"/>
				    	<a href="#" onclick="showAskWindow('${blockAuthorMessage}', document.forms.expiredAuth${author.authorId}); " style="display: none;" id="action2+${author.authorId}">
				    		<spring:message code="name.expire"/>
				    	</a>					
					</td>
					<td>
						<a href="#" onclick="cancel(this)" style="display: none" id="action3+${author.authorId}">
							<spring:message code="name.cancel"/>
						</a>
					</td>
				</tr>
			</table>
	    </td>
	  </tr>
	</c:forEach>
</table>
</div>
<div style="bottom: 0; height: 20%;">
<table style="margin-top: 5px; margin-left: -20px;">
	<tr>
		<td colspan="2" style="height: 40px;">
		<form:form commandName="author" cssClass="author-text-error">
			<form:errors path="authorName" cssClass="container-error-text"/>
		</form:form>
		</td>
	</tr>
	<tr>
		<td align="left" style="padding-right: 10px; height: 40px; width: 130px;">
			<spring:message code="name.addauthor"/>:
		</td>
		<td align="left">
			<c:url var="pathAuthorSave" value="/author/save"/>
			<form:form commandName="author" name="authorSave" cssClass="form-text" action="${pathAuthorSave }">
				<form:input path="authorName" cssClass="text-author-width"/>
			</form:form>
		</td>
		<td align="left">
			<a href="#" onclick="document.forms.authorSave.submit()">
				<spring:message code="name.save"/>
			</a>
		</td>
	</tr>
</table>
</div>
</div>