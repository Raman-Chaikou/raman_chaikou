<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div style="margin-left: 20px;">
<div style="height: 80%;">
<table>
	<tr>
		<td colspan="2" style="height: 40px;">
			<form:form commandName="tagTO">
				<form:errors  path="tagName" cssClass="container-error-text"/>
			</form:form>		
		</td>
	</tr>

	<c:forEach items="${tags}" var="tag">
	  <tr>
	    <td align="left" style="padding-right: 2px; height: 40px; width: 40px;">
	    	<spring:message code="name.title.tag"/>:
	    </td>
	    <td align="left">
	    	<c:url var="pathTagUpdate" value="/tag/update"/>
	    	<form:form commandName="tagTO" method="post" cssClass="form-text" action="${pathTagUpdate}"  name="tagForm${tag.tagId}" >
	    		<form:input type="text" cssClass="text-author-width" path="tagName" id="text${tag.tagId}"  disabled="true" value="${tag.tagName}"/>
	    		<form:hidden path="tagId" value="${tag.tagId}"/>
	    	</form:form>
	    	<form action='<c:url value="/tag/delete"/>' method="post" name="deleteTag${tag.tagId}">
	    		<input type="hidden" name="tagIdDelete" value="${tag.tagId}">
	    	</form>
	    </td>
	    <td align="left" style="width: 102px;">
	    	<a href="#" onclick="edit(this)"  id="edit+${tag.tagId}">
	    		<spring:message code="name.admin.edit"/>
	    	</a>
			<table>
				<tr>
					<td>
				    	<a href="#" onclick="document.forms.tagForm${tag.tagId}.submit();update(this)" style="display: none;" id="action1+${tag.tagId}">
				    		<spring:message code="name.update"/>
				    	</a>					
					</td>
					<td>
						<spring:message code="delete.tag.message" var="deleteTagMessage"/>
				    	<a href="#" onclick="showAskWindow('${deleteTagMessage}',document.forms.deleteTag${tag.tagId}); " style="display: none;" id="action2+${tag.tagId}">
				    		<spring:message code="name.delete"/>
				    	</a>					
					</td>
					<td>
						<a href="#" onclick="cancel(this)" style="display: none" id="action3+${tag.tagId}">
							<spring:message code="name.cancel"/>
						</a>
					</td>
				</tr>
			</table>
	    </td>
	  </tr>
	</c:forEach>
</table>
</div>
<div style="bottom: 0; height: 20%;" >
<table style=" margin-left: -20px;">
	<tr>
		<td colspan="2" style="height: 40px;">
			<form:form commandName="tag" cssClass="author-text-error">
				<form:errors path="tagName" cssClass="container-error-text"/>
			</form:form>		
		</td>
	</tr>
	<tr>
		<td align="left" style="padding-right: 10px; height: 40px; width: 130px;">
			<spring:message code="name.addtag"/>:
		</td>
		<td align="left">
			<c:url var="pathTagSave" value="/tag/save"/>
			<form:form commandName="tag" name="tagSave" cssClass="form-text" action="${pathTagSave }">
				<form:input path="tagName" cssClass="text-author-width"/>
			</form:form>
		</td>
		<td align="left">
			<a href="#" onclick="document.forms.tagSave.submit()">
				<spring:message code="name.save"/>
			</a>
		</td>
	</tr>
</table>
</div>
</div>