<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div >
<spring:message code="pattern.date" var="dataStyle" scope="page"/>
<div class="container-news">
	<table class="container-home-body-table cont-nbt-info">
		<tr>
			<td colspan="2" class="cont-hbt-title" align="left">${newsVO.news.title}</td>
		</tr>
		<tr>
			<td class="cont-hbt-name" align="left">
				(<spring:message code="name.by"/> ${newsVO.author.authorName})
			</td>
			<td class="cont-hbt-date" align="right">
				<fmt:formatDate value="${newsVO.news.modificationDate}"
								pattern="${dataStyle }"/>
				
			</td>		
		</tr>
		<tr>
			<td class="cont-nbt-table-info-td-text" colspan="2" align="left">
				${newsVO.news.fullText}
			</td>
		</tr>
	</table>
	<table class="cont-news-table-comm">
		<c:forEach items="${newsVO.comments}" var="newsComment">
			<tr>
				<td class="cont-nbt-td-comment">
					<div class="cont-hbt-date">
					<fmt:formatDate value="${newsComment.creationDate}" 
									pattern="${dataStyle }"/>
					</div>
					<div class="cont-nbt-td-colortext" style="min-height: 30px;
														  display: inline-table;
														  padding-top: 5px;">
						<div style="float: right;">
							<form name="formDeleteComment${newsComment.commentId}" action='<c:url value="/news/${newsVO.news.newsId}/delete/comment"/>' method="post">
							<input type="hidden" name="commentId" value="${newsComment.commentId}">
								<spring:message code="delete.comment.message" var="deleteCommMessage"/>
								<input type="button" 
									onclick="showAskWindow('${deleteCommMessage}', document.forms.formDeleteComment${newsComment.commentId})"  
									value="x">
							</form>
						</div>
						${newsComment.commentText}
					</div>
				</td>
			</tr>
		</c:forEach>
		<tr>
			<td>
				<form:form method="POST" commandName="comment">
					<table class="cont-nbt-table-textarea">
						<tr>
							<td class="cont-nbt-table-textarea-td">
								<form:textarea path="commentText" cssClass="cont-nbt-textarea"/>
							</td>
							<td>
								<form:errors path="commentText" cssClass="container-error-text"/>
							</td>
						</tr>
						<tr>
							<td align="right" colspan="1">
								<input type="submit" value="<spring:message code="name.postcomment"/>">
							</td>
						</tr>
					</table>
				</form:form>
			</td>
		</tr>
	</table>
</div>
<div class="container-news-pr-next">
	<table class="container-home-body-table">
		<tr>
			<td align="left">
				<c:if test="${not empty prev}">
				<a href="<c:url value="/news/${prev.newsId}"/>">
					<spring:message code="name.prev"/>
				</a>
				</c:if>
			</td>
			<td align="right">
				<c:if test="${not empty next}">
				<a href="<c:url value="/news/${next.newsId}"/>">
					<spring:message code="name.next"/>
				</a>
				</c:if>
			</td>
		</tr>
	</table>
</div>
</div>