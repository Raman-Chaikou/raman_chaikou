<%@taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div style="margin-top: 15px; margin-left: 20px;">
<form:form commandName="newsTO" method="post">
	<table>
		<tr>
			<td colspan="2" style="height: 20px;">
				<form:errors path="title" cssClass="container-error-text"/>
			</td>
		</tr>
		<tr>
			<td class="table-td-new-news">
				<spring:message code="name.title"/>:
			</td>
			<td align="left">
				<form:input path="title" cssClass="text-news-width"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="height: 20px;">
				<form:errors path="modificationDate" cssClass="container-error-text"/>
			</td>
		</tr>
		<tr>
			<td align="left" class="table-td-new-news">
				<spring:message code="name.date"/>:
			</td>
			<td align="left">
				<spring:message code="pattern.date" var="dataStyle" scope="page"/>
				<spring:message code="pattern.date.regular" var="regularData" scope="page"/>
				<fmt:formatDate value="${currDate}" pattern="${dataStyle}"  var="formDate"/>
				<form:input path="modificationDate" pattern="${regularData}" type="text" value="${formDate}" />
				<form:hidden path="creationDate" value="${formDate}"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="height: 20px;">
				<form:errors path="shortText" cssClass="container-error-text"/>
			</td>			
		</tr>
		<tr>
			<td align="left" class="table-td-new-news">
				<spring:message code="name.brief"/>:
			</td>
			<td align="left">
				<form:textarea path="shortText" cssClass="text-news-width text-news-height-brief"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="height: 20px;">
				<form:errors path="fullText" cssClass="container-error-text"/>
			</td>			
		</tr>
		<tr>
			<td align="left" class="table-td-new-news">
				<spring:message code="name.content"/>:
			</td>
			<td align="left">
				<form:textarea path="fullText" cssClass="text-news-width text-news-height-content"/>
			</td>

		</tr>
	</table>
	<table style="width: 100%; margin-top: 15px;">
		<tr>
			<td>
			<table style="width: 100%;">
				<tr>
					<td align="right">
						<div style="
								  margin-left: 10px;
								  margin-right: 5px;">
							<select name="authorId" style="width: 170px;">	
								<c:forEach items="${authors}" var="author">
											<option value="${author.authorId}"
											<c:if test="${authorIdSelected == author.authorId or author.authorId == 1}"> 
														selected
													</c:if> > 
												${author.authorName}
											</option>
								</c:forEach>
							</select>
						</div>					
					</td>
					<td align="left">
						<div class="multiselect" style="margin-right: 5px; width: 150px;">
							<div class="selectBox" onclick="showCheckboxes()">
								<select>
									<option>
										<spring:message code="filter.tag.message"/>
									</option>
								</select>
								<div class="overSelect"></div>
							</div>
							<div id="checkboxes" style="width: 150px;">
								<c:forEach items="${tags}" var="tag">
									<label class="checkBTag" for="${tag.tagId}" >
										<input type="checkbox" name="tagIds" value="${tag.tagId}" class="checkBTag" 
												id="${tag.tagId}" 
												
											<c:if test="${ not empty tagIdsSelected and tagIdsSelected.contains(tag.tagId) }">
												checked
											</c:if>/>
										${tag.tagName}
									</label>
								</c:forEach>
							</div>
						</div>					
					</td>
				</tr>
			</table>				
			</td>
		</tr>
		<tr>
			<td align="right" style="padding-top: 10px;">
				<input type="submit" value="<spring:message code="name.button.save"/>">
			</td>
		</tr>		
	</table>
</form:form>
</div>