<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<ul style="margin-left: -20px; list-style-image: url('<c:url value="/image/image-mini.jpg"/>');">
	
	<li style="padding-bottom: 10px;">
		<a href="<c:url value="/home?pageNumber=${pageNumber }"/>">
			<spring:message code="menu.item.newslist"/>
		</a>
	</li>

	<li style="padding-bottom: 10px;">
		<a href='<c:url value="/news/add"/>'>
			<spring:message code="menu.item.addnews"/>
		</a>
	</li>

	<li style="padding-bottom: 10px;">
		<a href='<c:url value="/author"/>'>
			<spring:message code="menu.item.add.updateauthor"/>
		</a>
	</li>

	<li style="padding-bottom: 10px;">
		<a href='<c:url value="/tag"/>'>
			<spring:message code="menu.item.add.updatetags"/>
		</a>
	</li>
</ul>