<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
	<link href='<c:url value="/css/style.css"/>' rel="stylesheet">
<title><tiles:insertAttribute name="title" ignore="true"/></title>
</head>
<body>
	<div class="container-layout">
		<table style="height: 100%; width: 100%; border: 2px solid; border-collapse: collapse;">
			<tr style="border-bottom: 2px solid;">
				<td class="container-header">
				<tiles:insertAttribute name="header"/>
				</td>
			</tr>
			<tr style="border-bottom: 2px solid;">
				<td class="container-body">
					<div class="container-body-inner">
						<tiles:insertAttribute name="body"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="container-footer">
				<tiles:insertAttribute name="footer"/>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>