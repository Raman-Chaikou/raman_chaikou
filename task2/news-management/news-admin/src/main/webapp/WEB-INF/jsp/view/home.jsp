<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<div class="container-home-header" align="center">
		<div style="margin-bottom: -15px; margin-left: 70px;">
			<form action="<c:url value="/home"/>" method="post" >
			<div style="float: left;
					  margin-left: 10px;
					  margin-right: 5px;">
				<select name="authorId" style="width: 170px;">
				
					<option value="0" <c:if test="${filter.authorIdFilter == 0 }">selected</c:if>>
						<spring:message code="filter.author.message"/>
					</option>	
					<c:forEach items="${authors}" var="author">
								<option value="${author.authorId}"
								<c:if test="${filter.authorIdFilter == author.authorId }"> 
											selected
										</c:if> > 
									${author.authorName}
								</option>
					</c:forEach>
				</select>
			</div>
			<div class="multiselect" style="float: left;margin-right: 5px; width: 150px;">
				<div class="selectBox" onclick="showCheckboxes()">
					<select>
						<option>
							<spring:message code="filter.tag.message"/>
						</option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes" style="width: 150px;">
					<c:forEach items="${tags }" var="tag">
						<label class="checkBTag" for="${tag.tagId}" >
							<input type="checkbox" name="tagIds" value="${tag.tagId}" class="checkBTag" 
									id="${tag.tagId}" 
									
								<c:if test="${ not empty filter.tagIdsFilter and filter.tagIdsFilter.contains(tag.tagId) }">
									checked
								</c:if>/>
							${tag.tagName}
						</label>
					</c:forEach>
				</div>
			</div>
			<div style="float: left;">
					<input type="submit" style="width: 70px;"  value="<spring:message code="name.filter"/>">
			</div>
			</form>
			<form  action="<c:url value="/home/reset"/>" method="get">
					<input type="submit" style="width: 75px;" value="<spring:message code="name.reset"/>">
			</form>	
		</div>
		<br>		
	</div>
	<div class="container-home-body" align="center">
	<c:if test="${not empty page.newsVOList }">
		<form name="formDeleteNews" action='<c:url value="/home/delete"/>' method="post">
		<c:forEach items="${page.newsVOList}" var="newsVO">
		<div>
			<table  class="container-home-body-table">
				<tr>
					<td class="cont-hbt-title" align="left">
						<a href='<c:url value="/news/${newsVO.news.newsId}"/>'>
							${newsVO.news.title}
						</a>						
					</td>
					<td class="cont-hbt-name" align="center">
						(<spring:message code="name.by"/> ${newsVO.author.authorName})
					</td>
					<td class="cont-hbt-date" colspan="2" align="right">
						<spring:message code="pattern.date" var="dataStyle" scope="page"/>
						<fmt:formatDate value="${newsVO.news.modificationDate}" pattern="${dataStyle}"/>
					</td>
				<tr>
				<tr>
					<td colspan="4" align="left">${newsVO.news.shortText}</td>
				</tr>
				<tr>
					<td class="cont-hbt-taglist" style="width: 50%;" colspan="2" align="right">
						<c:forEach items="${newsVO.tags}" var="tag">
							${tag.tagName} 
						</c:forEach>
					</td>
					<td class="cont-hbt-commentlist" align="center" style="width: 0%;">
						<spring:message code="name.comments"/>(${newsVO.comments.size()})
					</td>
					<td class="cont-hbt-view" align="right" style="width: 25%;">
						<a href='<c:url value="/news/${newsVO.news.newsId}/edit"/>'>
							<spring:message code="name.edit"/>
						</a>
						<input type="checkbox" name="newsSelectedList" value="${newsVO.news.newsId}">
					</td>
				</tr>
			</table>
		</div>
		<br>
		<br>
		</c:forEach>
		<div style="float: right;">
		<spring:message code="delete.news.message" var="deleteMessage"/>
		<spring:message code="delete.news.message.choose" var="chooseDelete"/>
			<input type="button"  onclick="deleteNews(document.forms.formDeleteNews,'${chooseDelete}','${deleteMessage}');" value='<spring:message code="name.button.delete"/>'>
		</div>
		</form>
	</c:if>
	</div>
	<div class="container-home-footer" align="center">
		<div>
			<form class="submit-pag" action="<c:url value="/home"/>" method="get">
				<c:if test="${not empty page.newsVOList }">
					<c:forEach begin="1" end="${page.countPage}" var="i">
						<input type="submit" name="pageNumber" value="${i}">
					</c:forEach>
				</c:if>
			</form>
		</div>
	</div>