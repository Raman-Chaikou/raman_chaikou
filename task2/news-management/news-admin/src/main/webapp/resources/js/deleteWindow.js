function showAskWindow(text, form){
	var answer = confirm(text);
	if(answer){
		form.submit();
	}	
}
function deleteNews(form, message1, message2){
	var newsSelectedIdList = form.newsSelectedList;
	var idSelected = false;

	if(newsSelectedIdList instanceof HTMLInputElement){
		if(newsSelectedIdList.checked == true){
			idSelected = true;
		}
	} else {
		for(i = 0; i < newsSelectedIdList.length; i++){
			if(newsSelectedIdList[i].checked == true){
				idSelected = true;
			}
		}		
	}

	
	if(!idSelected){
		alert(message1);
	} else {
		var answer = confirm(message2);
		if(answer){
			form.submit();
		}
	}
}
