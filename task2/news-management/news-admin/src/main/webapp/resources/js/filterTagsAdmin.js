var expanded = false;
function showCheckboxes(){
	var checkboxes = document.getElementById("checkboxes");
	if(!expanded){
		checkboxes.style.display = "block";
		expanded = true;
	} else {
		checkboxes.style.display = "none";
		expanded = false;
	}
}

document.onclick = function (e) {
	var elem = e ? e.target : window.event.srcElement;
	if((elem.className != "overSelect")){
		if (elem.className != "checkBTag") {
			var checkboxes = document.getElementById("checkboxes");
			checkboxes.style.display = "none";
			expanded = false;
		}

	}
}