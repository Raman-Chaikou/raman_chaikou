package com.epam.newsmanagement.util.page.entity;

import java.io.Serializable;
import java.util.List;

import com.epam.newsmanagement.entity.vo.NewsVO;

public class Page implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1028359307591756718L;
	
	private int countPage;
	
	private List<NewsVO> newsVOList;
	
	public int getCountPage() {
		return countPage;
	}

	public void setCountPage(int pageCount) {
		this.countPage = pageCount;
	}

	public List<NewsVO> getNewsVOList() {
		return newsVOList;
	}
	public void setNewsVOList(List<NewsVO> newsVOList) {
		this.newsVOList = newsVOList;
	}
	
	
}
