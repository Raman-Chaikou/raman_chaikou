package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

/**
 * The Class TagServiceImpl.
 */
@Service("tagService")
public class TagServiceImpl implements TagService{
	
	/** The tagDao use to communicate with DAO . */
	@Autowired
	private TagDAO tagDAO;

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommonService#create(java.lang.Object)
	 */
	@Override
	public long create(TagTO tag) throws ServiceException {
		try {
			return tagDAO.create(tag);
		} catch (DAOException e) {
			throw new ServiceException("Can't create this tag : " + tag, e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommonService#read(long)
	 */
	@Override
	public TagTO fetch(long id) throws ServiceException {
		try {
			return tagDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Can't read this tag : " + id, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommonService#update(java.lang.Object)
	 */
	@Override
	public void update(TagTO tag) throws ServiceException {
		try {
			 tagDAO.update(tag);
		} catch (DAOException e) {
			throw new ServiceException("Can't update this tag : " + tag, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommonService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
				tagDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Can't delete this tag : " + id, e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.TagService#searchTagsByNewsId(long)
	 */
	@Override
	public List<TagTO> searchTagsByNewsId(long newsId) throws ServiceException{
		try {
			return tagDAO.searchTagsByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't find the tags by this news id : " 
											+ newsId, e);
		}
	}

	@Override
	public List<TagTO> fetchTags() throws ServiceException {
		try {
			return tagDAO.fetchTags();
		} catch (DAOException e) {
			throw new ServiceException("Can't fetch list of tags.", e);
		}
	}
}
