package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.entity.vo.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;
import com.epam.newsmanagement.util.page.entity.Page;

/**
 * The Interface GeneralService.
 */
public interface NewsManagementService {
	
	/**
	 * Save news.
	 *
	 * @param news the news that should be save
	 * @param authorId the author id 
	 * @param tagList the list of the tags which should be save
	 * @throws ServiceException when you have some troubles with
	 * saving the information
	 */
	long saveNews(NewsTO news, long authorId, List<Long> tagList) 
			throws ServiceException;
	

	void editNews(NewsTO news, long authorId, List<Long> tagIdList) throws ServiceException;
		
	/**
	 * Fetch list news.
	 *
	 * @return the list
	 * @throws ServiceException when you have some troubles with
	 * fetching the news.
	 */
	Page fetchListNews(int pageSize, int pageNumber, NewsFilter filter) throws ServiceException;
	
	List<NewsTO> fetchNewsAndNeighbor(long newsId, NewsFilter filter) throws ServiceException;
		
	/**
	 * View news.
	 *
	 * @param newsId the id of a news that should be received
	 * @return the newsVO
	 * @throws ServiceException when you have some troubles with
	 * receiving the news
	 */
	NewsVO takeNews(long newsId) throws ServiceException;
	
	/**
	 * Adds the news author.
	 *
	 * @param author the author that should be added
	 * @throws ServiceException when you have some troubles with
	 * addition the author
	 */
	void addAuthor(AuthorTO author) throws ServiceException;
				
	/**
	 * Adds the comment.
	 *
	 * @param comment the comment that should be added
	 * @throws ServiceException when you have some troubles with 
	 * addition the comment
	 */
	void addComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Delete comment.
	 *
	 * @param commentId the id of comment that should be removed
	 * @throws ServiceException when you have some troubles with
	 * removal the element
	 */
	void deleteComment(long commentId) throws ServiceException;
	
	List<AuthorTO> fetchAuthors() throws ServiceException;
	
	List<AuthorTO> fetchNotExpiredAuthors() throws ServiceException;
	
	List<TagTO> fetchTags() throws ServiceException;
	
	void deleteNewsListById(List<Long> newsIdList) throws ServiceException;
	
	void editAuthor(AuthorTO author) throws ServiceException;
	
	void expireAuthor(long authorId) throws ServiceException;
	
	void editTag(TagTO tag) throws ServiceException;
	
	void deleteTag(long tagId) throws ServiceException;
	
	void addTag(TagTO tag) throws ServiceException;
	
	void attachAuthorToNews(long authorId, long newsId) throws ServiceException;
	
	void deleteNewsTagByNewsId(long newsId) throws ServiceException;
	
	void deleteNewsAuthorByNewsId(long newsId) throws ServiceException;
	
	AuthorTO fetchAuthor(long authorId) throws ServiceException;
}
