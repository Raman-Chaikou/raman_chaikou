package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;




/**
 * The Interface AuthorDAO.
 */
public interface AuthorDAO extends CommonDAO<AuthorTO>{
	/**
	 * Search authors by id of a news.
	 *
	 * @param newsId the id of a news which is used by search.
	 * @return the found of author
	 * @throws DAOException when you have some troubles with 
	 * search of the authors in database
	 */
	AuthorTO searchAuthorsByNewsId(long newsId) throws DAOException;
	
	List<AuthorTO> fetchAuthors() throws DAOException;
	
	List<AuthorTO> fetchNotExpiredAuthors() throws DAOException;
	
	void expireAuthor(long authorId) throws DAOException;
}
