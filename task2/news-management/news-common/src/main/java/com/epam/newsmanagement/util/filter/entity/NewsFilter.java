package com.epam.newsmanagement.util.filter.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class NewsFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3789015302202410479L;

	private Long authorIdFilter;
	
	private List<Long> tagIdsFilter;
	
	public NewsFilter(){
		tagIdsFilter = Collections.emptyList();
	}
	
	public NewsFilter(Long authorId, List<Long> tagIdList){
		this.authorIdFilter = authorId;
		this.tagIdsFilter = tagIdList;
	}
	
	public Long getAuthorIdFilter() {
		return authorIdFilter;
	}
	
	public void setAuthorIdFilter(Long authorIdFilter) {
		this.authorIdFilter = authorIdFilter;
	}
	
	public List<Long> getTagIdsFilter() {
		return tagIdsFilter;
	}
	
	public void setTagIdsFilter(List<Long> tagIdListFilter) {
		this.tagIdsFilter = tagIdListFilter;
	}
	@Override
	public String toString() {
		StringBuilder info = new StringBuilder();
		info.append("NewsFilter [authorIdFilter=");
		info.append(authorIdFilter);
		info.append(", tagIdsFilter=");
		info.append(tagIdsFilter);
		info.append("]");
		return info.toString();
	}
	
	
}
