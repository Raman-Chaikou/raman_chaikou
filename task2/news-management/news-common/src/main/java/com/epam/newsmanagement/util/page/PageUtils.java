package com.epam.newsmanagement.util.page;

public class PageUtils {
	public static int calculateMaxPositionNews(int pageSize, int pageNumber){
		return pageSize * pageNumber;
	}
	
	public static int calculateMinPositionNews(int pageSize, int maxPositionNews){
		return maxPositionNews - pageSize + 1;
	}
	
	public static int calculateCountPage(int pageSize, int countNews){
		return (int)Math.ceil((double)countNews / pageSize);
	}
}
