package com.epam.newsmanagement.entity.vo;

import java.util.List;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.to.TagTO;

/**
 * The Class NewsVO.
 */
public class NewsVO {
	
	/** The newsTO. */
	private NewsTO news;
	
	/** The authors list. */
	private AuthorTO author;
	
	/** The tags list. */
	private List<TagTO> tags;
	
	/** The comments list. */
	private List<CommentTO> comments;
	
	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public NewsTO getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news the new newsTO
	 */
	public void setNews(NewsTO news) {
		this.news = news;
	}
	
	/**
	 * Gets the comments list.
	 *
	 * @return the comments list
	 */
	public List<CommentTO> getComments() {
		return comments;
	}

	/**
	 * Sets the comments list.
	 *
	 * @param comments the new comments list
	 */
	public void setComments(List<CommentTO> comments) {
		this.comments = comments;
	}



	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	/**
	 * Gets the tags list.
	 *
	 * @return the tags list
	 */
	public List<TagTO> getTags() {
		return tags;
	}

	/**
	 * Sets the tags list.
	 *
	 * @param tags the new tags list
	 */
	public void setTags(List<TagTO> tags) {
		this.tags = tags;
	}	
}
