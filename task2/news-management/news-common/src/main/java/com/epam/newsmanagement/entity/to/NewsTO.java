package com.epam.newsmanagement.entity.to;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class NewsTO.
 */
public class NewsTO implements Serializable{
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8299664406737351377L;

	/** The id. */
	private long newsId;
	
	/** The short text. */
	private String shortText;
	
	/** The full text. */
	private String fullText;
	
	/** The title. */
	private String title;
	
	/** The creation date. */
	private Date creationDate;
	
	
	/** The modification date. */
	private Date modificationDate;
	
	/**
	 * Instantiates a new news with a default parameters.
	 */
	public NewsTO() {
		
	}
	
	/**
	 * Instantiates a new news, that contained a short text,
	 * a full text, a title, a creation date and modification date .
	 *
	 * @param shortText is the short text
	 * @param fullText is the full text
	 * @param title is the title
	 * @param creationDate is the creation date
	 * @param modificationDate is the modification date
	 */
	public NewsTO(String shortText, String fullText, String title,
			Date creationDate, Date modificationDate) {
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsTO other = (NewsTO) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (newsId != other.newsId)
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder info = new StringBuilder();
		info.append("NewsTO [id=");
		info.append(newsId);
		info.append(", shortText=");
		info.append(shortText);
		info.append(", fullText=");
		info.append(fullText);
		info.append(", title=");
		info.append(title);
		info.append(", creationDate=");
		info.append(creationDate);
		info.append(", modificationDate=");
		info.append(modificationDate);
		info.append("]");
		return info.toString();
	}
	
	/**
	 * Gets the id of the object.
	 *
	 * @return the id
	 */
	public long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the id of the object.
	 *
	 * @param id the new id
	 */
	public void setNewsId(long id) {
		this.newsId = id;
	}

	/**
	 * Gets the short text.
	 *
	 * @return the short text
	 */
	public String getShortText() {
		return shortText;
	}
	
	/**
	 * Sets the short text.
	 *
	 * @param shortText the new short text
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
	
	/**
	 * Gets the full text.
	 *
	 * @return the full text
	 */
	public String getFullText() {
		return fullText;
	}
	
	/**
	 * Sets the full text.
	 *
	 * @param fullText the new full text
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * Gets the modification date.
	 *
	 * @return the modification date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}
	
	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate the new modification date
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	
}
