package com.epam.newsmanagement.entity.to;

import java.io.Serializable;

/**
 * The Class AuthorTO.
 */
public class AuthorTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4513936706567958992L;

	/** The name. */
	private String authorName;
	
	/** The id. */
	private long authorId;
	
	/**
	 * Instantiates a new author.
	 */
	public AuthorTO() {
		
	}
	
	/**
	 * Instantiates a new author that contained the name.
	 *
	 * @param name the name
	 */
	public AuthorTO(String name) {
		this.authorName = name;
	}
	
	/**
	 * Instantiates a new author to.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public AuthorTO(long id, String name) {
		this.authorId = id;
		this.authorName = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (authorId ^ (authorId >>> 32));
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorTO other = (AuthorTO) obj;
		if (authorId != other.authorId)
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder info = new StringBuilder();
		info.append("AuthorTO [name=");
		info.append(authorName);
		info.append(", id=");
		info.append(authorId);
		info.append("]");
		return info.toString();
	}	
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setAuthorName(String name) {
		this.authorName = name;
	}
	
	/**
	 * Gets the id of the object.
	 *
	 * @return the id
	 */
	public long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the id of the object.
	 *
	 * @param id the new id
	 */
	public void setAuthorId(long id) {
		this.authorId = id;
	}
	
	
}
