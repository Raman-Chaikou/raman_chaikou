package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * The Class AuthorServiceImpl.
 */
@Service("authorService")
public class AuthorServiceImpl implements AuthorService{
	
	/** The authorDAO. */
	@Autowired
	private AuthorDAO authorDAO;
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.AuthorService#create(com.epam.newsmanagement.entity.AuthorTO)
	 */
	@Override
	public long create(AuthorTO author) throws ServiceException {
		try {
			return  authorDAO.create(author);
		} catch (DAOException e) {
			throw new ServiceException("Can't create this author:" + author, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.AuthorService#update(com.epam.newsmanagement.entity.AuthorTO)
	 */
	@Override
	public void update(AuthorTO author) throws ServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException e) {
			throw new ServiceException("Can't update this author: " + author, e);
		}
	
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.AuthorService#read(long)
	 */
	@Override
	public AuthorTO fetch(long id) throws ServiceException {
		try {
			return authorDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Can`t read this author: " + id, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.AuthorService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			authorDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Can`t delete this author: " + id, e);
		}		
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.AuthorService#searchAuthorsByNewsId(long)
	 */
	@Override
	public AuthorTO searchAuthorByNewsId(long newsId) 
			throws ServiceException {
		try {
			return  authorDAO.searchAuthorsByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't search the authors "
							+ "by news id : " + newsId , e);
		}
	}


	@Override
	public List<AuthorTO> fetchAuthors() throws ServiceException {
		try {
			return authorDAO.fetchAuthors();
		} catch (DAOException e) {
			throw new ServiceException("Can't fetch list of author.", e);
		}
	}


	@Override
	public List<AuthorTO> fetchNotExpiredAuthors() throws ServiceException {
		try {
			return authorDAO.fetchNotExpiredAuthors();
		} catch (DAOException e) {
			throw new ServiceException("Can't fetch list of author without expired.", e);
		}
	}


	@Override
	public void expiereAuthor(long authorId) throws ServiceException {
		try {
			authorDAO.expireAuthor(authorId);
		} catch (DAOException e) {
			throw new ServiceException("Can't update this "
					+ "expired of author by id: " + authorId, e);
		}
		
	}
	
}
