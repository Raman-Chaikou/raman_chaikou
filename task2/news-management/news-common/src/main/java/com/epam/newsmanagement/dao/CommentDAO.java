package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface CommentDAO.
 */
public interface CommentDAO extends CommonDAO<CommentTO>{
		
		/**
		 * Delete the list of comments from Database.
		 *
		 * @param commentIdList the list of id comment which should be removed
		 * from Database
		 * @throws DAOException when you have some troubles with
		 * removal the element in Database.
		 */
		void delete(List<Long> commentIdList) throws DAOException;
		
		/**
		 * Search comments by id of a news.
		 *
		 * @param newsId the id of a news which is used by search.
		 * @return the list of found of comments
		 * @throws DAOException when you have some troubles with 
		 * search of the comments in database
		 */
		List<CommentTO> searchCommentsByNewsId(long newsId) throws DAOException;
		
		/**
		 * Delete the comments from Database.
		 *
		 * @param newsId the id of a news which is used for search and remove comment
		 * from Database
		 * @throws DAOException when you have some troubles with
		 * removal the element in Database.
		 */
		void deleteCommentsByNewsId(long newsId) throws DAOException;
		
		void deleteCommentsByNewsIdList(List<Long> newsIdList) throws DAOException;
}
