package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;

/**
 * The Interface NewsService.
 */
public interface NewsService extends CommonService<NewsTO>{
	
	/**
	 * Delete  attachment between author and news.
	 *
	 * @param newsId the id of a news that is used
	 * for search and delete attachment
	 * @throws ServiceException when you have some troubles with
	 * removal attachment
	 */
	public void deleteNewsAuthorByNewsId(long newsId) 
			throws ServiceException;
	
	/**
	 * Search the news by author.
	 *
	 * @param authorId the id of the author that is used by search
	 * @return the list  of found of the news
	 * @throws ServiceException when you have some troubles with 
	 * search of the news
	 */
	public List<NewsTO> searchListNewsByAuthorId(long authorId) 
			throws ServiceException;
	
	/**
	 * Fetch list of news.
	 *
	 * @return the list of found of the news
	 * @throws ServiceException when you have some troubles with
	 * search of the news
	 */
	public List<NewsTO> fetchListNews() throws ServiceException;
	
	public List<NewsTO> fetchListNewsLimited(int minPositionNews, 
			int maxPositionNews, NewsFilter filter) throws ServiceException;
	
	/**
	 * Attach the tags to a news.
	 *
	 * @param tagIdList the list of id tags which should be attached to the news
	 * @param newsId the news id
	 * @throws ServiceException when you have some troubles with
	 * creation attachment
	 */
	public void attachTagsToNews(List<Long> tagIdList, long newsId) 
			throws ServiceException;
	
	/**
	 * Attach the author to a news.
	 *
	 * @param authorId the author id
	 * @param newsId the id news to which must be attached the id of author
	 * @throws ServiceException when you have some troubles with
	 * creation attachment
	 */
	public void attachAuthorToNews(long authorIdList, long newsId)
			throws ServiceException;
	
	/**
	 * Search news by tags.
	 *
	 * @param tagIdList the list of id tags which are used by search.
	 * @return the list of found of the news
	 * @throws ServiceException when you have some troubles with
	 * search of the news
	 */
	public List<NewsTO> searchListNewsByTagsId(List<Long> tagIdList) 
			throws ServiceException;
	
	/**
	 * Fetch id of author by id of a news.
	 *
	 * @param newsId the id of a news that is used for fetch the author id
	 * @return the id of author
	 * @throws ServiceException when you have some troubles with
	 * search of the author id
	 */
	long fetchAuthorIdByNewsId(long newsId) throws ServiceException;
	
	int fetchCountNews(NewsFilter filter) throws ServiceException;
		
	int fetchPositionNews(long newsId, NewsFilter filter) throws ServiceException;
	
	void deleteNewsListById(List<Long> newsIdList) throws ServiceException;
	
	void deleteNewsTagByNewsId(long newsId) throws ServiceException;
	
}
