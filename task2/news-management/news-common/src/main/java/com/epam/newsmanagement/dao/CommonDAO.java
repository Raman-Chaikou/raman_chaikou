package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface CommonDAO.
 *
 * @param <T> the generic type
 */
public interface CommonDAO<T>{
	
	/**
	 * Creates the new element.
	 *
	 * @param entity the entity that should be added in Database
	 * @return the id of element that was added in Database
	 * @throws DAOException when you have some troubles with 
	 * creation the new element in Database.
	 */
	long create(T entity) throws DAOException;	
	
	/**
	 * Read the element.
	 *
	 * @param id the id that should be read from Database
	 * @return the element that was read from Database
	 * @throws DAOException when you have some troubles with 
	 * reading the element in Database.
	 */
	T fetch(long id) throws DAOException;	
	
	/**
	 * Update the element.
	 *
	 * @param entity the entity that should be updated in Database
	 * @throws DAOException when you have some troubles with
	 *  updating the element in Database.
	 */
	void update(T entity) throws DAOException;	
	
	/**
	 * Delete the element.
	 *
	 * @param id the id that should be removed from Database
	 * @throws DAOException when you have some troubles with
	 * removal the element in Database.
	 */
	void delete(long id) throws DAOException;	
}
