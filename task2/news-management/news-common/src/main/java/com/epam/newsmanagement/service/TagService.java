package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The Interface TagService.
 */
public interface TagService extends CommonService<TagTO>{
	/**
	 * Search by news.
	 *
	 * @param newsId the id of a news that is used for search of the list of tags
	 * @return the of found of the tags
	 * @throws ServiceException  when you have some troubles with
	 * search of the tags
	 */
	List<TagTO> searchTagsByNewsId(long newsId) throws ServiceException;
	
	List<TagTO> fetchTags() throws ServiceException;
}
