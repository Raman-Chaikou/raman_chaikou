package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.databaseutil.DatabaseUtils;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;

/**
 * The Class NewsDAOImpl.
 */
@Repository("newsDAO")
public class NewsDAOImpl  implements NewsDAO{
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/** The Constant SQL_INSERT_NEWS. */
	private static final String SQL_INSERT_NEWS = 
									"INSERT INTO news(news_id, short_text, full_text, title, "
									+ "creation_date, modification_date) "
									+ "VALUES (auto_inc_news.nextval, ?,  ?, ?, ?, ?)";
	
	/** The Constant SQL_DELETE_NEWS. */
	private static final String SQL_DELETE_NEWS = 
									"DELETE FROM NEWS "+ 
									"WHERE news.news_id = ?";
	
	/** The Constant SQL_DELETE_NEWS_AUTHOR. */
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = 
									"DELETE FROM NEWS_AUTHOR "
									+ "WHERE NEWS_ID = ?";
	
	/** The Constant SQL_DELETE_NEWS_TAG_BY_NEWS_ID. */
	private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = 
									"DELETE FROM NEWS_TAG "
									+ "WHERE NEWS_ID = ?";
	
	/** The Constant SQL_UPDATE_NEWS. */
	private static final String SQL_UPDATE_NEWS = 
									"  update NEWS set "
									+ "short_text = ?,"
									+ "full_text = ?,"
									+ "title = ?,"
									+ "creation_date = ?,"
									+ "modification_date = ?"
									+ "where news_id = ?";
	
	/** The Constant SQL_FETCH_LIST_NEWS. */
	private static final String SQL_FETCH_LIST_NEWS = 
									"SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
									+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, "
									+ "COUNT(COMMENTS.COMMENT_ID) "
									+ "FROM NEWS LEFT JOIN COMMENTS "
									+ "ON NEWS.NEWS_ID = COMMENTS.NEWS_ID "
									+ "GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
												+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
												+ "NEWS.MODIFICATION_DATE, COMMENTS.NEWS_ID "
									+"ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC";
	

	
	/** The Constant SQL_READ_NEWS. */
	private static final String SQL_READ_NEWS = 
									"select NEWS.news_id, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
									+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
									+ " from news "
									+ "where news.news_id = ?";
	
	/** The Constant SQL_READ_NEWS_AUTHOR. */
	private static final String SQL_READ_NEWS_AUTHOR_BY_NEWS_ID = 
									"SELECT NEWS_AUTHOR.AUTHOR_ID FROM NEWS_AUTHOR"
									+ " WHERE NEWS_ID = ?";
	
	/** The Constant SQL_READ_NEWS_TAG_BY_NEWS_ID. */
	private static final String SQL_READ_NEWS_TAG_BY_NEWS_ID = 
									"SELECT NEWS_TAG.TAG_ID FROM NEWS_TAG "
									+ "WHERE NEWS_ID = ?";
	
	/** The Constant SQL_SEARCH_BY_AUTHOR. */
	private static final String SQL_SEARCH_BY_AUTHOR_ID = 
									"select NEWS.news_id, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
									+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
									+" from news join NEWS_AUTHOR "
									+ "on   news_author.news_id = news.news_id "
									+ "and   news_author.author_id = ?";
	
	/** The Constant SQL_INSERT_TAG. */
	private static final String SQL_INSERT_NEWS_TAG = 
									"  INSERT INTO NEWS_TAG(NEWS_TAG_ID, NEWS_ID, TAG_ID) "
									+ "values   (auto_inc_news_tag.nextval, ?, ?)";
	
	/** The Constant SQL_INSERT_AUTHOR. */
	private static final String SQL_INSERT_NEWS_AUTHOR = 
									"INSERT INTO NEWS_AUTHOR(NEWS_AUTHOR_ID, NEWS_ID, AUTHOR_ID) "
									+ "VALUES (AUTO_INC_NEWS_AUTHOR.NEXTVAL, ?, ?)";
	
	/** The Constant SQL_SEARCH_BY_TAGS. */
	private static final String SQL_SEARCH_BY_TAGS_ID = 
									"SELECT UNIQUE(NEWS.NEWS_ID), NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
									+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
									+ "FROM NEWS JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID "
									+ "JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID "
									+ "WHERE TAG.TAG_ID IN (?)";
	
	private static final String SQL_FETCH_NEWS_POSITION_BEGIN = 
									" SELECT RNUM "															
									+ "FROM ( "
									+"SELECT ROWNUM RNUM, NEWS_ID "
									+ "FROM ( "
									+  "SELECT NEWS.NEWS_ID, COUNT(COMMENTS.COMMENT_ID) "
									+ "AS COUNT_COMMENTS "
									+ "FROM NEWS LEFT JOIN COMMENTS "
									+ "ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";
	private static final String SQL_FETCH_NEWS_POSITION_END = 
									"GROUP BY NEWS.NEWS_ID, NEWS.MODIFICATION_DATE "
									+ "ORDER BY COUNT_COMMENTS DESC, "
									+ "NEWS.MODIFICATION_DATE DESC, news_id) "
									+ "ORDER BY  count_comments desc, rnum) "
									+ "WHERE news_id = ? ";
	
	
	private static final String SQL_FETCH_LIST_NEWS_LIM_BEGIN = 
									 "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, "
								    + "TITLE, CREATION_DATE, MODIFICATION_DATE "
								    + "FROM ( "
								    + "SELECT ROWNUM RNUM, NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE,"
								    + "CREATION_DATE, MODIFICATION_DATE "
							        + "FROM ( "
									+ "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
									+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE,"
									+ "COUNT(UNIQUE(COMMENTS.COMMENT_ID)) AS COUNT_COMMENTS "
									+ "FROM NEWS LEFT JOIN COMMENTS "
									+ "ON NEWS.NEWS_ID = COMMENTS.NEWS_ID ";
											
											
	private static final String SQL_FETCH_LIST_NEWS_LIM_END = 
									 "GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
									+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
									+ "NEWS.MODIFICATION_DATE, COMMENTS.NEWS_ID "
									+"ORDER BY COUNT_COMMENTS DESC, NEWS.MODIFICATION_DATE DESC, news_id "
									+ ") WHERE ROWNUM <= ? ORDER BY  count_comments desc, rnum"
									+ ") WHERE RNUM >= ?";	
	
	
	private static final String SQL_FILTER_AUTHOR_FETCH_NEWS = 	
									  " join news_author "
									+ "on news.NEWS_ID = news_author.NEWS_ID "
									+ "and news_author.author_id = ?  ";
	
	private static final String SQL_FILTER_TAGS_FETCH_NEWS = 
									"JOIN NEWS_TAG "  
									 +"on news.news_id = NEWS_TAG.NEWS_ID " 
									 +"and NEWS_TAG.TAG_ID in (?) ";

															 
	private static final String SQL_FETCH_COUNT_NEWS = 
									"select count(UNIQUE(news.news_id)) "
								     +" from NEWS ";
	
/*	private static final String SQL_FETCH_COUNT_NEWS_BY_AUTH_ID = 
									"join news_author  "
									+"on news.news_id = news_author.NEWS_ID "
									+ "AND news_author.author_id = ? ";*/
	
/*	private static final String SQL_FETCH_COUNT_NEWS_BY_TAGS_ID = 
									"join news_tag "
									+ "on news.news_id = news_tag.NEWS_ID "
									+ "and news_tag.TAG_ID IN (?) ";*/
	
	/**
	 * Fetch data result set.
	 *
	 * @param resultSet has some data from Database
	 * @return the news is created with data from Database
	 * @throws SQLException the SQLException when you have trouble with ResultSet
	 */
	private NewsTO fetchDataResultSet(ResultSet resultSet) throws SQLException {
		NewsTO news = new NewsTO();
		news.setNewsId(resultSet.getLong(1));
		news.setShortText(resultSet.getString(2));
		news.setFullText(resultSet.getString(3));
		news.setTitle(resultSet.getString(4));
		news.setCreationDate(resultSet.getTimestamp(5));
		news.setModificationDate(resultSet.getDate(6));
		return news;
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#searchNewsByAuthorId(long)
	 */
	@Override
	public List<NewsTO> searchNewsByAuthorId(long authorId) throws DAOException{
		List<NewsTO> newsList = new LinkedList<NewsTO>();				
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			 preparedStatement = 
					connection.prepareStatement(SQL_SEARCH_BY_AUTHOR_ID);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery(); 						
			while(resultSet.next()) {
				newsList.add(fetchDataResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Can't search a news by author id : " + authorId, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#fetchListNews()
	 */
	@Override
	public List<NewsTO> fetchListNews() throws DAOException{					
		List<NewsTO> newsList = new LinkedList<NewsTO>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet =  statement.executeQuery(SQL_FETCH_LIST_NEWS);
			while(resultSet.next()) {
				newsList.add(fetchDataResultSet(resultSet));
			}
	
		} catch (SQLException e) {
			throw new DAOException("Can't fetch the list of news : ", e);
		} finally {
			DatabaseUtils.closeResources(statement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}
		

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#attachTagsToNews(java.util.List, long)
	 */
	@Override
	public void attachTagsToNews(List<Long> tagIdList, long newsId) 
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS_TAG);
			for (Long tagId : tagIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't attach the tags " + tagIdList 
									+" with a news. " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#attachAuthorsToNews(java.util.List, long)
	 */
	@Override
	public void attachAuthorToNews(long authorId, long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't attach the authors " + authorId 
									+ " with the news " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#searchNewsByTagsId(java.util.List)
	 */
	@Override
	public List<NewsTO> searchNewsByTagsId(List<Long> tagIdList) 
			throws DAOException{
		List<NewsTO> newsList = new LinkedList<NewsTO>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String finalStringSearchNews = SQL_SEARCH_BY_TAGS_ID.replace("?",
											generateParamForTagId(tagIdList)); 
		try {			
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = 
					connection.prepareStatement(finalStringSearchNews);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				NewsTO news = fetchDataResultSet(resultSet);
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't search the news by the id of the tags : " 
									+ tagIdList, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}
	
	/**
	 * Generate param for tag id.
	 *
	 * @param tagIdList the list id tags
	 * @return the string for select
	 */
	private String generateParamForTagId(List<Long> tagIdList) {
		StringBuilder selectFinal = new StringBuilder();
		Iterator<Long> tagIdIterator = tagIdList.iterator();
		for (Long tagId : tagIdList) {
			tagIdIterator.next();
			selectFinal.append(tagId);
			if (tagIdIterator.hasNext()) {
				selectFinal.append(", ");
			}
		}
		return selectFinal.toString();
	}
		

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#create(java.lang.Object)
	 */
	@Override
	public long create(NewsTO news) throws DAOException {
		long id = 0;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = 
				connection.prepareStatement(SQL_INSERT_NEWS, 
											new String[] {"news_id"});
			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(
								news.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(
								news.getModificationDate().getTime()));
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();					
			if(resultSet.next()) {
				id = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create the news : " + news, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		
		return id;
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#read(long)
	 */
	@Override
	public NewsTO fetch(long id) throws DAOException {
		NewsTO news = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = 
					connection.prepareStatement(SQL_READ_NEWS);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();		
			if(resultSet.next()) {
				news = fetchDataResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read the news : " + id, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;																
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#fetchAuthorIdListByNewsId(long)
	 */
	@Override
	public long fetchAuthorIdByNewsId(long newsId) throws DAOException {
		long authorId = 0L; 
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(
					SQL_READ_NEWS_AUTHOR_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				authorId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch id author by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authorId;
	}
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#fetchTagIdListByNewsId(long)
	 */
	@Override
	public List<Long> fetchTagIdsByNewsId(long newsId) throws DAOException {
		List<Long> tagIdList = new ArrayList<Long>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(
					SQL_READ_NEWS_TAG_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagIdList.add(resultSet.getLong(1));
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch id tag by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement,	resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tagIdList;

	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(java.lang.Object)
	 */
	@Override
	public void update(NewsTO news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(
									news.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(
									news.getModificationDate().getTime()));
			preparedStatement.setLong(6, news.getNewsId());
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Can't upadte the news : " + news, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();			
		} catch (SQLException e) {
			throw new DAOException("Can't delete the news : " + id, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}
	
	@Override
	public void deleteNewsListById(List<Long> newsIdList) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			for (Long newsId : newsIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't delete the list of news : " + newsIdList, e);
		} finally{
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
			
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#deleteNewsAuthorByNewsId(long)
	 */
	@Override
	public void deleteNewsAuthorByNewsId(long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(
					SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't delete attachment between news and author by news id : " 
										+ newsId, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}
	
	@Override
	public void deleteNewsAuthorByNewsIds(List<Long> newsIdList) 
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(
					SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			for (Long newsId : newsIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't delete list of attachment between "
					+ "news and author by list of news id  : "+ newsIdList, e);
		} finally{
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
			
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#deleteNewsTagByNewsId(long)
	 */
	@Override
	public void deleteNewsTagByNewsId(long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(
					SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't delete attachment between news and tag by news id " 
										+ newsId, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		
	}

	
	@Override
	public void deleteNewsTagByNewsIds(List<Long> newsIdList) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(
					SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
			for (Long newsId : newsIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't delete the list attachment "
					+ "between news and tag by list of news id " + newsIdList, e);
		} finally {
			DatabaseUtils.closeResources(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}		
		
	}

	@Override
	public List<NewsTO> fetchListNewsLimited(int minPositionNews, 
			int maxPositionNews, NewsFilter filter) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> newsTOList = new ArrayList<NewsTO>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = buildPrepapedStatementFetchListNews(connection, minPositionNews, 
					maxPositionNews, filter);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				NewsTO news = fetchDataResultSet(resultSet);
				newsTOList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch the limited list of news : ", e);
		} finally{
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsTOList;
	}
	
	private PreparedStatement buildPrepapedStatementFetchListNews(
			Connection connection, int minPositionNews, int maxPositionNews,
			NewsFilter filter) throws SQLException{
		PreparedStatement preparedStatement = null;
		String finalSQLRequest = buildSQLRequestFetchListNews(filter);
		preparedStatement = setupDataForRequestFetchListNews(filter, finalSQLRequest, 
				connection, minPositionNews, maxPositionNews);
		
		return preparedStatement;
	}
	
	private String buildSQLRequestFetchListNews(NewsFilter filter){
		StringBuilder resultRequest = new StringBuilder(
				SQL_FETCH_LIST_NEWS_LIM_BEGIN);
				
		buildSQLRequestWithFilter(filter, resultRequest);
						
		resultRequest.append(SQL_FETCH_LIST_NEWS_LIM_END);		
		return resultRequest.toString();
	}
	
	private void buildSQLRequestWithFilter(NewsFilter filter, 
											StringBuilder resultRequest){
		
		Long authorId = filter.getAuthorIdFilter();
		List<Long> tagIdList = filter.getTagIdsFilter();
		
		if ((authorId != null) && (authorId != 0)) {
			resultRequest.append(SQL_FILTER_AUTHOR_FETCH_NEWS);
		}
		
		if(!tagIdList.isEmpty()){
			String finalSearchByTagsId = SQL_FILTER_TAGS_FETCH_NEWS.replace("?", 
					generateParamForTagId(tagIdList));
			resultRequest.append(finalSearchByTagsId);
		}
	}
	
	private PreparedStatement setupDataForRequestFetchListNews(NewsFilter filter, 
			String sqlRequest, Connection connection, 
			int minPositionNews,int maxPositionNews) throws SQLException{
		
		Long authorId = filter.getAuthorIdFilter();
				
		PreparedStatement preparedStatement = 
				connection.prepareStatement(sqlRequest);
		
		if((authorId == null) || (authorId == 0)){
			preparedStatement.setInt(1, maxPositionNews);
			preparedStatement.setInt(2, minPositionNews);
		}
		
		if((authorId != null) && (authorId != 0)){
			preparedStatement.setLong(1, authorId);
			preparedStatement.setInt(2, maxPositionNews);
			preparedStatement.setInt(3, minPositionNews);
		}
		
		return preparedStatement;
	}
	
	@Override
	public int fetchCountNews(NewsFilter filter) throws DAOException {
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int countNews = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = buildPreparedStatementCountNews(connection, filter);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				countNews = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch count news by filter " + filter, e);
		} finally{
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return countNews;
	}
	
	private PreparedStatement buildPreparedStatementCountNews(
			Connection connection, NewsFilter filter) throws SQLException{

		String finalSQLRequest = buildSQLRequestCountNews(filter);
		PreparedStatement preparedStatement = setupDataForRequestCountNews(
				filter, finalSQLRequest, connection);
		return preparedStatement;
	}
	
	private String buildSQLRequestCountNews(NewsFilter filter){
		StringBuilder resultRequest = 
				new StringBuilder(SQL_FETCH_COUNT_NEWS);
		buildSQLRequestWithFilter(filter, resultRequest);
		return resultRequest.toString();
	}
	
	
	private PreparedStatement setupDataForRequestCountNews(NewsFilter filter, 
			String sqlRequest, Connection connection) throws SQLException{
		
		Long authorId = filter.getAuthorIdFilter();	
		PreparedStatement preparedStatement = 
				connection.prepareStatement(sqlRequest);
		
		if((authorId != null) && (authorId != 0)){
			preparedStatement.setLong(1, authorId);
		}
		
		return preparedStatement;
	}
	
	
	@Override
	public int fetchPositionNews(long newsId, NewsFilter filter) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int positionNews = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = buildPreparedStatementPositionNews(
					newsId, connection, filter);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				positionNews = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch the position this id of news. " + newsId,e);
		} finally{
			DatabaseUtils.closeResources(preparedStatement, resultSet);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return positionNews;
	}
	
	private PreparedStatement buildPreparedStatementPositionNews(
			Long newsId, Connection connection, NewsFilter filter) throws SQLException{

		String finalSQLRequest = buildSQLRequestPositionNews(filter);
		PreparedStatement preparedStatement = setupDataForRequestPositionNews(
				newsId, filter, finalSQLRequest, connection);
		return preparedStatement;
	}
	
	private String buildSQLRequestPositionNews(NewsFilter filter){
		StringBuilder resultRequest = 
				new StringBuilder(SQL_FETCH_NEWS_POSITION_BEGIN);
		buildSQLRequestWithFilter(filter, resultRequest);
		
		resultRequest.append(SQL_FETCH_NEWS_POSITION_END);
		return resultRequest.toString();
	}
	
	
	private PreparedStatement setupDataForRequestPositionNews(Long newsId, NewsFilter filter, 
			String sqlRequest, Connection connection) throws SQLException{
		
		Long authorId = filter.getAuthorIdFilter();	
		PreparedStatement preparedStatement = 
				connection.prepareStatement(sqlRequest);
		
		if((authorId != null) && (authorId != 0)){
			preparedStatement.setLong(1, authorId);
			preparedStatement.setLong(2, newsId);
		} else {
			preparedStatement.setLong(1, newsId);
		}
		
		return preparedStatement;
	}
}
