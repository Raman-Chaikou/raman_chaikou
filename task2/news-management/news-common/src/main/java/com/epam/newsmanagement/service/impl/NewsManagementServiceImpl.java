package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.entity.vo.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;
import com.epam.newsmanagement.util.page.PageUtils;
import com.epam.newsmanagement.util.page.entity.Page;

/**
 * The Class NewsServiceGeneralImpl.
 */
@Service("newsManagementService")
public class NewsManagementServiceImpl implements NewsManagementService {

	/** The news service. */
	@Autowired
	private NewsServiceImpl newsService;

	/** The author service. */
	@Autowired
	private AuthorServiceImpl authorService;

	/** The tag service. */
	@Autowired
	private TagServiceImpl tagService;

	/** The comment service. */
	@Autowired
	private CommentServiceImpl commentService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagement.service.GeneralService#saveNews(com.epam.
	 * newsmanagement.entity.NewsTO, com.epam.newsmanagement.entity.AuthorTO,
	 * java.util.List)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public long saveNews(NewsTO news, long authorId, List<Long> tagIdList)
			throws ServiceException {
		long newsId = newsService.create(news);
		newsService.attachAuthorToNews(authorId, newsId);
		newsService.attachTagsToNews(tagIdList, newsId);
		return newsId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagement.service.GeneralService#editNews(com.epam.
	 * newsmanagement.entity.NewsTO)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void editNews(NewsTO news, long authorId, List<Long> tagIdList)
			throws ServiceException {

		long newsId = news.getNewsId();

		newsService.deleteNewsAuthorByNewsId(newsId);
		newsService.attachAuthorToNews(authorId, newsId);

		newsService.deleteNewsTagByNewsId(newsId);
		newsService.attachTagsToNews(tagIdList, newsId);

		newsService.update(news);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagement.service.GeneralService#fetchListNews()
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Page fetchListNews(int pageSize, int pageNumber, NewsFilter filter)
			throws ServiceException {
		Page page = new Page();
		int maxPositionNews = PageUtils.calculateMaxPositionNews(pageSize,
				pageNumber);
		int minPositionNews = PageUtils.calculateMinPositionNews(pageSize,
				maxPositionNews);
		List<NewsTO> newsTOList = newsService.fetchListNewsLimited(
				minPositionNews, maxPositionNews, filter);
		List<NewsVO> newsVOList = attachNewsTOListToNewsVOList(newsTOList);
		int countNews = newsService.fetchCountNews(filter);
		int countPage = PageUtils.calculateCountPage(pageSize, countNews);
		page.setCountPage(countPage);
		page.setNewsVOList(newsVOList);
		return page;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<NewsTO> fetchNewsAndNeighbor(long newsId, NewsFilter filter)
			throws ServiceException {
		int positionNews = newsService.fetchPositionNews(newsId, filter);
		int nextNews = positionNews + 1;
		int prevNews = positionNews - 1;
		List<NewsTO> newsTOList = newsService.fetchListNewsLimited(prevNews,
				nextNews, filter);
		return newsTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagement.service.GeneralService#viewNews(int)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public NewsVO takeNews(long newsId) throws ServiceException {
		NewsTO newsTO = newsService.fetch(newsId);
		NewsVO newsVO = takeDataForNewsVO(newsTO);
		return newsVO;

	}

	
	private NewsVO takeDataForNewsVO(NewsTO newsTO) throws ServiceException {
		long newsId = newsTO.getNewsId();
		AuthorTO author = authorService.searchAuthorByNewsId(newsId);
		List<TagTO> tagList = tagService.searchTagsByNewsId(newsId);
		List<CommentTO> commentList = commentService
				.searchCommentsByNewsId(newsId);
		NewsVO newsVO = insertDataIntoNewsVO(newsTO, author, tagList,
				commentList);
		return newsVO;
	}

	/**
	 * Inserted data into newsVO.
	 *
	 * @param newsTO
	 *            the newsTO that belong the newsVO
	 * @param authorList
	 *            the author list which belong the newsVO
	 * @param tagList
	 *            the tag list which belong the newsVO
	 * @param commentList
	 *            the comment list which belong the newsVO
	 * @return the newsVO that contains newsTO, the list authors, the list tags
	 *         and the list comments
	 */
	private NewsVO insertDataIntoNewsVO(NewsTO newsTO, AuthorTO author,
			List<TagTO> tagList, List<CommentTO> commentList) {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(newsTO);
		newsVO.setAuthor(author);
		newsVO.setTags(tagList);
		newsVO.setComments(commentList);
		return newsVO;
	}

	/**
	 * Attach news to list to newsVO list.
	 *
	 * @param newsTOList
	 *            the newsTO list
	 * @return the list newsVO
	 * @throws ServiceException
	 */
	private List<NewsVO> attachNewsTOListToNewsVOList(List<NewsTO> newsTOList)
			throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		for (NewsTO newsTO : newsTOList) {
			NewsVO newsVO = takeDataForNewsVO(newsTO);
			newsVOList.add(newsVO);
		}
		return newsVOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.newsmanagement.service.GeneralService#addNewsAuthor(com.epam
	 * .newsmanagement.entity.AuthorTO)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addAuthor(AuthorTO author) throws ServiceException {
		authorService.create(author);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagement.service.GeneralService#addComment(com.epam.
	 * newsmanagement.entity.CommentTO)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addComment(CommentTO comment) throws ServiceException {
		commentService.create(comment);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.newsmanagement.service.GeneralService#deleteComment(int)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteComment(long commentId) throws ServiceException {
		commentService.delete(commentId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<AuthorTO> fetchAuthors() throws ServiceException {
		return authorService.fetchAuthors();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<AuthorTO> fetchNotExpiredAuthors() throws ServiceException {
		return authorService.fetchNotExpiredAuthors();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<TagTO> fetchTags() throws ServiceException {
		return tagService.fetchTags();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteNewsListById(List<Long> newsIdList)
			throws ServiceException {
		commentService.deleteCommentsByNewsIds(newsIdList);
		newsService.deleteNewsListById(newsIdList);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void editAuthor(AuthorTO author) throws ServiceException {
		authorService.update(author);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void expireAuthor(long authorId) throws ServiceException {
		authorService.expiereAuthor(authorId);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void editTag(TagTO tag) throws ServiceException {
		tagService.update(tag);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteTag(long tagId) throws ServiceException {
		tagService.delete(tagId);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addTag(TagTO tag) throws ServiceException {
		tagService.create(tag);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void attachAuthorToNews(long authorId, long newsId)
			throws ServiceException {
		newsService.attachAuthorToNews(authorId, newsId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteNewsTagByNewsId(long newsId) throws ServiceException {
		newsService.deleteNewsTagByNewsId(newsId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteNewsAuthorByNewsId(long newsId) throws ServiceException {
		newsService.deleteNewsAuthorByNewsId(newsId);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public AuthorTO fetchAuthor(long authorId) throws ServiceException {
		return authorService.fetch(authorId);
	}
}
