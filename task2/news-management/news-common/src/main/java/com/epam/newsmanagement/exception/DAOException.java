package com.epam.newsmanagement.exception;

/**
 * The Class DAOException. It is checked exception.
 * Checked exceptions need to be declared in a method or constructor's 
 * throws clause if they can be thrown 
 * by the execution of the method or constructor and 
 * propagate outside the method or constructor boundary.
 * 
 * DAOException is thrown when you have some troubles with Database.
 */
public class DAOException extends Exception{
	
	/**
	 * Instantiates a new DAOException.
	 */
	public DAOException(){
		super();
	}
	
	/**
	 * Instantiates a new DAOException 
	 *  with additional information that contains the message.
	 *
	 * @param message the message
	 */
	public DAOException(String message){
		super(message);
	}
	
	/**
	 * Instantiates a new DAOException with the message and 
	 * its generating exception.
	 *
	 * @param message the message
	 * @param throwable its generating exception
	 */
	public DAOException(String message, Throwable throwable){
		super(message, throwable);
	}
	
	/**
	 * Instantiates a new DAOException with 
	 * its generating exception.
	 *
	 * @param throwable  its generating exception
	 */
	public DAOException(Throwable throwable){
		super(throwable);
	}
}
