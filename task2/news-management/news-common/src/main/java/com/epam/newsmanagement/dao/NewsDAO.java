package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;

/**
 * The Interface NewsDAO.
 */
public interface  NewsDAO extends CommonDAO<NewsTO>{
	
	/**
	 * Search the news by author.
	 *
	 * @param authorId the id of the author that is used by search
	 * @return the list of found of the news
	 * @throws DAOException when you have some troubles with 
	 * search of the news in database
	 */
	List<NewsTO> searchNewsByAuthorId(long authorId) throws DAOException;
	
	/**
	 * Fetch list of news.
	 *
	 * @return the list of found of the news
	 * @throws DAOException when you have some troubles with
	 * search of the news in Database
	 */
	List<NewsTO> fetchListNews() throws DAOException;
	
	/**
	 * Attach the tags to a news.
	 *
	 * @param tagIdList the list of id tags which should be attached to the news
	 * @param newsId the id news to which must be attached tags 
	 * @throws DAOException when you have some troubles with
	 * creation attachment in Database 
	 */
	void attachTagsToNews(List<Long> tagIdList, long newsId) throws DAOException;
	
	/**
	 * Search news by tags.
	 *
	 * @param tagIdList the list of id tags which are used by search.
	 * @return the list of found of the news
	 * @throws DAOException when you have some troubles with
	 * search of the news in Database.
	 */
	List<NewsTO> searchNewsByTagsId(List<Long> tagIdList) throws DAOException;
	
	/**
	 * Attach the author to a news.
	 *
	 * @param authorId the id of author that should be attached to the news
	 * @param newsId the id news to which must be attached the id of author
	 * @throws DAOException when you have some troubles with
	 * creation attachment in Database
	 */
	void attachAuthorToNews(long authorId, long newsId) throws DAOException;
	
	/**
	 * Fetch list id of author by id of a news.
	 *
	 * @param newsId the id of a news that is used for fetch the author id
	 * @return the id of author
	 * @throws DAOException when you have some troubles with
	 * search of the author id in Database
	 */
	long fetchAuthorIdByNewsId(long newsId) throws DAOException;
	
	/**
	 * Fetch list id of author by id of a news.
	 *
	 * @param newsId the id of a news that is used for fetch the tag id
	 * @return the list id of tag
	 * @throws DAOException when you have some troubles with
	 * search of the author id in Database
	 */
	List<Long> fetchTagIdsByNewsId(long newsId) throws DAOException;
	
	/**
	 * Delete attachment between author and news.
	 *
	 * @param newsId the id of a news that is used for search and delete attachment
	 * between author and  news in Database
	 * @throws DAOException when you have some troubles with
	 * removal attachment in Database
	 */
	void deleteNewsAuthorByNewsId(long newsId) throws DAOException;
	
	/**
	 * Delete attachment between tag and news.
	 *
	 * @param newsId the id of a news that is used for search and delete attachment
	 * between tag and  news in Database
	 * @throws DAOException when you have some troubles with
	 * removal attachment in Database
	 */
	void deleteNewsTagByNewsId(long newsId) throws DAOException;
	
	List<NewsTO> fetchListNewsLimited(int minPositionNews, int maxPositionNews,
			NewsFilter filter) throws DAOException;
	
	
	int fetchCountNews(NewsFilter filter) throws DAOException;
	
	int fetchPositionNews(long newsId, NewsFilter filter) throws DAOException;
	
	void deleteNewsListById(List<Long> newsIdList) throws DAOException;
	
	void deleteNewsAuthorByNewsIds(List<Long> newsIdList) throws DAOException;
	
	void deleteNewsTagByNewsIds(List<Long> newsIdList) throws DAOException;
	
}
