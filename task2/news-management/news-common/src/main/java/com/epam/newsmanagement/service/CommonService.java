package com.epam.newsmanagement.service;

import com.epam.newsmanagement.exception.ServiceException;

/**
 * The Interface CommonService.
 *
 * @param <T> the generic type
 */
public interface CommonService<T> {
	
	/**
	 * Creates the new element.
	 *
	 * @param entity the entity that should be added
	 * @return the id of element that was added
	 * @throws ServiceException when you have some troubles with
	 * creation the new element  
	 */
	long create(T entity) throws ServiceException;
	
	/**
	 * Update the element.
	 *
	 * @param entity the entity that should be updated
	 * @throws ServiceException when you have some troubles with 
	 * updating the element
	 */
	void update(T entity) throws ServiceException;
	
	/**
	 * Read the element.
	 *
	 * @param id the id that should be read
	 * @return the element that was read 
	 * @throws ServiceException when you have some troubles with
	 * reading the element
	 */
	T fetch(long id) throws ServiceException;
	
	/**
	 * Delete the element.
	 *
	 * @param id the id that should be removed 
	 * @throws ServiceException when you have some troubles with
	 * removal the element
	 */
	void delete(long id) throws ServiceException;
}
