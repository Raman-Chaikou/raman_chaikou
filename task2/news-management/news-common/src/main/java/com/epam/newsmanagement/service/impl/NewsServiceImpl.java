package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;

/**
 * The Class NewsServiceImpl.
 */
@Service("newsService")
public class NewsServiceImpl implements NewsService{
	
	/** The newsDAO use to communicate with DAO . */
	@Autowired
	private NewsDAO newsDAO;
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.s#create(java.lang.Object)
	 */
	@Override
	public long create(NewsTO news) throws ServiceException {
		try {
			return newsDAO.create(news);
		} catch (DAOException e) {
			throw new ServiceException("Can't create a news: " + news, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommonService#update(java.lang.Object)
	 */
	@Override
	public void update(NewsTO news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			throw new ServiceException("Can't update a news: " + news, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommonService#read(long)
	 */
	@Override
	public NewsTO fetch(long id) throws ServiceException {
		try {
			return newsDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Can't read a news: " + id, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommonService#delete(long)
	 */
	@Override
	public void delete(long newsId) throws ServiceException {
		try {
			newsDAO.deleteNewsTagByNewsId(newsId);
			newsDAO.delete(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't delete a news: " + newsId, e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.NewsService#deleteNewsAuthorByNewsId(long)
	 */
	@Override
	public void deleteNewsAuthorByNewsId(long newsId) throws ServiceException {
		try {
				newsDAO.deleteNewsAuthorByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't delete a link between "
							+ "a news and an author by news id :" + newsId, e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.NewsService#searchNewsByAuthorId(long)
	 */
	@Override
	public List<NewsTO> searchListNewsByAuthorId(long authorId) throws ServiceException{
		try {
			return newsDAO.searchNewsByAuthorId(authorId);
		} catch (DAOException e) {
			throw new ServiceException("Can't search a news by author id : " + authorId, e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.NewsService#fetchListNews()
	 */
	@Override
	public List<NewsTO> fetchListNews() throws ServiceException{
		try {
			return newsDAO.fetchListNews();
		} catch (DAOException e) {
			throw new ServiceException("Can't fetch the news.", e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.NewsService#attachTagsToNews(java.util.List, long)
	 */
	@Override
	public void attachTagsToNews(List<Long> tagIdList, long newsId) 
			throws ServiceException {
		try {
			newsDAO.attachTagsToNews(tagIdList, newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't connect the tags " + tagIdList
							+ " with this news " + newsId, e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.NewsService#attachAuthorToNews(long, long)
	 */
	@Override
	public void attachAuthorToNews(long authorId, long newsId) throws ServiceException{
		try {
			newsDAO.attachAuthorToNews(authorId, newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't connect the authors" + authorId
					+ " with this news " + newsId, e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.NewsService#searchNewsByTagsId(java.util.List)
	 */
	@Override
	public List<NewsTO> searchListNewsByTagsId(List<Long> tagIdList) 
			throws ServiceException{
		try {
			return  newsDAO.searchNewsByTagsId(tagIdList);
		} catch (DAOException e) {
			throw new ServiceException("Can't search the news by the tags : " 
											+ tagIdList, e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.NewsService#readNewsAuthorIdByNewsId(long)
	 */
	@Override
	public long fetchAuthorIdByNewsId(long newsId) throws ServiceException {
		try {
			return newsDAO.fetchAuthorIdByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't read the author of news by news id : "
											+ newsId, e);
		}
	}


	@Override
	public List<NewsTO> fetchListNewsLimited(int minPositionNews,
			int maxPositionNews, NewsFilter filter) throws ServiceException {
		try {
			return newsDAO.fetchListNewsLimited(minPositionNews, maxPositionNews, filter);
		} catch (DAOException e) {
			throw new ServiceException(
					"Can't fetch the limited list of news", e);
		}
	}


	@Override
	public int fetchCountNews(NewsFilter filter) throws ServiceException {
		try {
			return newsDAO.fetchCountNews(filter);
		} catch (DAOException e) {
			throw new ServiceException("Can't fetch count news", e);
		}
	}


	@Override
	public int fetchPositionNews(long newsId, NewsFilter filter) throws ServiceException {
		try {
			return newsDAO.fetchPositionNews(newsId, filter);
		} catch (DAOException e) {
			throw new ServiceException("Can't fetch the position of news " 
											+ newsId, e);
		}
	}


	@Override
	public void deleteNewsListById(List<Long> newsIdList) throws ServiceException {
		try {
			newsDAO.deleteNewsAuthorByNewsIds(newsIdList);
			newsDAO.deleteNewsTagByNewsIds(newsIdList);
			newsDAO.deleteNewsListById(newsIdList);
		} catch (DAOException e) {
			throw new ServiceException("Can't delete the list of news: " + newsIdList, e);
		}		
	}


	@Override
	public void deleteNewsTagByNewsId(long newsId) throws ServiceException {
		try {
			newsDAO.deleteNewsTagByNewsId(newsId);
	} catch (DAOException e) {
		throw new ServiceException("Can't delete a link between "
						+ "a news and an tag by news id :" + newsId, e);
	}
	}		
}
