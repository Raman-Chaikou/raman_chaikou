package com.epam.newsmanagement.dao.databaseutil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class CloseResourceDatabase.
 */
public class DatabaseUtils {
	
	private static final Logger LOG = Logger.getLogger(DatabaseUtils.class);
	
	/**
	 * Close resources.
	 *
	 * @param statement the statement
	 */
	public static void closeResources(Statement statement){
		try {
			closeStatement(statement);
		} catch (SQLException e) {
			LOG.error("Problem with closing Statement");
		}
	}
	
	public static void closeResources(Statement statement, ResultSet resultSet){
		try {
			closeResultSet(resultSet);
		} catch (SQLException e) {
			LOG.error("Problem with closing ResultSet");
		}
		try {
			closeStatement(statement);
		} catch (SQLException e) {
			LOG.error("Problem with closing Statement");
		}
	}
	
	
	/**
	 * Close resources.
	 *
	 * @param connection the connection
	 * @param statement the statement
	 * @param resultSet the result set
	 */
	public static void closeResources(Connection connection, Statement statement,
								ResultSet resultSet){
		try {
			closeResultSet(resultSet);
		} catch (SQLException e) {
			LOG.error("Problem with closing ResultSet");
		}
		try {
			closeStatement(statement);
		} catch (SQLException e) {
			LOG.error("Problem with closing Statement");
		}
		try {
			closeConnection(connection);
		} catch (SQLException e) {
			LOG.error("Problem with closing Connection");
		}
	}
	
	/**
	 * Close resources.
	 *
	 * @param connection the connection
	 * @param statement the statement
	 * @throws DAOException when you have some troubles with 
	 * closing resources
	 */	
	public static void closeResources(Connection connection, Statement statement) throws DAOException {
		
		try {
			closeStatement(statement);
		} catch (SQLException e1) {
			throw new DAOException("Problem with closing Statement", e1);
		}
		
		try {
			closeConnection(connection);
		} catch (SQLException e) {
			throw new DAOException("Problem with closing Connection", e);
		}
	}
	
	/**
	 * Close connection.
	 *
	 * @param connection the connection that is used in DAO.
	 * @throws SQLException when you have some troubles with
	 * closing the connection with Database
	 */
	private static void closeConnection(Connection connection) throws SQLException {
		if(connection != null) {
			connection.close();
		}
	}
	
	/**
	 * Close statement.
	 *
	 * @param statement the statement that is used in DAO.
	 * @throws SQLException when you have some troubles with
	 *  closing the statement
	 */
	private static void closeStatement(Statement statement) throws SQLException {
		if(statement != null) {
			statement.close();
		}
	}
	
	/**
	 * Close result set.
	 *
	 * @param resultSet the resultSet  that is used in DAO.
	 * @throws SQLException when you have some troubles with
	 * closing the resultSet
	 */
	private static void closeResultSet(ResultSet resultSet) throws SQLException {
		if(resultSet != null) {
			resultSet.close();
		}
	}
}
