package test.com.epam.newsmanagement.dao;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.filter.entity.NewsFilter;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import static test.com.epam.newsmanagement.dao.Constans.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {SPRING_CONTEXT})
@TestExecutionListeners({DbUnitTestExecutionListener.class,
						 DependencyInjectionTestExecutionListener.class})
public class NewsDAOImplTest {
	
	private static NewsTO news;
	
	@Autowired
	private NewsDAO newsDAO;
	
	@BeforeClass
	public static void setUp() {
		news = new NewsTO();
		news.setShortText("short3");
		news.setFullText("full3");
		news.setTitle("title3");
		news.setCreationDate(Timestamp.valueOf("2015-03-11 11:38:47.79"));
		news.setModificationDate(Date.valueOf("2015-03-11"));
	}
	

	
	private void equalNews(NewsTO expected, NewsTO actual) {
		assertEquals(expected.getNewsId(), actual.getNewsId());
		assertEquals(expected.getShortText(), actual.getShortText());
		assertEquals(expected.getFullText(), actual.getFullText());
		assertEquals(expected.getTitle(), actual.getTitle());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getModificationDate(), actual.getModificationDate());
	}

	@Test
	@DatabaseSetup("/news/sampleData-create.xml")
	public void testCreate() throws DAOException {
		long idNews = newsDAO.create(news);
		news.setNewsId(idNews);
		NewsTO actualNews = newsDAO.fetch(idNews);
		equalNews(news, actualNews);
	}

	@Test
	@DatabaseSetup("/news/sampleData-delete.xml")
	public void testDelete() throws DAOException {
		long newsId = 3L;
		newsDAO.deleteNewsTagByNewsId(newsId);
		List<Long> tagIdList = newsDAO.fetchTagIdsByNewsId(newsId);
		assertEquals(0, tagIdList.size());
		newsDAO.deleteNewsAuthorByNewsId(newsId);
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(newsId);
		assertEquals(0, authorIdActual);
		newsDAO.delete(newsId);
		assertNull(newsDAO.fetch(newsId));
	}

	@Test
	@DatabaseSetup("/news/sampleData-update.xml")
	public void testUpdate() throws DAOException {
		long idNews = 3L;
		news.setNewsId(idNews);
		newsDAO.update(news);
		NewsTO actualNews = newsDAO.fetch(idNews);
		equalNews(news, actualNews);
	}

	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchListNews() throws DAOException {
		List<NewsTO> listNews = newsDAO.fetchListNews();
		assertEquals(4, listNews.size());
		long idNews = 3L;
		news.setNewsId(idNews);
		NewsTO actualNews = null;
		for (NewsTO receivedNews : listNews) {
			if(receivedNews.getNewsId() == idNews) {
				actualNews = receivedNews;
			} 
		}
		equalNews(news, actualNews);
		
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchListNewsLimitedFilterEmpty() throws DAOException{
		NewsFilter filter = new NewsFilter();
		List<NewsTO> newsTOList = newsDAO.fetchListNewsLimited(1, 4, filter);
		assertEquals(4, newsTOList.size());
		long newsId = 3L;
		news.setNewsId(newsId);
		NewsTO actuNews = null;
		for (NewsTO receivedNews : newsTOList) {
			if(receivedNews.getNewsId() == newsId){
				actuNews = receivedNews;
			}
		}
		equalNews(news, actuNews);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchListNewsLimitedFilterAuthorId() throws DAOException{
		NewsFilter filter = new NewsFilter();
		filter.setAuthorIdFilter(1L);
		List<NewsTO> newsTOList = 
				newsDAO.fetchListNewsLimited(1, 4, filter);
		assertEquals(2, newsTOList.size());
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchListNewsLimitedFilterTagIdList() throws DAOException{
		List<Long> tagIdList = Arrays.asList(1L);
		NewsFilter filter = new NewsFilter();
		filter.setTagIdsFilter(tagIdList);
		List<NewsTO> newsTOList = 
				newsDAO.fetchListNewsLimited(1, 4, filter);
		assertEquals(3, newsTOList.size());
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchListNewsLimitedFilterAuthorIdTagIdList() throws DAOException{
		List<Long> tagIdList = Arrays.asList(1L);
		Long authorId = 1L;
		NewsFilter filter = new NewsFilter(authorId, tagIdList);
		List<NewsTO> newsTOList = newsDAO.fetchListNewsLimited(1, 4, filter);
		assertEquals(2, newsTOList.size());
	}

	@Test
	@DatabaseSetup("/news/sampleData-read.xml")
	public void testRead() throws DAOException {
		NewsTO news = new NewsTO();
		long idNews = 1L;
		news.setNewsId(idNews);
		news.setShortText("short1");
		news.setFullText("full1");
		news.setTitle("title1");
		news.setCreationDate(Timestamp.valueOf("2015-03-11 11:38:47.79"));
		news.setModificationDate(Date.valueOf("2015-03-11"));
		NewsTO actual = newsDAO.fetch(idNews);
		equalNews(news, actual);
	}

	@Test
	@DatabaseSetup("/news/sampleData-searchByAuthor.xml")	
	public void testSearchByAuthor() throws DAOException {
		long idAuthor = 2L;
		List<NewsTO> listNews = newsDAO.searchNewsByAuthorId(idAuthor);
		assertEquals(1, listNews.size());
		NewsTO actualNews = null;
		long newsId = 3L;
		news.setNewsId(newsId);
		for (NewsTO receivedNews : listNews) {
			if (news.getNewsId() == newsId) {
				actualNews = receivedNews;
			}
		}
		equalNews(news, actualNews);		
	}

	@Test
	@DatabaseSetup("/news/sampleData-addTags.xml")
	public void testAttachTagsToNews() throws DAOException {
		List<Long> listIdTags = Arrays.asList(2L, 3L);
		long idNews = 3L;
		newsDAO.attachTagsToNews(listIdTags, idNews);
		List<NewsTO> listNews = newsDAO.searchNewsByTagsId(listIdTags);
		news.setNewsId(idNews);
		NewsTO actualNews = null;
		for (NewsTO receivedNews : listNews) {
			if(receivedNews.getNewsId() == idNews) {
				actualNews = receivedNews;
			}
		}
		equalNews(news, actualNews);
	}

	@Test
	@DatabaseSetup("/news/sampleData-searchByTags.xml")
	public void testSearchNewsByTags() throws DAOException {
		List<Long> listIdTags = Arrays.asList(1L, 2L);
		List<NewsTO> listNews =  newsDAO.searchNewsByTagsId(listIdTags);
		assertEquals(2,listNews.size());
		NewsTO actualNews = null;
		long idNews = 3L;
		news.setNewsId(idNews);
		for (NewsTO receiedNews : listNews) {
			if(receiedNews.getNewsId() == idNews) {
				actualNews = receiedNews;
			}
		}
		equalNews(news, actualNews);
		
	}

	@Test
	@DatabaseSetup("/news/sampleData-addAuthor.xml")
	public void testAttachAuthorsToNews() throws DAOException {
		long authorId = 1L;
		long idNews = 1L;
		newsDAO.attachAuthorToNews(authorId, idNews);
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(idNews);
		assertEquals(authorId, authorIdActual);
		assertEquals(authorId, authorIdActual);
	}

	@Test
	@DatabaseSetup("/news/sampleData-deleteNewsAuthor.xml")
	public void testDeleteNewsAuthorByNews() throws DAOException {
		long newsId = 1L;
		newsDAO.deleteNewsAuthorByNewsId(newsId);
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(newsId);
		assertEquals(0, authorIdActual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-NewsTag.xml")
	public void testDeleteNewsTagByNews() throws DAOException {
		long newsId = 1L;
		newsDAO.deleteNewsTagByNewsId(newsId);
		List<Long> tagIdList = newsDAO.fetchTagIdsByNewsId(newsId);
		assertEquals(0, tagIdList.size());
	}

	@Test
	@DatabaseSetup("/news/sampleData-readNewsAuthor.xml")
	public void testFetchAuthorIdListByNewsId() throws DAOException {
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(1L);
		assertEquals(1L, authorIdActual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-NewsTag.xml")
	public void testFetchTagIdListByNewsId() throws DAOException {
		List<Long> tagIdList = newsDAO.fetchTagIdsByNewsId(1L);
		assertEquals(2, tagIdList.size());
		assertEquals(2L,(long) tagIdList.get(1));
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchPositionNewsFilterOut() throws DAOException{
		int expected = 2;
		NewsFilter filter = new NewsFilter();
		Long newsId = 2L;
		int actual = newsDAO.fetchPositionNews(newsId, filter);
		assertEquals(expected, actual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchPositionNewsFilterAuthorId() throws DAOException{
		int expected = 1;
		NewsFilter filter = new NewsFilter();
		filter.setAuthorIdFilter(1L);
		Long newsId = 3L;
		int actual = newsDAO.fetchPositionNews(newsId, filter);
		assertEquals(expected, actual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchPositionNewsFilterTagIdList() throws DAOException{
		int expected = 1;
		NewsFilter filter = new NewsFilter();
		List<Long> tagIdList = Arrays.asList(1L);
		filter.setTagIdsFilter(tagIdList);
		Long newsId = 3L;
		int actual = newsDAO.fetchPositionNews(newsId, filter);
		assertEquals(expected, actual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchPositionNewsFilterAuthorIdTagIdList() throws DAOException{
		int expected = 2;
		List<Long> tagIdList = Arrays.asList(1L, 2L);
		Long authorId = 1L;
		NewsFilter filter = new NewsFilter(authorId, tagIdList);
		Long newsId = 2L;
		int actual = newsDAO.fetchPositionNews(newsId, filter);
		assertEquals(expected, actual);
	}
	
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchCountNewsFilterOut() throws DAOException{
		NewsFilter filter = new NewsFilter();
		int actual = newsDAO.fetchCountNews(filter);
		assertEquals(4, actual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchCountNewsFilterAuthorId() throws DAOException{
		NewsFilter filter = new NewsFilter();
		Long authorId = 1L;
		filter.setAuthorIdFilter(authorId);
		int actual = newsDAO.fetchCountNews(filter);
		assertEquals(2, actual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")	
	public void testFetchCountNewsFilterTagIdList() throws DAOException{
		NewsFilter filter = new NewsFilter();
		List<Long> tagIdList = Arrays.asList(1L);
		filter.setTagIdsFilter(tagIdList);
		int actual = newsDAO.fetchCountNews(filter);
		assertEquals(3, actual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchCountNewsFilterAuthorIdTagIdList() throws DAOException{
		Long authorId = 1L;
		List<Long> tagIdList = Arrays.asList(1L, 2L);
		NewsFilter filter = new NewsFilter(authorId, tagIdList);
		int actual = newsDAO.fetchCountNews(filter);
		assertEquals(2, actual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testDeleteNewsListById() throws DAOException{
		List<Long> newsIdList = Arrays.asList(1L, 2L);
		newsDAO.deleteNewsTagByNewsIds(newsIdList);
		newsDAO.deleteNewsAuthorByNewsIds(newsIdList);
		newsDAO.deleteNewsListById(newsIdList);
		List<NewsTO> newsTOList = newsDAO.fetchListNews();
		for (NewsTO newsTO : newsTOList) {
			if(newsIdList.contains(newsTO.getNewsId())){
				fail();
			}
		}
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testDeleteNewsAuthorByNewsIdList() throws DAOException{
		List<Long> newsIdList = Arrays.asList(1L, 2L);
		newsDAO.deleteNewsAuthorByNewsIds(newsIdList);
		long authorId = newsDAO.fetchAuthorIdByNewsId(2L);
		assertEquals(0, authorId);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testDeleteNewsTagByNewsIdList() throws DAOException{
		List<Long> newsIdList = Arrays.asList(1L, 2L);
		newsDAO.deleteNewsTagByNewsIds(newsIdList);
		List<Long> tagIdList = newsDAO.fetchTagIdsByNewsId(1L);
		assertEquals(0, tagIdList.size());
	}
	
	
}
