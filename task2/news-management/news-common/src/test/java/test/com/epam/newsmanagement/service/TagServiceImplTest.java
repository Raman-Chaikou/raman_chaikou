package test.com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.aop.framework.DefaultAopProxyFactory;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
	
	private static TagTO tag;
	private static List<TagTO> listTag;
	
	@Mock
	private TagDAO tagDAO;
	
	@InjectMocks
	private TagServiceImpl tagService;
	
	@BeforeClass
	public static void setUp() {
		tag = new TagTO("Tag name");
		listTag = new ArrayList<TagTO>() {
			{
				add(new TagTO(1, "tag11"));
				add(new TagTO(2, "tage"));
			}
		};
	}
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreate() throws DAOException, ServiceException {
		long expected = 1L;
		when(tagDAO.create(tag)).thenReturn(expected);
		long actual = tagService.create(tag);
		assertEquals(expected, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(tagDAO).create(any(TagTO.class));
		tagService.create(any(TagTO.class));
	}
	
	@Test
	public void testRead() throws DAOException, ServiceException {
		long tagId = 2L;
		when(tagDAO.fetch(tagId)).thenReturn(tag);
		TagTO actual = tagService.fetch(tagId);
		assertEquals(tag, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testReadFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(tagDAO).fetch(anyLong());
		tagService.fetch(anyLong());
	}
	
	@Test
	public void testDelete() throws DAOException, ServiceException {
		long tagId = 1L;
		tagService.delete(tagId);
		verify(tagDAO).delete(tagId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(tagDAO).delete(anyLong());
		tagService.delete(anyLong());
	}
	
	@Test
	public void testUpdate() throws DAOException, ServiceException {
		tagService.update(tag);
		verify(tagDAO).update(tag);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(tagDAO).update(tag);
		tagService.update(tag);
	}
	
	@Test
	public void testSearchTagsByNewsId() throws DAOException, ServiceException {
		when(tagDAO.searchTagsByNewsId(anyLong())).thenReturn(listTag);
		assertEquals(listTag, tagService.searchTagsByNewsId(anyLong()));	
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchTagsByNewsIdFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(tagDAO).searchTagsByNewsId(anyLong());
		tagService.searchTagsByNewsId(anyLong());
	}
	
	@Test
	public void testFetchTagList() throws DAOException, ServiceException{
		when(tagDAO.fetchTags()).thenReturn(listTag);
		assertEquals(listTag, tagService.fetchTags());
	}
	
	@Test(expected = ServiceException.class)
	public void testFetchTagListFail() throws DAOException, ServiceException{
		doThrow(DAOException.class).when(tagDAO).fetchTags();
		tagService.fetchTags();
	}
	
	@Test
	public void testFetchListTag() throws DAOException, ServiceException{
		when(tagDAO.fetchTags()).thenReturn(listTag);
		List<TagTO> actualTagList = tagService.fetchTags();
		assertEquals(listTag, actualTagList);
	}
	
	@Test(expected = ServiceException.class)
	public void testFetchListTagFail() throws DAOException, ServiceException{
		doThrow(DAOException.class).when(tagDAO).fetchTags();
		tagService.fetchTags();
	}
	
}
