package test.com.epam.newsmanagement.dao;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import static test.com.epam.newsmanagement.dao.Constans.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {SPRING_CONTEXT})
@TestExecutionListeners({DbUnitTestExecutionListener.class,
						 DependencyInjectionTestExecutionListener.class
						})
@DatabaseSetup("/author/sampleData-author.xml")
public class AuthorDAOImplTest {

	private static AuthorTO author;
	
	@Autowired
	private AuthorDAO authorDAO;
	
	@BeforeClass
	public static void setUp() {
		author = new AuthorTO();
		author.setAuthorName("Nick");
	}
	
	private void equalAuthor(AuthorTO expected, AuthorTO actual) {
		assertEquals(expected.getAuthorId(), actual.getAuthorId());
		assertEquals(expected.getAuthorName(), actual.getAuthorName());
	}
	
	@Test
	public void testCreate() throws DAOException {
		long authorId = authorDAO.create(author);
		author.setAuthorId(authorId);
		AuthorTO authorActual = authorDAO.fetch(authorId);
		equalAuthor(author, authorActual);
	}
	
	@Test
	public void testRead() throws DAOException {
		AuthorTO expected = new AuthorTO("Joi");
		expected.setAuthorId(1L);
		AuthorTO actual = authorDAO.fetch(1L);
		equalAuthor(expected, actual);
	}
	
	@Test
	public void testUpdate() throws DAOException {
		author.setAuthorId(1L);
		authorDAO.update(author);
		AuthorTO actualAuthor = authorDAO.fetch(1L);
		equalAuthor(author, actualAuthor);
	}
	
	@Test
	public void testDelete() throws DAOException {
		long idAuthor = 2L;
		author.setAuthorId(idAuthor);
		authorDAO.delete(idAuthor);
		assertNull(authorDAO.fetch(idAuthor));
	}
	
	@Test
	public void testSearchAuthorsByNewsId() throws DAOException {
		long newsId = 3L;
		long authorId = 3L;
		AuthorTO actualAuthor = authorDAO.searchAuthorsByNewsId(newsId);
		author.setAuthorId(authorId);
		equalAuthor(author, actualAuthor);
	}
	
	@Test
	public void testFetchListAuthor() throws DAOException{
		List<AuthorTO> authorList = authorDAO.fetchAuthors();
		assertEquals(6, authorList.size());
		long authorId = 1L;
		AuthorTO authorExpected = new AuthorTO(authorId, "Joi");
		AuthorTO authorActual = null;
		for (AuthorTO receivedAuthorTO : authorList) {
			if(receivedAuthorTO.getAuthorId() == authorId){
				authorActual = receivedAuthorTO;
			}
		}
		equalAuthor(authorExpected, authorActual);
	}
	
	@Test
	public void testFetchListAuthorOutExpired() throws DAOException{
		List<AuthorTO> authorList = authorDAO.fetchNotExpiredAuthors();
		assertEquals(3, authorList.size());
	}
	
	@Test
	public void testExpireAuthor() throws DAOException{
		Long authorId = 1L;
		authorDAO.expireAuthor(authorId);
		List<AuthorTO> authorList = authorDAO.fetchNotExpiredAuthors();
		for (AuthorTO authorTO : authorList) {
			if(authorTO.getAuthorId() == authorId){
				fail();
			}
		}
	}
}
