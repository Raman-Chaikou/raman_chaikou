package test.com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	private static CommentTO comment;
	private static List<CommentTO> listComments;
	private static List<Long> newsListId;
	
	@Mock
	private CommentDAO commentDAO;
	
	@InjectMocks
	private CommentServiceImpl commentService;
	
	@BeforeClass
	public static void setUp() {
		Date date = new Date();
		comment = new CommentTO("text", date, 1L);
		listComments = new ArrayList<CommentTO>() {
			{
				add(new CommentTO());
				add(new CommentTO());
			}
		};
		
		newsListId = Arrays.asList(1L, 2L);
	}
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreate() throws DAOException, ServiceException {
		long expected = 2L;
		when(commentDAO.create(any(CommentTO.class))).thenReturn(expected);
		long actual = commentService.create(any(CommentTO.class));
		assertEquals(expected, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDAO).create(any(CommentTO.class));
		commentService.create(any(CommentTO.class));
	}
	
	@Test
	public void testRead() throws DAOException, ServiceException {
		long commentId = 2L;
		when(commentDAO.fetch(commentId)).thenReturn(comment);
		CommentTO actual = commentService.fetch(commentId);
		assertEquals(comment, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testReadFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDAO).fetch(anyLong());
		commentService.fetch(anyLong());
	}
	
	@Test
	public void testUpdate() throws DAOException, ServiceException {
		commentService.update(comment);
		verify(commentDAO).update(comment);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDAO).update(comment);
		commentService.update(comment);
	}
	
	@Test
	public void testDelete() throws DAOException, ServiceException {
		long commentId = 2L;
		commentService.delete(commentId);
		verify(commentDAO).delete(commentId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDAO).delete(anyLong());
		commentService.delete(anyLong());
	}
	
	@Test
	public void testDeleteCommentByNewsId() throws ServiceException, DAOException {
		long newsId = 3L;
		commentService.deleteCommentsByNewsId(newsId);
		verify(commentDAO).deleteCommentsByNewsId(newsId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteCommentByNewsIdFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDAO).deleteCommentsByNewsId(anyLong());
		commentService.deleteCommentsByNewsId(anyLong());
	}
	
	
	@Test
	public void testSearchCommentByNewsId() throws ServiceException, DAOException {
		when(commentDAO.searchCommentsByNewsId(anyLong())).thenReturn(listComments);
		assertEquals(listComments, commentService.searchCommentsByNewsId(anyLong()));
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchCommentByNewsIdFail() throws ServiceException, DAOException {
		doThrow(DAOException.class).when(commentDAO).searchCommentsByNewsId(anyLong());
		commentService.searchCommentsByNewsId(anyLong());
	}
	
	@Test
	public void testDeleteCommentListByNewsIdList() throws ServiceException, DAOException{
		commentService.deleteCommentsByNewsIds(newsListId);
		verify(commentDAO).deleteCommentsByNewsIdList(newsListId);
	}
}
