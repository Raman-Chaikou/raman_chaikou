package test.com.epam.newsmanagement.dao;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.impl.CommentDAOImpl;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import static org.junit.Assert.*;
import static test.com.epam.newsmanagement.dao.Constans.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {SPRING_CONTEXT})
@TestExecutionListeners({DbUnitTestExecutionListener.class,
							DependencyInjectionTestExecutionListener.class})
@DatabaseSetup("/comment/sampleData-comment.xml")
public class CommentDAOImplTest {
	private static CommentTO comment;
	
	@Autowired
	private CommentDAOImpl commentDAO;
	
	@BeforeClass
	public static void setUp() {
		comment = new CommentTO();
		comment.setCommentText("comment text 7");
		comment.setCreationDate(Timestamp.valueOf("2015-03-11 11:38:47.79"));
		comment.setNewsId(1L);
	}
	
	private void equalComment(CommentTO expected, CommentTO actual) {
		assertEquals(expected.getCommentId(), actual.getCommentId());
		assertEquals(expected.getCommentText(), actual.getCommentText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getNewsId(), actual.getNewsId());
	}

	@Test
	public void testCreate() throws DAOException {
		long idComment = commentDAO.create(comment);
		comment.setCommentId(idComment);
		CommentTO actualComment = commentDAO.fetch(idComment);
		equalComment(comment, actualComment);
	}
	
	@Test
	public void testDelete() throws DAOException {
		commentDAO.delete(4L);
		assertNull(commentDAO.fetch(4L));
	}
	
	@Test
	public void testDeleteList() throws DAOException {
		List<Long> listIdComment = Arrays.asList(1L, 2L, 3L);
		commentDAO.delete(listIdComment);
		assertNull(commentDAO.fetch(1L));
		assertNull(commentDAO.fetch(2L));
		assertNull(commentDAO.fetch(3L));
		
	}
	
	@Test
	public void testDeleteCommentByNewsId() throws DAOException {
		long newsId = 4L;
		commentDAO.deleteCommentsByNewsId(newsId);
		List<CommentTO> commentTOList = commentDAO.searchCommentsByNewsId(newsId);
		assertEquals(0, commentTOList.size());
	}
	
	@Test
	public void testSearchCommentByNews() throws DAOException {
		List<CommentTO> listComment = commentDAO.searchCommentsByNewsId(1L);
		assertEquals(3, listComment.size());
		long idComment = 7L;
		comment.setCommentId(idComment);
		CommentTO commentActual = null;
		for (CommentTO receivedComment : listComment) {
			if(receivedComment.getCommentId() == idComment) {
				commentActual = receivedComment;
			}
		}
		equalComment(comment, commentActual);
	}
	
	@Test
	public void testRead() throws DAOException {
		long idComment = 7L;
		CommentTO commentActual = commentDAO.fetch(idComment);
		comment.setCommentId(idComment);
		equalComment(comment, commentActual);
	}
	
	@Test
	public void testUpdate() throws DAOException {
		long idComment = 2L;
		comment.setCommentId(idComment);
		commentDAO.update(comment);
		CommentTO commentActual = commentDAO.fetch(idComment);
		System.out.println(commentActual.getCommentText());
		equalComment(comment, commentActual);
	}
	
	@Test
	public void testDeleteCommentListByNewsIdList() throws DAOException{
		List<Long> newsIdList = Arrays.asList(1L, 2L);
		commentDAO.deleteCommentsByNewsIdList(newsIdList);
		assertEquals(0, commentDAO.searchCommentsByNewsId(1L).size());
		assertEquals(0, commentDAO.searchCommentsByNewsId(2L).size());
	}
	
}
