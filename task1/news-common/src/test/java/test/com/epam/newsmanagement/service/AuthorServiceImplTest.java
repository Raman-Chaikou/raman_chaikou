package test.com.epam.newsmanagement.service;


import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {
	
	private static AuthorTO author;
	
	@Mock
	private AuthorDAO authorDAO;
	
	@InjectMocks
	private AuthorServiceImpl authorService;
	
	@BeforeClass
	public static void setUp() {
		author = new AuthorTO("Batman");
	}
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
			
	@Test
	public void testCreate() throws ServiceException, DAOException {
		long expected = 2L;
		when(authorDAO.create(author)).thenReturn(expected);
		long actual = authorService.create(author);
		assertEquals(expected, actual);		
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(authorDAO).create(any(AuthorTO.class));
		authorService.create(any(AuthorTO.class));
	}
	
	@Test
	public void testRead() throws ServiceException, DAOException {
		when(authorDAO.read(2L)).thenReturn(author);
		AuthorTO actual = authorService.read(2L);
		assertEquals(author, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testReadFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(authorDAO).read(anyLong());
		authorService.read(anyLong());
	}
	
	@Test
	public void testUpdate() throws ServiceException, DAOException {
		authorService.update(author);
		verify(authorDAO).update(author);
		
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(authorDAO).update(author);
		authorService.update(author);
	}
	
	@Test
	public void testDelete() throws ServiceException, DAOException {
		long authorId = 1L;
		authorService.delete(authorId);
		verify(authorDAO).delete(authorId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteFail() throws DAOException, ServiceException {
		long authorId = 1L;
		doThrow(DAOException.class).when(authorDAO).delete(authorId);
		authorService.delete(authorId);
	}
	
	@Test
	public void testSearchAuthorsByNewsId() throws DAOException, ServiceException {
		
		when(authorDAO.searchAuthorsByNewsId(anyLong())).thenReturn(author);
		assertEquals(author, authorService.searchAuthorsByNewsId(anyLong()));
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchAuthorsByNewsIdFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(authorDAO).searchAuthorsByNewsId(anyLong());
		authorService.searchAuthorsByNewsId(anyLong());
	}
}
