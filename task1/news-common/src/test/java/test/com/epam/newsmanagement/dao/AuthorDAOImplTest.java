package test.com.epam.newsmanagement.dao;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.impl.AuthorDAOImpl;
import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application-context.xml"})
@TestExecutionListeners({DbUnitTestExecutionListener.class,
						 DependencyInjectionTestExecutionListener.class})
@DatabaseSetup("/author/sampleData-author.xml")
public class AuthorDAOImplTest {

	private static AuthorTO author;
	
	@Autowired
	private AuthorDAO authorDAO;
	
	@BeforeClass
	public static void setUp() {
		author = new AuthorTO();
		author.setName("Nick");
	}
	
	private void equalAuthor(AuthorTO expected, AuthorTO actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getName(), actual.getName());
	}
	
	@Test
	public void testCreate() throws DAOException {
		long authorId = authorDAO.create(author);
		author.setId(authorId);
		AuthorTO authorActual = authorDAO.read(authorId);
		equalAuthor(author, authorActual);
	}
	
	@Test
	public void testRead() throws DAOException {
		AuthorTO expected = new AuthorTO("Joi");
		expected.setId(1L);
		AuthorTO actual = authorDAO.read(1L);
		equalAuthor(expected, actual);
	}
	
	@Test
	public void testUpdate() throws DAOException {
		author.setId(1L);
		authorDAO.update(author);
		AuthorTO actualAuthor = authorDAO.read(1L);
		equalAuthor(author, actualAuthor);
	}
	
	@Test
	public void testDelete() throws DAOException {
		long idAuthor = 2L;
		author.setId(idAuthor);
		authorDAO.delete(idAuthor);
		assertNull(authorDAO.read(idAuthor));
	}
	
	@Test
	public void testSearchAuthorsByNewsId() throws DAOException {
		long newsId = 3L;
		long authorId = 3L;
		AuthorTO actualAuthor = authorDAO.searchAuthorsByNewsId(newsId);
		author.setId(authorId);
		equalAuthor(author, actualAuthor);
	}
}
