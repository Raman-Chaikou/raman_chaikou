package test.com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application-context.xml"})
@TestExecutionListeners({DbUnitTestExecutionListener.class,
						 DependencyInjectionTestExecutionListener.class})
public class NewsDAOImplTest {
	
	private static NewsTO news;
	
	@Autowired
	private NewsDAO newsDAO;
	
	@BeforeClass
	public static void setUp() {
		news = new NewsTO();
		news.setShortText("short3");
		news.setFullText("full3");
		news.setTitle("title3");
		news.setCreationDate(Timestamp.valueOf("2015-03-11 11:38:47.79"));
		news.setModificationDate(Date.valueOf("2015-03-11"));
	}
	

	
	private void equalNews(NewsTO expected, NewsTO actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getShortText(), actual.getShortText());
		assertEquals(expected.getFullText(), actual.getFullText());
		assertEquals(expected.getTitle(), actual.getTitle());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getModificationDate(), actual.getModificationDate());
	}

	@Test
	@DatabaseSetup("/news/sampleData-create.xml")
	public void testCreate() throws DAOException {
		long idNews = newsDAO.create(news);
		news.setId(idNews);
		NewsTO actualNews = newsDAO.read(idNews);
		equalNews(news, actualNews);
	}

	@Test
	@DatabaseSetup("/news/sampleData-delete.xml")
	public void testDelete() throws DAOException {
		long newsId = 3L;
		newsDAO.deleteNewsTagByNewsId(newsId);
		List<Long> tagIdList = newsDAO.fetchTagIdListByNewsId(newsId);
		assertEquals(0, tagIdList.size());
		newsDAO.deleteNewsAuthorByNewsId(newsId);
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(newsId);
		assertEquals(0, authorIdActual);
		newsDAO.delete(newsId);
		assertNull(newsDAO.read(newsId));
	}

	@Test
	@DatabaseSetup("/news/sampleData-update.xml")
	public void testUpdate() throws DAOException {
		long idNews = 3L;
		news.setId(idNews);
		newsDAO.update(news);
		NewsTO actualNews = newsDAO.read(idNews);
		equalNews(news, actualNews);
	}

	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchListNews() throws DAOException {
		List<NewsTO> listNews = newsDAO.fetchListNews();
		assertEquals(4, listNews.size());
		long idNews = 3L;
		news.setId(idNews);
		NewsTO actualNews = null;
		for (NewsTO receivedNews : listNews) {
			if(receivedNews.getId() == idNews) {
				actualNews = receivedNews;
			} 
		}
		equalNews(news, actualNews);
		
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-fetchListNews.xml")
	public void testFetchListNewsLimited() throws DAOException{
		List<NewsTO> newsTOList = newsDAO.fetchListNewsLimited(1, 4);
		assertEquals(4, newsTOList.size());
		long newsId = 3L;
		news.setId(newsId);
		NewsTO actuNews = null;
		for (NewsTO receivedNews : newsTOList) {
			if(receivedNews.getId() == newsId){
				actuNews = receivedNews;
			}
		}
		equalNews(news, actuNews);
	}

	@Test
	@DatabaseSetup("/news/sampleData-read.xml")
	public void testRead() throws DAOException {
		NewsTO news = new NewsTO();
		long idNews = 1L;
		news.setId(idNews);
		news.setShortText("short1");
		news.setFullText("full1");
		news.setTitle("title1");
		news.setCreationDate(Timestamp.valueOf("2015-03-11 11:38:47.79"));
		news.setModificationDate(Date.valueOf("2015-03-11"));
		NewsTO actual = newsDAO.read(idNews);
		equalNews(news, actual);
	}

	@Test
	@DatabaseSetup("/news/sampleData-searchByAuthor.xml")	
	public void testSearchByAuthor() throws DAOException {
		long idAuthor = 2L;
		List<NewsTO> listNews = newsDAO.searchNewsByAuthorId(idAuthor);
		assertEquals(1, listNews.size());
		NewsTO actualNews = null;
		long newsId = 3L;
		news.setId(newsId);
		for (NewsTO receivedNews : listNews) {
			if (news.getId() == newsId) {
				actualNews = receivedNews;
			}
		}
		equalNews(news, actualNews);		
	}

	@Test
	@DatabaseSetup("/news/sampleData-addTags.xml")
	public void testAttachTagsToNews() throws DAOException {
		List<Long> listIdTags = Arrays.asList(2L, 3L);
		long idNews = 3L;
		newsDAO.attachTagsToNews(listIdTags, idNews);
		List<NewsTO> listNews = newsDAO.searchNewsByTagsId(listIdTags);
		news.setId(idNews);
		NewsTO actualNews = null;
		for (NewsTO receivedNews : listNews) {
			if(receivedNews.getId() == idNews) {
				actualNews = receivedNews;
			}
		}
		equalNews(news, actualNews);
	}

	@Test
	@DatabaseSetup("/news/sampleData-searchByTags.xml")
	public void testSearchNewsByTags() throws DAOException {
		List<Long> listIdTags = Arrays.asList(1L, 2L);
		List<NewsTO> listNews =  newsDAO.searchNewsByTagsId(listIdTags);
		assertEquals(2,listNews.size());
		NewsTO actualNews = null;
		long idNews = 3L;
		news.setId(idNews);
		for (NewsTO receiedNews : listNews) {
			if(receiedNews.getId() == idNews) {
				actualNews = receiedNews;
			}
		}
		equalNews(news, actualNews);
		
	}

	@Test
	@DatabaseSetup("/news/sampleData-addAuthor.xml")
	public void testAttachAuthorsToNews() throws DAOException {
		long authorId = 1L;
		long idNews = 1L;
		newsDAO.attachAuthorToNews(authorId, idNews);
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(idNews);
		assertEquals(authorId, authorIdActual);
		assertEquals(authorId, authorIdActual);
	}

	@Test
	@DatabaseSetup("/news/sampleData-deleteNewsAuthor.xml")
	public void testDeleteNewsAuthorByNews() throws DAOException {
		long newsId = 1L;
		newsDAO.deleteNewsAuthorByNewsId(newsId);
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(newsId);
		assertEquals(0, authorIdActual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-NewsTag.xml")
	public void testDeleteNewsTagByNews() throws DAOException {
		long newsId = 1L;
		newsDAO.deleteNewsTagByNewsId(newsId);
		List<Long> tagIdList = newsDAO.fetchTagIdListByNewsId(newsId);
		assertEquals(0, tagIdList.size());
	}

	@Test
	@DatabaseSetup("/news/sampleData-readNewsAuthor.xml")
	public void testFetchAuthorIdListByNewsId() throws DAOException {
		long authorIdActual = newsDAO.fetchAuthorIdByNewsId(1L);
		assertEquals(1L, authorIdActual);
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-NewsTag.xml")
	public void testFetchTagIdListByNewsId() throws DAOException {
		List<Long> tagIdList = newsDAO.fetchTagIdListByNewsId(1L);
		assertEquals(2, tagIdList.size());
		assertEquals(2L,(long) tagIdList.get(1));
	}
	
	@Test
	@DatabaseSetup("/news/sampleData-create.xml")
	public void testFetchCountNews() throws DAOException{
		int expected = 2;
		int actual = newsDAO.fetchCountNews();
		assertEquals(expected, actual);
	}
	
}
