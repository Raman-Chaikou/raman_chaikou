package test.com.epam.newsmanagement.dao;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.impl.TagDAOImpl;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application-context.xml"})
@TestExecutionListeners({DbUnitTestExecutionListener.class,
						 DependencyInjectionTestExecutionListener.class})
@DatabaseSetup("/tag/sampleData-tag.xml")
public class TagDAOImplTest {
	
	private static TagTO tag;
	
	@Autowired
	private TagDAOImpl tagDAO;
	
	
	@BeforeClass
	public static void setUp() {
		tag = new TagTO("Tag5");
	}
	
	private void equalTags(TagTO expected, TagTO actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getTagName(), actual.getTagName());
	}
	
	@Test
	public void testSearchByNews() throws DAOException {
		List<TagTO> listTags = tagDAO.searchTagsByNewsId(1L);
		assertEquals(3, listTags.size());
		for (TagTO tagActual : listTags) {
			assertNotNull(tagActual);
		}
	}
	
	@Test
	public void testCreate() throws DAOException {
		long idTag = tagDAO.create(tag);
		tag.setId(idTag);
		TagTO tagActual = tagDAO.read(idTag);
		equalTags(tag, tagActual);
	}
	
	
	@Test
	public void testRead() throws DAOException {
		long idTag = 3L;
		TagTO tagExpected = new TagTO(idTag,"Tag3");
		TagTO tagActual = tagDAO.read(idTag);
		equalTags(tagExpected, tagActual);
	}
	
	@Test
	public void testUpdate() throws DAOException {
		long idTag = 4L;
		tag.setId(idTag);
		tagDAO.update(tag);
		TagTO tagActual = tagDAO.read(idTag);
		equalTags(tag, tagActual);
	}
	
	@Test
	public void testDelete() throws DAOException {
		long idTag = 2L;
		tagDAO.delete(idTag);
		assertNull(tagDAO.read(idTag));
	}
}
