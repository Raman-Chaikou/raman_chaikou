package test.com.epam.newsmanagement.dao;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.impl.CommentDAOImpl;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application-context.xml"})
@TestExecutionListeners({DbUnitTestExecutionListener.class,
							DependencyInjectionTestExecutionListener.class})
@DatabaseSetup("/comment/sampleData-comment.xml")
public class CommentDAOImplTest {
	private static CommentTO comment;
	
	@Autowired
	private CommentDAOImpl commentDAO;
	
	@BeforeClass
	public static void setUp() {
		comment = new CommentTO();
		comment.setCommentText("comment text 7");
		comment.setCreationDate(Timestamp.valueOf("2015-03-11 11:38:47.79"));
		comment.setNewsID(1L);
	}
	
	private void equalComment(CommentTO expected, CommentTO actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getCommentText(), actual.getCommentText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getNewsID(), actual.getNewsID());
	}

	@Test
	public void testCreate() throws DAOException {
		long idComment = commentDAO.create(comment);
		comment.setId(idComment);
		CommentTO actualComment = commentDAO.read(idComment);
		equalComment(comment, actualComment);
	}
	
	@Test
	public void testDelete() throws DAOException {
		commentDAO.delete(4L);
		assertNull(commentDAO.read(4L));
	}
	
	@Test
	public void testDeleteList() throws DAOException {
		List<Long> listIdComment = Arrays.asList(1L, 2L, 3L);
		commentDAO.delete(listIdComment);
		assertNull(commentDAO.read(1L));
		assertNull(commentDAO.read(2L));
		assertNull(commentDAO.read(3L));
		
	}
	
	@Test
	public void testDeleteCommentByNewsId() throws DAOException {
		long newsId = 4L;
		commentDAO.deleteCommentByNewsId(newsId);
		List<CommentTO> commentTOList = commentDAO.searchCommentsByNewsId(newsId);
		assertEquals(0, commentTOList.size());
	}
	
	@Test
	public void testSearchCommentByNews() throws DAOException {
		List<CommentTO> listComment = commentDAO.searchCommentsByNewsId(1L);
		assertEquals(3, listComment.size());
		long idComment = 7L;
		comment.setId(idComment);
		CommentTO commentActual = null;
		for (CommentTO receivedComment : listComment) {
			if(receivedComment.getId() == idComment) {
				commentActual = receivedComment;
			}
		}
		equalComment(comment, commentActual);
	}
	
	@Test
	public void testRead() throws DAOException {
		long idComment = 7L;
		CommentTO commentActual = commentDAO.read(idComment);
		comment.setId(idComment);
		equalComment(comment, commentActual);
	}
	
	@Test
	public void testUpdate() throws DAOException {
		long idComment = 2L;
		comment.setId(idComment);
		commentDAO.update(comment);
		CommentTO commentActual = commentDAO.read(idComment);
		System.out.println(commentActual.getCommentText());
		equalComment(comment, commentActual);
	}
	
}
