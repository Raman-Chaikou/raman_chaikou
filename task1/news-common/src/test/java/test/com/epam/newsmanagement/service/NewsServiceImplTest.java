package test.com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {
	private static NewsTO news;
	private static List<NewsTO> listNews;
	private static List<Long> listIdTags;
	
	@Mock
	private NewsDAO newsDAO;
	
	@InjectMocks
	private NewsServiceImpl newsService;
	
	@BeforeClass
	public static void setUp() {
		news = new NewsTO("short text", "full text", "title",
				new Date(), new Date());
		listNews = new ArrayList<NewsTO>() {
			{
				add(new NewsTO("short text", "full text", "title",
				new Date(), new Date()));
				add(new NewsTO("short text2", "full text2", "title2",
				new Date(), new Date()));
			}
		};
		
		listIdTags = new ArrayList<Long>() {
			{
				add(1L);
				add(2L);
			}
		};
	}
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void testCreate() throws DAOException, ServiceException {
		long expected = 2L;
		when(newsDAO.create(news)).thenReturn(expected);
		long actual = newsService.create(news);
		assertEquals(expected, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).create(any(NewsTO.class));
		newsService.create(any(NewsTO.class));
	}
	
	@Test
	public void testRead() throws DAOException, ServiceException {
		when(newsDAO.read(2L)).thenReturn(news);
		NewsTO actual = newsService.read(2L);
		assertEquals(news, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testReadFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).read(anyLong());
		newsService.read(anyLong());
	}
	
	@Test
	public void testUpdate() throws DAOException, ServiceException {
		newsService.update(news);
		verify(newsDAO).update(news);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).update(news);
		newsService.update(news);
	}
	
	
	@Test
	public void testDelete() throws DAOException, ServiceException {
		newsService.delete(1L);
		verify(newsDAO).delete(1L);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).delete(anyLong());
		newsService.delete(anyLong());
	}
	
	@Test
	public void testSearchNewsByAuthorId() throws DAOException, ServiceException{	
		when(newsDAO.searchNewsByAuthorId(1L)).thenReturn(listNews);
		List<NewsTO> actual = newsService.searchNewsByAuthorId(1L);
		assertEquals(listNews, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchNewsByAuthorIdFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).searchNewsByAuthorId(anyLong());
		newsService.searchNewsByAuthorId(anyLong());
	}
	
	@Test
	public void testFetchListNews() throws DAOException, ServiceException {
		when(newsDAO.fetchListNews()).thenReturn(listNews);
		List<NewsTO> actual = newsService.fetchListNews();
		assertEquals(listNews, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testFetchListNewsFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).fetchListNews();
		newsService.fetchListNews();
	}
	
	@Test
	public void testAttachTagsToNews() throws DAOException, ServiceException {
		newsService.attachTagsToNews(listIdTags, 1L);
		verify(newsDAO).attachTagsToNews(listIdTags, 1L);
	}
	
	@Test(expected = ServiceException.class)
	public void testAttachTagsToNewsFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).attachTagsToNews(anyList(), anyLong());
		newsService.attachTagsToNews(anyList(), anyLong());
	}
	
	@Test
	public void testAttachAuthorsToNews() throws DAOException, ServiceException {
		long authorId = 2L;
		newsService.attachAuthorToNews(authorId, 1L);
		verify(newsDAO).attachAuthorToNews(authorId, 1L);
	}
	
	@Test(expected = ServiceException.class)
	public void testAttachAuthorsToNewsFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).attachAuthorToNews(anyLong(), anyLong());
		newsService.attachAuthorToNews(anyLong(), anyLong());
	}
	
	@Test
	public void testSearchByNewsTags() throws DAOException, ServiceException {
		when(newsDAO.searchNewsByTagsId(listIdTags)).thenReturn(listNews);
		List<NewsTO> actual = newsService.searchNewsByTagsId(listIdTags);
		assertEquals(listNews, actual);
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchByNewsTagsFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).searchNewsByTagsId(anyList());
		newsService.searchNewsByTagsId(listIdTags);
	}
	
	@Test
	public void testDeleteNewsAuthorByNewsId() throws DAOException, ServiceException {
		long idNews = 1L;
		newsService.deleteNewsAuthorByNewsId(idNews);
		verify(newsDAO).deleteNewsAuthorByNewsId(idNews);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteNewsAuthorByNewsIdFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).deleteNewsAuthorByNewsId(anyLong());
		newsService.deleteNewsAuthorByNewsId(anyLong());
	}
	
	@Test
	public void testReadNewsAuthorIdByNewsId() throws DAOException, ServiceException {
		long authorId = 2L;
		when(newsDAO.fetchAuthorIdByNewsId(anyLong())).thenReturn(authorId);
		assertEquals(authorId, newsService.fetchAuthorIdByNewsId(anyLong()));
	}
	
	@Test(expected = ServiceException.class)
	public void testReadNewsAuthorFail() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDAO).fetchAuthorIdByNewsId(anyLong());
		newsService.fetchAuthorIdByNewsId(anyLong());
	}
	
	@Test
	public void testFetchCountNews() throws DAOException, ServiceException{
		int countNews = 10;
		when(newsDAO.fetchCountNews()).thenReturn(countNews);
		int actualCountNews = newsService.fetchCountNews();
		assertEquals(countNews, actualCountNews);
	}
	
	@Test(expected = ServiceException.class)
	public void testFetchCountNewsFail() throws DAOException, ServiceException{
		doThrow(DAOException.class).when(newsDAO).fetchCountNews();
		newsService.fetchCountNews();
	}
}
