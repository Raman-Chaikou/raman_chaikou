package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.vo.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.pageutil.entity.Page;

/**
 * The Interface GeneralService.
 */
public interface NewsManagementService {
	
	/**
	 * Save news.
	 *
	 * @param news the news that should be save
	 * @param authorId the author id 
	 * @param tagList the list of the tags which should be save
	 * @throws ServiceException when you have some troubles with
	 * saving the information
	 */
	void saveNews(NewsTO news, long authorId, List<Long> tagList) 
			throws ServiceException;
	
	/**
	 * Edits the news.
	 *
	 * @param news the news that should be edit
	 * @throws ServiceException when you have some troubles with
	 * editing of a news
	 */
	void editNews(NewsVO news) throws ServiceException;
	
	/**
	 * Delete news.
	 *
	 * @param newsId the id of a news that should be removed 
	 * @throws ServiceException  when you have some troubles with
	 * removal the news.
	 */
	void deleteNews(long newsId) throws ServiceException;
	
	/**
	 * Fetch list news.
	 *
	 * @return the list
	 * @throws ServiceException when you have some troubles with
	 * fetching the news.
	 */
	Page fetchListNews(Page page) throws ServiceException;
		
	/**
	 * View news.
	 *
	 * @param newsId the id of a news that should be received
	 * @return the newsVO
	 * @throws ServiceException when you have some troubles with
	 * receiving the news
	 */
	NewsVO takeNews(long newsId) throws ServiceException;
	
	/**
	 * Adds the news author.
	 *
	 * @param author the author that should be added
	 * @throws ServiceException when you have some troubles with
	 * addition the author
	 */
	void addAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Search the news by author.
	 *
	 * @param newsId the id of the author that is used by search
	 * @return the list newsVO
	 * @throws ServiceException when you have some troubles with
	 * search of the news
	 */
	List<NewsVO> searchNewsByAuthorId(long newsId) throws ServiceException;
	
	/**
	 * Attach the tags to a news.
	 *
	 * @param tagIdList the list of id tags which should be attached to the news
	 * @param newsId the id news to which must be attached tags
	 * @throws ServiceException when you have some troubles with
	 * attachment 
	 */
	void attachTagsToNews(List<Long> tagIdList, long newsId) throws ServiceException;
	
	/**
	 * Search news by tags.
	 *
	 * @param tagIdList the list of id tags which are used by search.
	 * @return the list newsVO
	 * @throws ServiceException when you have some troubles with
	 * search of the news
	 */
	List<NewsVO> searchNewsByTagId(List<Long> tagIdList) throws ServiceException;
	
	/**
	 * Adds the comment.
	 *
	 * @param comment the comment that should be added
	 * @throws ServiceException when you have some troubles with 
	 * addition the comment
	 */
	void addComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Delete comment.
	 *
	 * @param commentId the id of comment that should be removed
	 * @throws ServiceException when you have some troubles with
	 * removal the element
	 */
	void deleteComment(long commentId) throws ServiceException;
}
