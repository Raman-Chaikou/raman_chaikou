package com.epam.newsmanagement.entity.to;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class CommentTO.
 */
public class CommentTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1434040169933065560L;

	/** The id. */
	private long id;
	
	/** The comment text. */
	private String commentText;
	
	/** The creation date. */
	private Date creationDate;
	
	/** The id of a news. */
	private long newsID;
	
	/**
	 * Instantiates a new comment.
	 */
	public CommentTO() {
		
	}
	
	/**
	 * Instantiates a new comment that contained 
	 * the text of comment, the creation date of comment and
	 * the id of a news.
	 *
	 * @param commentText the text of comment
 	 * @param creationDate the creation date of comment
	 * @param newsID the id of a news
	 */
	public CommentTO(String commentText, Date creationDate, long newsID) {
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsID = newsID;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (newsID ^ (newsID >>> 32));
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentTO other = (CommentTO) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id != other.id)
			return false;
		if (newsID != other.newsID)
			return false;
		return true;
	}	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CommentTO [id=" + id + ", commentText=" + commentText
				+ ", creationDate=" + creationDate + ", newsID=" + newsID + "]";
	}	
	

	/**
	 * Gets the id of the object.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * Sets the id of the object.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the comment text
	 */
	public String getCommentText() {
		return commentText;
	}
	
	/**
	 * Sets the comment text.
	 *
	 * @param commentText the new comment text
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * Gets the id of a news.
	 *
	 * @return the news id
	 */
	public long getNewsID() {
		return newsID;
	}
	
	/**
	 * Sets the id of a news.
	 *
	 * @param newsID the new news id
	 */
	public void setNewsID(long newsID) {
		this.newsID = newsID;
	}
	
	
}
