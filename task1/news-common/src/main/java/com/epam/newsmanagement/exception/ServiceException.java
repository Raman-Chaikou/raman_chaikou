package com.epam.newsmanagement.exception;

/**
 * The Class ServiceException. It is checked exception.
 * Checked exceptions need to be declared in a method or constructor's 
 * throws clause if they can be thrown 
 * by the execution of the method or constructor and 
 * propagate outside the method or constructor boundary.
 * 
 * ServiceException is thrown when you have some trouble with service layer. 
 */
public class ServiceException extends Exception{
	
	/**
	 * Instantiates a new ServiceException 
	 * without additional information.
	 */
	public ServiceException(){
		super();
	}
	
	/**
	 * Instantiates a new ServiceException 
	 * with additional information that contains the message.
	 *
	 * @param message the message
	 */
	public ServiceException(String message){
		super(message);
	}
	
	/**
	 * Instantiates a new ServicException with the message and 
	 * its generating exception.
	 *
	 * @param message the message
	 * @param throwable its generating exception 
	 */
	public ServiceException(String message, Throwable throwable){
		super(message, throwable);
	}
	
	/**
	 * Instantiates a new ServiceException with
	 * its generating exception.
	 *
	 * @param throwable its generating exception
	 */
	public ServiceException(Throwable throwable){
		super(throwable);
	}
}
