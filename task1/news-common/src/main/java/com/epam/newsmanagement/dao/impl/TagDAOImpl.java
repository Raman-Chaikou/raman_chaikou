package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.databaseutil.DatabaseUtils;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class TagDAOImpl.
 */
@Repository("tagDAO")
public class TagDAOImpl implements TagDAO{
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/** The Constant SQL_SEARCH_BY_NEWS. */
	private static final String SQL_SEARCH_BY_NEWS_ID = "SELECT TAG.TAG_ID, TAG.TAG_NAME"
													+ " FROM TAG JOIN NEWS_TAG "
													+ "ON NEWS_TAG.TAG_ID = TAG.TAG_ID "
													+ "AND "
													+ "NEWS_TAG.NEWS_ID = ?";
	
	/** The Constant SQL_INSERT_TAG. */
	private static final String SQL_INSERT_TAG = "INSERT INTO TAG(TAG_ID, TAG_NAME) "
												+ "VALUES (AUTO_INC_TAG.NEXTVAL, ?)";
	
	/** The Constant SQL_READ_TAG. */
	private static final String SQL_READ_TAG = "SELECT TAG.TAG_ID, TAG.TAG_NAME "
												+ "FROM TAG "
												+ "WHERE TAG.TAG_ID = ?";
	
	/** The Constant SQL_UPDATE_TAG. */
	private static final String SQL_UPDATE_TAG = "UPDATE TAG SET "
												+ "TAG_NAME = ? "
												+ "WHERE TAG_ID = ?";
	
	/** The Constant SQL_DELETE_TAG. */
	private static final String SQL_DELETE_TAG = "DELETE FROM TAG "
												+ "WHERE TAG_ID = ?";
		
	/**
	 * Fetch data result set.
	 *
	 * @param resultSet has some data from Database
	 * @return the tag  is created with data from Database
	 * @throws SQLException the SQLException
	 */
	private TagTO fetchDataResultSet(ResultSet resultSet) throws SQLException {
		long tagId = resultSet.getLong(1);
		String name = resultSet.getString(2);
		TagTO tag = new TagTO(tagId, name);
		return tag;
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#create(java.lang.Object)
	 */
	@Override
	public long create(TagTO tag) throws DAOException {
		long id = 0;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					SQL_INSERT_TAG, new String[] {"TAG_ID"});
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create the tag : " + tag, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return id;
	}
	


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#read(long)
	 */
	@Override
	public TagTO read(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		TagTO tag = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_TAG);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = fetchDataResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read the tag : " + id, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return tag;
	}
		

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.TagDAO#searchTagsByNewsId(long)
	 */
	@Override
	public List<TagTO> searchTagsByNewsId(long newsId) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<TagTO> tagList = new LinkedList<TagTO>();
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SEARCH_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				TagTO tag = fetchDataResultSet(resultSet);
				tagList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't search the tag by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return tagList;
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(java.lang.Object)
	 */
	@Override
	public void update(TagTO tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't update the tag : " + tag, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG); 
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't delete the tag : " + id, e );
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}


}
