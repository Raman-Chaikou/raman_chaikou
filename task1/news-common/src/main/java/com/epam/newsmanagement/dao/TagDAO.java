package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface TagDAO.
 */
public interface TagDAO extends CommonDAO<TagTO>{
	
	/**
	 * Search by news.
	 *
	 * @param newsId the id of a news that is used for search of the list of tags
	 * @return the list of found of the tags
	 * @throws DAOException when you have some troubles with
	 * search of the tags in Database.
	 */
	List<TagTO> searchTagsByNewsId(long newsId) throws DAOException;
}
