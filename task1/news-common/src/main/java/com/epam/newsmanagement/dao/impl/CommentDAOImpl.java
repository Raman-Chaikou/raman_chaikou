package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.w3c.dom.ls.LSInput;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.databaseutil.DatabaseUtils;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class CommentDAOImpl.
 */
@Repository("commentDAO")
public class CommentDAOImpl implements CommentDAO{
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/** The Constant SQL_INSERT_COMMENT. */
	private static final String SQL_INSERT_COMMENT = "  INSERT INTO COMMENTS(COMMENT_ID, COMMENT_TEXT, "
													+ "	CREATION_DATE, NEWS_ID) "
													+ "values   (auto_inc_comments.nextval, ?, ?,?)";
	
	/** The Constant SQL_DELETE_COMMENT. */
	private static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS "
													+ "WHERE comment_id  = ?";
	
	
	/** The Constant SQL_SEARCH_BY_NEWS. */
	private static final String SQL_SEARCH_COMMENT_BY_NEWS_ID = "SELECT COMMENT_ID, COMMENT_TEXT,"
																+ " CREATION_DATE, NEWS_ID "
																+ "FROM COMMENTS "
																+ "WHERE NEWS_ID = ?";
	
	/** The Constant SQL_DELETE_COMMENT_BY_NEWS_ID. */
	private static final String SQL_DELETE_COMMENT_BY_NEWS_ID = "DELETE FROM COMMENTS "
																+ "WHERE NEWS_ID = ?";
	/** The Constant SQL_SELECT_COMMENT. */
	private static final String SQL_SELECT_COMMENT = "SELECT COMMENT_ID, COMMENT_TEXT,"
													+ " CREATION_DATE, NEWS_ID "
													+ "FROM COMMENTS "
													+ "WHERE COMMENT_ID = ?";
	
	/** The Constant SQL_UPDATE_COMMENT. */
	private static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET "
													+ "COMMENT_TEXT = ?, "
													+ "CREATION_DATE = ?, "
													+ "NEWS_ID = ? "
													+ "WHERE COMMENT_ID = ?";
				


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#create(java.lang.Object)
	 */
	@Override
	public long create(CommentTO comment) throws DAOException {
		long id = 0;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
				connection = dataSource.getConnection();
				preparedStatement = 
				connection.prepareStatement(SQL_INSERT_COMMENT, 
											new String[] {"comment_id"});
				preparedStatement.setString(1, comment.getCommentText());
				preparedStatement.setTimestamp(2, new Timestamp(
									comment.getCreationDate().getTime()));
				preparedStatement.setLong(3, comment.getNewsID());
				preparedStatement.executeUpdate();
				resultSet = preparedStatement.getGeneratedKeys();					
				if(resultSet.next()) {
					id = resultSet.getInt(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DAOException("Can't create the comment : " + comment, e);
			} finally {
				DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
			}
		return id;
	}
	
	/**
	 * Fetch data result set.
	 *
	 * @param resultSet has some data from Database
	 * @return the comment is created with data from Database
	 * @throws SQLException the SQLException when you have trouble with ResultSet
	 */
	private CommentTO fetchDataResultSet(ResultSet resultSet) throws SQLException {
		CommentTO comment = new CommentTO();
		comment.setId(resultSet.getLong(1));
		comment.setCommentText(resultSet.getString(2));
		comment.setCreationDate(resultSet.getTimestamp(3));
		comment.setNewsID(resultSet.getLong(4));
		return comment;
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#read(long)
	 */
	@Override
	public CommentTO read(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		CommentTO comment = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENT);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				comment = fetchDataResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read the comment : " + id, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return comment;
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(java.lang.Object)
	 */
	@Override
	public void update(CommentTO comment) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			preparedStatement.setString(1, comment.getCommentText());
			preparedStatement.setTimestamp(2, new Timestamp(
							comment.getCreationDate().getTime()));
			preparedStatement.setLong(3, comment.getNewsID());
			preparedStatement.setLong(4, comment.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't update the comment : " + comment, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, id);
			preparedStatement.execute();
		} catch (SQLException e) {
			throw new DAOException("Can't delete the comment : " + id, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommentDAO#delete(java.util.List)
	 */
	@Override
	public void delete(List<Long> commentIdList) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			for (Long commentId : commentIdList) {
				preparedStatement.setLong(1, commentId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't delete the comments : " + commentIdList, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommentDAO#searchCommentsByNewsId(long)
	 */
	@Override
	public List<CommentTO> searchCommentsByNewsId(long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<CommentTO> commentList = new LinkedList<CommentTO>();
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
											SQL_SEARCH_COMMENT_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				CommentTO comment = fetchDataResultSet(resultSet);
				commentList.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't search comment by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return commentList;
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommentDAO#deleteCommentByNewsId(long)
	 */
	@Override
	public void deleteCommentByNewsId(long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
											SQL_DELETE_COMMENT_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't delete the comments by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
		
	}
}
