package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.hamcrest.Condition.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.databaseutil.DatabaseUtils;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class NewsDAOImpl.
 */
@Repository("newsDAO")
public class NewsDAOImpl  implements NewsDAO{
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/** The Constant SQL_INSERT_NEWS. */
	private static final String SQL_INSERT_NEWS = "INSERT INTO news(news_id, short_text, full_text, title, "
																 + "creation_date, modification_date) "
												+ "VALUES (auto_inc_news.nextval, ?,  ?, ?, ?, ?)";
	
	/** The Constant SQL_DELETE_NEWS. */
	private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS "+ 
												  "WHERE news.news_id = ?";
	
	/** The Constant SQL_DELETE_NEWS_AUTHOR. */
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "DELETE FROM NEWS_AUTHOR "
																	+ "WHERE NEWS_ID = ?";
	
	/** The Constant SQL_DELETE_NEWS_TAG_BY_NEWS_ID. */
	private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "DELETE FROM NEWS_TAG "
																	+ "WHERE NEWS_ID = ?";
	
	/** The Constant SQL_UPDATE_NEWS. */
	private static final String SQL_UPDATE_NEWS = "  update NEWS set "
													+ "short_text = ?,"
													+ "full_text = ?,"
													+ "title = ?,"
													+ "creation_date = ?,"
													+ "modification_date = ?"
													+ "where news_id = ?";
	
	/** The Constant SQL_FETCH_LIST_NEWS. */
	private static final String SQL_FETCH_LIST_NEWS = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
															+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, "
															+ "COUNT(COMMENTS.COMMENT_ID) "
															+ "FROM NEWS LEFT JOIN COMMENTS "
															+ "ON NEWS.NEWS_ID = COMMENTS.NEWS_ID "
															+ "GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
																		+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
																		+ "NEWS.MODIFICATION_DATE, COMMENTS.NEWS_ID "
															+"ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC";
	
	private static final String SQL_FETCH_LIST_NEWS_LIMITED = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, "
															+ "TITLE, CREATION_DATE, MODIFICATION_DATE "
															+ "FROM ( "
																+ "SELECT ROWNUM RNUM, NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE,"
																					+ "CREATION_DATE, MODIFICATION_DATE "
																+ "FROM ( "
																	+ "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
																	+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE,"
																	+ "COUNT(COMMENTS.COMMENT_ID) AS COUNT_COMMENTS "
																	+ "FROM NEWS LEFT JOIN COMMENTS "
																	+ "ON NEWS.NEWS_ID = COMMENTS.NEWS_ID "
																	+ "GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
																				+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
																				+ "NEWS.MODIFICATION_DATE, COMMENTS.NEWS_ID "
																	+"ORDER BY COUNT_COMMENTS DESC, NEWS.MODIFICATION_DATE DESC "
																	+ ") WHERE ROWNUM <= ? "
																	+ ") WHERE RNUM >= ?";	
	
	/** The Constant SQL_READ_NEWS. */
	private static final String SQL_READ_NEWS = "select NEWS.news_id, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
														+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
														+ " from news "
														+ "where news.news_id = ?";
	
	/** The Constant SQL_READ_NEWS_AUTHOR. */
	private static final String SQL_READ_NEWS_AUTHOR_BY_NEWS_ID = "SELECT NEWS_AUTHOR.AUTHOR_ID FROM NEWS_AUTHOR"
																+ " WHERE NEWS_ID = ?";
	
	/** The Constant SQL_READ_NEWS_TAG_BY_NEWS_ID. */
	private static final String SQL_READ_NEWS_TAG_BY_NEWS_ID = "SELECT NEWS_TAG.TAG_ID FROM NEWS_TAG "
																+ "WHERE NEWS_ID = ?";
	
	/** The Constant SQL_SEARCH_BY_AUTHOR. */
	private static final String SQL_SEARCH_BY_AUTHOR_ID = "select NEWS.news_id, NEWS.SHORT_TEXT, NEWS.FULL_TEXT,"
														+ " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
														+" from news join NEWS_AUTHOR "
														+ "on   news_author.news_id = news.news_id "
														+ "and   news_author.author_id = ?";
	
	/** The Constant SQL_INSERT_TAG. */
	private static final String SQL_INSERT_NEWS_TAG = "  INSERT INTO NEWS_TAG(NEWS_TAG_ID, NEWS_ID, TAG_ID) "
													+ "values   (auto_inc_news_tag.nextval, ?, ?)";
	
	/** The Constant SQL_INSERT_AUTHOR. */
	private static final String SQL_INSERT_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR(NEWS_AUTHOR_ID, NEWS_ID, AUTHOR_ID) "
													+ "VALUES (AUTO_INC_NEWS_AUTHOR.NEXTVAL, ?, ?)";
	
	/** The Constant SQL_SEARCH_BY_TAGS. */
	private static final String SQL_SEARCH_BY_TAGS_ID = "SELECT UNIQUE(NEWS.NEWS_ID), NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
													+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE "
													+ "FROM NEWS JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID "
													+ "JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID "
													+ "WHERE TAG.TAG_ID IN (?)";
	
	private static final String SQL_FETCH_NEWS_COUNT = "SELECT COUNT(NEWS.news_id)"
													 + "FROM NEWS";
	
	
	/**
	 * Fetch data result set.
	 *
	 * @param resultSet has some data from Database
	 * @return the news is created with data from Database
	 * @throws SQLException the SQLException when you have trouble with ResultSet
	 */
	private NewsTO fetchDataResultSet(ResultSet resultSet) throws SQLException {
		NewsTO news = new NewsTO();
		news.setId(resultSet.getLong(1));
		news.setShortText(resultSet.getString(2));
		news.setFullText(resultSet.getString(3));
		news.setTitle(resultSet.getString(4));
		news.setCreationDate(resultSet.getTimestamp(5));
		news.setModificationDate(resultSet.getDate(6));
		return news;
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#searchNewsByAuthorId(long)
	 */
	@Override
	public List<NewsTO> searchNewsByAuthorId(long authorId) throws DAOException{
		List<NewsTO> newsList = new LinkedList<NewsTO>();				
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			 preparedStatement = 
					connection.prepareStatement(SQL_SEARCH_BY_AUTHOR_ID);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery(); 						
			while(resultSet.next()) {
				newsList.add(fetchDataResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Can't search a news by author id : " + authorId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return newsList;
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#fetchListNews()
	 */
	@Override
	public List<NewsTO> fetchListNews() throws DAOException{					
		List<NewsTO> newsList = new LinkedList<NewsTO>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			resultSet =  statement.executeQuery(SQL_FETCH_LIST_NEWS);
			while(resultSet.next()) {
				newsList.add(fetchDataResultSet(resultSet));
			}
	
		} catch (SQLException e) {
			throw new DAOException("Can't fetch the list of news : ", e);
		} finally {
			DatabaseUtils.closeResources(connection, statement, resultSet);
		}
		return newsList;
	}
		

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#attachTagsToNews(java.util.List, long)
	 */
	@Override
	public void attachTagsToNews(List<Long> tagIdList, long newsId) 
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS_TAG);
			for (Long tagId : tagIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't attach the tags " + tagIdList 
									+" with a news. " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#attachAuthorsToNews(java.util.List, long)
	 */
	@Override
	public void attachAuthorToNews(long authorId, long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't attach the authors " + authorId 
									+ " with the news " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
		
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#searchNewsByTagsId(java.util.List)
	 */
	@Override
	public List<NewsTO> searchNewsByTagsId(List<Long> tagIdList) 
			throws DAOException{
		List<NewsTO> newsList = new LinkedList<NewsTO>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String finalStringSearchNews = SQL_SEARCH_BY_TAGS_ID.replace("?",
											generateParamForTagId(tagIdList)); 
		try {			
			connection = dataSource.getConnection();
			preparedStatement = 
					connection.prepareStatement(finalStringSearchNews);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				NewsTO news = fetchDataResultSet(resultSet);
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't search the news by the id of the tags : " 
									+ tagIdList, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return newsList;
	}
	
	/**
	 * Generate param for tag id.
	 *
	 * @param tagIdList the list id tags
	 * @return the string for select
	 */
	private String generateParamForTagId(List<Long> tagIdList) {
		StringBuilder selectFinal = new StringBuilder();
		Iterator<Long> tagIdIterator = tagIdList.iterator();
		for (Long tagId : tagIdList) {
			tagIdIterator.next();
			selectFinal.append(tagId);
			if (tagIdIterator.hasNext()) {
				selectFinal.append(", ");
			}
		}
		return selectFinal.toString();
	}
		

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#create(java.lang.Object)
	 */
	@Override
	public long create(NewsTO news) throws DAOException {
		long id = 0;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = 
				connection.prepareStatement(SQL_INSERT_NEWS, 
											new String[] {"news_id"});
			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(
								news.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(
								news.getModificationDate().getTime()));
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();					
			if(resultSet.next()) {
				id = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create the news : " + news, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		
		return id;
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#read(long)
	 */
	@Override
	public NewsTO read(long id) throws DAOException {
		NewsTO news = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = 
					connection.prepareStatement(SQL_READ_NEWS);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();		
			if(resultSet.next()) {
				news = fetchDataResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read the news : " + id, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return news;																
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#fetchAuthorIdListByNewsId(long)
	 */
	@Override
	public long fetchAuthorIdByNewsId(long newsId) throws DAOException {
		long authorId = 0L; 
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					SQL_READ_NEWS_AUTHOR_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				authorId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch id author by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return authorId;
	}
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#fetchTagIdListByNewsId(long)
	 */
	@Override
	public List<Long> fetchTagIdListByNewsId(long newsId) throws DAOException {
		List<Long> tagIdList = new ArrayList<Long>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					SQL_READ_NEWS_TAG_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagIdList.add(resultSet.getLong(1));
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch id tag by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, 
											resultSet);
		}
		return tagIdList;

	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(java.lang.Object)
	 */
	@Override
	public void update(NewsTO news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(
									news.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(
									news.getModificationDate().getTime()));
			preparedStatement.setLong(6, news.getId());
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException("Can't upadte the news : " + news, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}

	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();			
		} catch (SQLException e) {
			throw new DAOException("Can't delete the news : " + id, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#deleteNewsAuthorByNewsId(long)
	 */
	@Override
	public void deleteNewsAuthorByNewsId(long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't delete attachment between news and author by news id : " 
										+ newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.NewsDAO#deleteNewsTagByNewsId(long)
	 */
	@Override
	public void deleteNewsTagByNewsId(long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't delete attachment between news and tag by news id " 
										+ newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
		
	}


	@Override
	public List<NewsTO> fetchListNewsLimited(int minPositionNews, int maxPositionNews) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> newsTOList = new ArrayList<NewsTO>();
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					SQL_FETCH_LIST_NEWS_LIMITED);
			preparedStatement.setInt(1, maxPositionNews);
			preparedStatement.setInt(2, minPositionNews);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				NewsTO news = fetchDataResultSet(resultSet);
				newsTOList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch the limited list of news : ", e);
		} finally{
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return newsTOList;
	}


	@Override
	public int fetchCountNews() throws DAOException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		int countNews = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_FETCH_NEWS_COUNT);
			if (resultSet.next()) {
				countNews = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't fetch the count news.", e);
		} finally {
			DatabaseUtils.closeResources(connection, statement, resultSet);
		}
		return countNews;
	}




}
