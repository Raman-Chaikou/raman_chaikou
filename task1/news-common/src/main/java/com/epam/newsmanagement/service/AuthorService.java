package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The Interface AuthorService.
 */
public interface AuthorService extends CommonService<AuthorTO>{
	/**
	 * Search by news.
	 *
	 * @param newsId the id of a news that is used for search of the list of tags
	 * @return the found of the author
	 * @throws ServiceException  when you have some troubles with
	 * search of the author
	 */
	AuthorTO searchAuthorsByNewsId(long newsId) throws ServiceException;
}
