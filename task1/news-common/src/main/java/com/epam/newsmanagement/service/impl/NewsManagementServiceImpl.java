package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.to.TagTO;
import com.epam.newsmanagement.entity.vo.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.pageutil.PageUtils;
import com.epam.newsmanagement.pageutil.entity.Page;
import com.epam.newsmanagement.service.NewsManagementService;

/**
 * The Class NewsServiceGeneralImpl.
 */
@Service("newsManagementService")
public class NewsManagementServiceImpl implements NewsManagementService{
	
	/** The news service. */
	@Autowired
	private NewsServiceImpl newsService;
	
	/** The author service. */
	@Autowired
	private AuthorServiceImpl authorService;
	
	/** The tag service. */
	@Autowired
	private TagServiceImpl tagService;
	
	/** The comment service. */
	@Autowired
	private CommentServiceImpl commentService;
	
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#saveNews(com.epam.newsmanagement.entity.NewsTO, com.epam.newsmanagement.entity.AuthorTO, java.util.List)
	 */
	@Override
	public void saveNews(NewsTO news, long authorId, List<Long> tagIdList) 
			throws ServiceException {
		long newsId = newsService.create(news);
		newsService.attachAuthorToNews(authorId, newsId);
		newsService.attachTagsToNews(tagIdList, newsId);
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#editNews(com.epam.newsmanagement.entity.NewsTO)
	 */
	@Override
	public void editNews(NewsVO news) throws ServiceException {
		NewsTO newsTO = news.getNews();
		newsService.update(newsTO);		
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#deleteNews(int)
	 */
	@Override
	public void deleteNews(long newsId) throws ServiceException {
		commentService.deleteCommentByNewsId(newsId);
		newsService.delete(newsId);		
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#fetchListNews()
	 */
	@Override
	public Page fetchListNews(Page page) throws ServiceException {
		int pageSize = page.getPageSize();
		int pageNumber = page.getPageNumber();
		int maxPositionNews = PageUtils.calculateMaxPositionNews(
											pageSize, pageNumber);
		int minPositionNews = PageUtils.calculateMinPositionNews(
										pageSize, maxPositionNews);
		List<NewsTO> newsTOList = newsService.fetchListNewsLimited(
									minPositionNews, maxPositionNews);
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		for (NewsTO newsTO : newsTOList) {
			NewsVO newsVO = takeDataForNewsVO(newsTO);
			newsVOList.add(newsVO);
		}
		int countNews = newsService.fetchCountNews();
		int countPage = PageUtils.calculateCountPage(pageSize, countNews);
		page.setCountPage(countPage);                      
		page.setNewsVOList(newsVOList);
		return page;
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#viewNews(int)
	 */
	@Override
	public NewsVO takeNews(long newsId) throws ServiceException {
		NewsTO newsTO = newsService.read(newsId);
		NewsVO newsVO = takeDataForNewsVO(newsTO);
		return newsVO;
		
	}
	
	private NewsVO takeDataForNewsVO(NewsTO newsTO) throws ServiceException{
		long newsId = newsTO.getId();
		AuthorTO author = authorService.searchAuthorsByNewsId(newsId);
		List<TagTO> tagList = tagService.searchTagsByNewsId(newsId);
		List<CommentTO> commentList = commentService.searchCommentByNewsId(newsId);
		NewsVO newsVO = insertDataIntoNewsVO(newsTO, author, tagList, commentList);
		return newsVO;
	}

	/**
	 * Inserted data into newsVO.
	 *
	 * @param newsTO the newsTO that belong the newsVO
	 * @param authorList the author list which belong the newsVO
	 * @param tagList the tag list which belong the newsVO
	 * @param commentList the comment list which belong the newsVO 
	 * @return the newsVO that contains newsTO, the list authors, the list tags 
	 * and the list comments 
	 */
	private NewsVO insertDataIntoNewsVO(NewsTO newsTO, AuthorTO author, 
							List<TagTO> tagList, List<CommentTO> commentList) {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(newsTO);
		newsVO.setAuthor(author);
		newsVO.setTagList(tagList);
		newsVO.setCommentList(commentList);
		return newsVO;
	}
	
	/**
	 * Attach news to list to newsVO list.
	 *
	 * @param newsTOList the newsTO list
	 * @return the list newsVO
	 */
	private List<NewsVO> attachNewsTOListToNewsVOList(List<NewsTO> newsTOList){
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		for (NewsTO newsTO : newsTOList) {
			NewsVO newsVO = new NewsVO();
			newsVO.setNews(newsTO);
			newsVOList.add(newsVO);
		}
		return newsVOList;
	}
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#addNewsAuthor(com.epam.newsmanagement.entity.AuthorTO)
	 */
	@Override
	public void addAuthor(AuthorTO author) throws ServiceException {
		authorService.create(author);
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#searchByAuthor(int)
	 */
	@Override
	public List<NewsVO> searchNewsByAuthorId(long authorId) throws ServiceException {
		List<NewsTO> newsTOList = newsService.searchNewsByAuthorId(authorId);
		List<NewsVO> newsVOList = attachNewsTOListToNewsVOList(newsTOList);
		return newsVOList;
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#addTagsNews(java.util.List, int)
	 */
	@Override
	public void attachTagsToNews(List<Long> tagIdList, long newsId)
			throws ServiceException {
		newsService.attachTagsToNews(tagIdList, newsId);
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#searchNewsByTags(java.util.List)
	 */
	@Override
	public List<NewsVO> searchNewsByTagId(List<Long> tagIdList) throws ServiceException {
		List<NewsTO> newsTOList =  newsService.searchNewsByTagsId(tagIdList);
		List<NewsVO> newsVOList = attachNewsTOListToNewsVOList(newsTOList);
		return newsVOList;
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#addComment(com.epam.newsmanagement.entity.CommentTO)
	 */
	@Override
	public void addComment(CommentTO comment) throws ServiceException {
		commentService.create(comment);		
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.GeneralService#deleteComment(int)
	 */
	@Override
	public void deleteComment(long commentId) throws ServiceException {
		commentService.delete(commentId);		
	}
	
	
}
