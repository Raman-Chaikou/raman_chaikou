package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface CommentService.
 */
public interface CommentService extends CommonService<CommentTO>{	
	/**
	 * Search comments by id of a news.
	 *
	 * @param newsId the id of a news which is used by search.
	 * @return the list of found of comments
	 * @throws ServiceException when you have some troubles with
	 * search of the comments
	 */ 
	List<CommentTO> searchCommentByNewsId(long newsId) throws ServiceException;
	
	/**
	 * Delete comment by news id.
	 *
	 * @param newsId the news id that is used for search and remove the comment
	 * @throws ServiceException when you have some troubles with
	 * removal the comment
	 */
	void deleteCommentByNewsId(long newsId) throws ServiceException;
}
