package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.databaseutil.DatabaseUtils;
import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;




/**
 * The Class AuthorDAOImpl.
 */
@Repository("authorDAO")
public class AuthorDAOImpl  implements AuthorDAO{
	

	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/** The Constant SQL_INSERT_AUTHOR. */
	private static final String SQL_INSERT_AUTHOR = "  INSERT INTO author(author_id, name) "
													+ "VALUES (auto_inc_author.nextval, ?)";
	
	/** The Constant SQL_SELECT_AUTHOR. */
	private static final String SQL_SELECT_AUTHOR = "select  AUTHOR.NAME "
													+ "from AUTHOR  "
													+ "where AUTHOR_ID = ?";
	
	/** The Constant SQL_UPDATE_AUTHOR. */
	private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET "
													+ "NAME = ? "
													+ "WHERE AUTHOR_ID = ?";
	
	/** The Constant SQL_DELETE_AUTHOR. */
	private static final String SQL_DELETE_AUTHOR = "DELETE FROM AUTHOR "
												  	+ "WHERE AUTHOR_ID = ?";
	
	/** The Constant SQL_SEARCH_BY_NEWS_ID. */
	private static final String SQL_SEARCH_BY_NEWS_ID = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.NAME "
														+ "FROM AUTHOR JOIN NEWS_AUTHOR "
														+ "ON NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID "
														+ "AND "
														+ "NEWS_AUTHOR.NEWS_ID = ?";

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#create(java.lang.Object)
	 */
	@Override
	public long create(AuthorTO author) throws DAOException {
		long id = 0;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = 
					connection.prepareStatement(SQL_INSERT_AUTHOR, 
												new String[] {"author_id"});
			preparedStatement.setString(1, author.getName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();			
			if(resultSet.next()) {
				id = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create the author : " + author, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return id;
	}
	

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#read(long)
	 */
	@Override
	public AuthorTO read(long id) throws DAOException {
		AuthorTO author = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = 
						connection.prepareCall(SQL_SELECT_AUTHOR);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				String name = resultSet.getString(1);
				author = new AuthorTO(name);
				author.setId(id);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read data of author : " + id, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement, resultSet);
		}
		return author;	
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(java.lang.Object)
	 */
	@Override
	public void update(AuthorTO author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			preparedStatement.setString(1, author.getName());
			preparedStatement.setLong(2, author.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't update the author : " + author, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection =  dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't delete the author : " + id, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement);
		}
	}
	
	/**
	 * Fetch data result set.
	 *
	 * @param resultSet has some data from Database
	 * @return the author  is created with data from Database
	 * @throws SQLException the SQLException when you have trouble with ResultSet
	 */
	private AuthorTO fetchDataResultSet(ResultSet resultSet) throws SQLException {
		long authorId = resultSet.getLong(1);
		String name = resultSet.getString(2);
		AuthorTO author = new AuthorTO(authorId, name);
		return author;
	}

	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.dao.AuthorDAO#searchAuthorsByNewsId(long)
	 */
	@Override
	public AuthorTO searchAuthorsByNewsId(long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		AuthorTO author = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
									SQL_SEARCH_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				author = fetchDataResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't search the authors "
									+ "by news id : " + newsId, e);
		} finally {
			DatabaseUtils.closeResources(connection, preparedStatement,
											resultSet);
		}
		return author;
	}	
}
