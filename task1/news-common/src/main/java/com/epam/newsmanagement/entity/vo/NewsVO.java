package com.epam.newsmanagement.entity.vo;

import java.util.List;

import com.epam.newsmanagement.entity.to.AuthorTO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.entity.to.NewsTO;
import com.epam.newsmanagement.entity.to.TagTO;

/**
 * The Class NewsVO.
 */
public class NewsVO {
	
	/** The newsTO. */
	private NewsTO news;
	
	/** The authors list. */
	private AuthorTO author;
	
	/** The tags list. */
	private List<TagTO> tagList;
	
	/** The comments list. */
	private List<CommentTO> commentList;
	
	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public NewsTO getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news the new newsTO
	 */
	public void setNews(NewsTO news) {
		this.news = news;
	}
	
	/**
	 * Gets the comments list.
	 *
	 * @return the comments list
	 */
	public List<CommentTO> getCommentList() {
		return commentList;
	}

	/**
	 * Sets the comments list.
	 *
	 * @param commentList the new comments list
	 */
	public void setCommentList(List<CommentTO> commentList) {
		this.commentList = commentList;
	}



	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	/**
	 * Gets the tags list.
	 *
	 * @return the tags list
	 */
	public List<TagTO> getTagList() {
		return tagList;
	}

	/**
	 * Sets the tags list.
	 *
	 * @param tagList the new tags list
	 */
	public void setTagList(List<TagTO> tagList) {
		this.tagList = tagList;
	}	
}
