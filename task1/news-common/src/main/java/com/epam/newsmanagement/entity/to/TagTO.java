package com.epam.newsmanagement.entity.to;

import java.io.Serializable;

/**
 * The Class TagTO.
 */
public class TagTO implements Serializable{


	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7639147522977102717L;
	
	/** The id. */
	private long id;
	
	/** The name of tag. */
	private String tagName;

	/**
	 * Instantiates a new tag, that contained the name of tag.
	 *
	 * @param tagName the name of tag
	 */
	public TagTO(String tagName) {
		this.tagName = tagName;
	}
	
	/**
	 * Instantiates a new tag to.
	 *
	 * @param id the id
	 * @param tagName the tag name
	 */
	public TagTO(long id, String tagName) {
		this.id = id;
		this.tagName = tagName;
	}
	
	/**
	 * Gets the id of the object.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of the object.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the name of tag.
	 *
	 * @return the name of tag
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Sets the tag name.
	 *
	 * @param tagName the new name of tag
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagTO other = (TagTO) obj;
		if (id != other.id)
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TagTO [id=" + id + ", tagName=" + tagName + "]";
	}
}
