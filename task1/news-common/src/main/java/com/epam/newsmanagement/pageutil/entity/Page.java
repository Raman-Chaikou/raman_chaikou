package com.epam.newsmanagement.pageutil.entity;

import java.util.List;

import com.epam.newsmanagement.entity.vo.NewsVO;

public class Page {
	private int countPage;
	private int pageNumber;
	private int pageSize;
	private List<NewsVO> newsVOList;
	
	public Page(int pageSize, int pageNumber){
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
	}
		
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getCountPage() {
		return countPage;
	}

	public void setCountPage(int pageCount) {
		this.countPage = pageCount;
	}

	public List<NewsVO> getNewsVOList() {
		return newsVOList;
	}
	public void setNewsVOList(List<NewsVO> newsVOList) {
		this.newsVOList = newsVOList;
	}
	
	
}
