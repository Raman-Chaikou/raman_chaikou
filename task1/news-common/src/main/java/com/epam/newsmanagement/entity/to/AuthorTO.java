package com.epam.newsmanagement.entity.to;

import java.io.Serializable;

/**
 * The Class AuthorTO.
 */
public class AuthorTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4513936706567958992L;

	/** The name. */
	private String name;
	
	/** The id. */
	private long id;
	
	/**
	 * Instantiates a new author.
	 */
	public AuthorTO() {
		
	}
	
	/**
	 * Instantiates a new author that contained the name.
	 *
	 * @param name the name
	 */
	public AuthorTO(String name) {
		this.name = name;
	}
	
	/**
	 * Instantiates a new author to.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public AuthorTO(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorTO other = (AuthorTO) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuthorTO [name=" + name + ", id=" + id + "]";
	}	
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the id of the object.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of the object.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	
}
