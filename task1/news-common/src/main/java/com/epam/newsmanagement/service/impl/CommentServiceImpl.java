package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.to.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * The Class CommentServiceImpl.
 */
@Service("commentService")
public class CommentServiceImpl implements CommentService{
	
	/** The commentDAO use to communicate with DAO. */
	@Autowired
	private CommentDAO commentDAO;


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommentService#create(com.epam.newsmanagement.entity.CommentTO)
	 */
	@Override
	public long create(CommentTO comment) throws ServiceException {
		try {
			return  commentDAO.create(comment);
		} catch (DAOException e) {
			throw new ServiceException("Can't create this comment: " + comment, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommentService#update(com.epam.newsmanagement.entity.CommentTO)
	 */
	@Override
	public void update(CommentTO comment) throws ServiceException {
		try {
			 commentDAO.update(comment);
		} catch (DAOException e) {
			throw new ServiceException("Can`t update this comment: " + comment, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommentService#read(long)
	 */
	@Override
	public CommentTO read(long id) throws ServiceException {
		try {
			return commentDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException("Can't read this comment: " + id, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommentService#delete(long)
	 */
	@Override
	public void delete(long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Can't delete this comment: " + id, e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommentService#searchCommentByNewsId(long)
	 */
	@Override
	public List<CommentTO> searchCommentByNewsId(long newsId) throws ServiceException{
		try {
			return commentDAO.searchCommentsByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't serch comment by news id : " 
										+ newsId, e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.newsmanagement.service.CommentService#deleteCommentByNewsId(long)
	 */
	@Override
	public void deleteCommentByNewsId(long newsId) throws ServiceException {
		try {
			commentDAO.deleteCommentByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException("Can't delete the comment by news id : " 
											+ newsId, e);
		}
		
	}
}
